package com.wm.muggamu.Utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.wm.muggamu.Activities.MainActivity;
import com.wm.muggamu.R;


import org.json.JSONException;
import org.json.JSONObject;

public class Notification_ServiceActivity extends FirebaseMessagingService
{
    private static final int REQUEST_CODE = 1;
    private static final int NOTIFICATION_ID = 6578;
    public Notification_ServiceActivity()
    {
        super();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
              super.onMessageReceived(remoteMessage);
              if(remoteMessage.getData().containsKey("type"))
        {
            String title=remoteMessage.getData().get("title");
            String message=remoteMessage.getData().get("message");
            sendNotification(title, message);
        }
        else
        {
            JSONObject jsonObject = null;
            String title,body,type;
            try {
                jsonObject = new JSONObject(remoteMessage.getData().get("original"));
                Log.e("notificaiton_json", new Gson().toJson(jsonObject));
                JSONObject jsonObject1 =jsonObject.getJSONObject("data");
                title=jsonObject1.getString("title");
                body=jsonObject1.getString("body");
                type=jsonObject1.getString("type");
                sendNotification(title, body);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
             //   sendNotification("", "","");
            }
            // Log.d("remoteMessage",  remoteMessage.toString());
        }
    }
    private void sendNotification(String OrderId, String message)
    {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
       // intent.putExtra("type",type);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setSmallIcon(R.drawable.logo_transperent)
                .setContentTitle(OrderId)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("MainChannel001", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            notificationBuilder.setChannelId("MainChannel001");
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        mNotificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());
    }
}