package com.wm.muggamu.Utils;

public class UrlApi
{
   // public static String BASE_URL = "https://webmobril.org/dev/dryCleaning/api/user/";    //webmobril server

    public static String BASE_URL = "http://52.54.1.108/drycleaning/api/user/";       // Aws Server
    public static String BASE_URL1 = "http://52.54.1.108/drycleaning/api/v1/";       //Aws contact us
    public static String BASE_URL2 = "http://52.54.1.108/drycleaning/user/";       // Aws privacy policy

    public static final String LOGIN="login-with-otp";
    public static final String OtpVerifie="verify-otp";
    public static final String GetProfile="get-user-profile";
    public static final String UPDATEPROFILE="update-user-profile";
    public static final String CONTACTUS="contact-us";
    public static final String GETCATEGORYHOME="get-categories";
    public static final String GETPROVIDERBYCATEGORY="get-providers-by-category";
    public static final String GETPROVIDERDETAILSBYID="get-provider-services";
    public static final String REVIEW="reviews";
    public static final String ALLPOST="all-post";
    public static final String SEARCHCATEGORY="search-category";
    public static final String SEARCHPROVIDER="search-provider";
    public static final String SERVICEONPOST="services-on-post";
    public static final String CREATEPOST="create-post";
    public static final String DELETEPOST="remove-post";
    public static final String ALLBIDSONPOST="all-bids-on-post";

    public static final String ACCEPTBIDSAPI="accept-bid";
    public static final String REJECTBIDSAPI="reject-bid";
    public static final String GETADDRESSAPI="get-addresses";
    public static final String ADDADDRESSAPI="add-address";
    public static final String EDITADDRESSAPI="edit-address";
    public static final String REMOVEADDRESS="remove-address";
    public static final String MARKASDEFAULT="mark-as-default";
    public static final String GETSERVICESUBITEMS="get-service-subitems";
    public static final String CREATEBOOKING="create-booking";
    public static final String GETALLBOOKING="get-all-bookings";
    public static final String ORDERDETAILS="booking-details";
    public static final String GETNOTIFICATION="notifications";
    public static final String TRACKORDER="track-order";
    public static final String RATEPROVIDER="rate-provider";
    public static final String MESSAGES = "messages";
    public static final String GETTOTALFEE = "get-delivery-fees";
    public static final String PRIVACY ="privacy";
    public static final String TERMS="terms";
    public static final String SUBITEMSONPOST="subitems-on-post";
    public static final String UPDATETOKEN="update-device-token";

    public static final String GETPROVIDERAVAILABILITY="get-provider-availability";
    public static final String CREATEAPPOIMENT="create-appointment";
    public static final String getappointmentDetails="get-appointment-details";
}
