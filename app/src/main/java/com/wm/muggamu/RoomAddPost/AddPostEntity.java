package com.wm.muggamu.RoomAddPost;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "addpost_table")
public class AddPostEntity implements Serializable
{
    @NonNull
    @ColumnInfo(name = "parent_id")
    private int parent_id;

    @NonNull
    @ColumnInfo(name = "child_id")
    private int child_id;
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "unique_id")
    private String unique_id;

    @NonNull
    @ColumnInfo(name = "service_id")
    private int service_id;

    @NonNull
    @ColumnInfo(name = "child_quantity")
    private int child_quantity;


    @NonNull
    @ColumnInfo(name = "item_name")
    private String item_name;

    @NonNull
    @ColumnInfo(name = "parent_name")
    private String parent_name;

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public AddPostEntity()
    {
    }


   /* @NonNull
    @ColumnInfo(name = "deliver_fee")
    private float deliver_fee;*/



    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getChild_id() {
        return child_id;
    }

    public void setChild_id(int child_id) {
        this.child_id = child_id;
    }

    public void setService_id(int service_id) {
        this.service_id = service_id;
    }

    public int getService_id() {
        return service_id;
    }

    public int getChild_quantity() {
        return child_quantity;
    }

    public void setChild_quantity(int child_quantity) {
        this.child_quantity = child_quantity;
    }


   /* @NonNull
    public float getDeliver_fee() {
        return deliver_fee;
    }

    public void setDeliver_fee(@NonNull float deliver_fee) {
        this.deliver_fee = deliver_fee;
    }*/


    @NonNull
    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(@NonNull String item_name) {
        this.item_name = item_name;
    }


    @NonNull
    public String getParent_name() {
        return parent_name;
    }

    public void setParent_name(@NonNull String parent_name)
    {
        this.parent_name = parent_name;
    }

}
