package com.wm.muggamu.RoomAddPost;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {AddPostEntity.class}, version = 4, exportSchema = true)
public abstract class AddPostDatabase extends RoomDatabase
{
    public abstract AddPostDao addPostDao();
    private static AddPostDatabase instance;
    public static AddPostDatabase getInstance(Context context)
    {
        if(instance == null) instance = Room.databaseBuilder(context, AddPostDatabase.class, "addpostDatabase4.db")
                    .allowMainThreadQueries()
                    .build();
        return instance;
    }
}
