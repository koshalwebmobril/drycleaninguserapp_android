package com.wm.muggamu.RoomAddPost;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParentAddPostModel implements Parcelable {

	@SerializedName("category")
	private String category;

	@SerializedName("items")
	private List<AddPostEntity> items;

	public ParentAddPostModel(Parcel in) {
		category = in.readString();
	}

	public static final Creator<ParentAddPostModel> CREATOR = new Creator<ParentAddPostModel>() {
		@Override
		public ParentAddPostModel createFromParcel(Parcel in) {
			return new ParentAddPostModel(in);
		}

		@Override
		public ParentAddPostModel[] newArray(int size) {
			return new ParentAddPostModel[size];
		}
	};

	public ParentAddPostModel() {

	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<AddPostEntity> getItems() {
		return items;
	}

	public void setItems(List<AddPostEntity> items) {
		this.items = items;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(category);
	}
}