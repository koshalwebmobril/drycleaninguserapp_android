package com.wm.muggamu.RoomAddPost;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface AddPostDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertCustomData(AddPostEntity addPostEntity);

    @Query("SELECT * FROM addpost_table")
    public List<AddPostEntity> getAllData();

    @Query("SELECT * FROM addpost_table WHERE unique_id=:childId")
    public boolean getItem(String childId);

    @Query("SELECT child_quantity FROM addpost_table WHERE unique_id=:child_id")
    public int getItemValue(String child_id);




    @Query("SELECT * From addpost_table WHERE child_quantity IS NULL")
    public boolean getZero();

    @Query("DELETE FROM addpost_table")
    void emptyList();

    @Query("DELETE from addpost_table WHERE unique_id=:addpostEntity ")
    void deleteItem(String addpostEntity);


    @Query("SELECT parent_id FROM addpost_table GROUP BY parent_id")
    public List<Integer> getCategoryIdList();

    @Query("SELECT parent_name FROM addpost_table WHERE parent_id=:parentId")
    public String getParentName(int parentId);

  /*  @Query("SELECT child_id FROM addpost_table WHERE parent_id=:parentId")
    public List<Integer> getParentId(int parentId);*/



    @Query("SELECT * FROM addpost_table WHERE parent_id=:parentId")
    public List<AddPostEntity> getChildIdAndQuantity(int parentId);
}
