package com.wm.muggamu.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Activities.MainActivity;
import com.wm.muggamu.Adapter.UserPostAdapter;
import com.wm.muggamu.Interface.OnClickRemovePost;
import com.wm.muggamu.Models.userallpostmodel.AllPostModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.postremovemodel.RemovePostResponse;
import com.wm.muggamu.Models.userallpostmodel.UserAllPostResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.util.List;

import butterknife.BindView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class UserPostFragment extends Fragment implements OnClickRemovePost {
    View view;
    UserPostAdapter addbidsadapter;
    @BindView(R.id.recyclerview_addpost)
    RecyclerView recyclerviewAddpost;

    @BindView(R.id.no_post)
    TextView noPost;

    private OnClickRemovePost onClickRemovePost;
    List<AllPostModel> allPostModelList;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_post, container, false);
        ((MainActivity) requireActivity()).toolbarHomeForPost("Posts");
        ((MainActivity) requireActivity()).updateBottomBar(2);
        init();
        getUserAllPosts();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init() {
        onClickRemovePost = this;
        recyclerviewAddpost=view.findViewById(R.id.recyclerview_addpost);
        noPost=view.findViewById(R.id.no_post);
    }

    public void getUserAllPosts()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(), getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<UserAllPostResponse> call = service.allPost(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<UserAllPostResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UserAllPostResponse> call, Response<UserAllPostResponse> response) {
                progressDialog.dismiss();
                try {
                    UserAllPostResponse resultFile = response.body();

                    if (resultFile.getCode() == 200)
                    {
                        noPost.setVisibility(View.INVISIBLE);
                        recyclerviewAddpost.setVisibility(View.VISIBLE);

                        allPostModelList = resultFile.getPostResult();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
                        recyclerviewAddpost.setLayoutManager(mLayoutManager);
                        addbidsadapter = new UserPostAdapter(getActivity(), allPostModelList, onClickRemovePost);
                        recyclerviewAddpost.setAdapter(addbidsadapter);
                    }
                    else if(resultFile.getCode() == 401)
                    {
                        Toast.makeText(getActivity(), "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        noPost.setVisibility(View.VISIBLE);
                        recyclerviewAddpost.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<UserAllPostResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    @Override
    public void removepost(int post_id, int position)
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(), getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<RemovePostResponse> call = service.removePost(LoginPreferences.getActiveInstance(getActivity()).getToken(), String.valueOf(post_id));
        call.enqueue(new Callback<RemovePostResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<RemovePostResponse> call, Response<RemovePostResponse> response) {
                progressDialog.dismiss();
                try {
                    RemovePostResponse resultFile = response.body();
                    Toast.makeText(getActivity(), resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if (resultFile.getCode() == 200)
                    {
                        allPostModelList.remove(position);

                        if(allPostModelList.size()<=0)
                        {
                            noPost.setVisibility(View.VISIBLE);
                            recyclerviewAddpost.setVisibility(View.INVISIBLE);
                        }
                        else {
                            noPost.setVisibility(View.INVISIBLE);
                            recyclerviewAddpost.setVisibility(View.VISIBLE);
                        }
                        addbidsadapter.notifyDataSetChanged();
                    }
                    else if (resultFile.getCode() == 401)
                    {
                        Toast.makeText(getActivity(), "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<RemovePostResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}