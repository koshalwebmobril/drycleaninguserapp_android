package com.wm.muggamu.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Adapter.ReviewsAdapter;
import com.wm.muggamu.Models.reviewmodel.ReviewListItemModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.reviewmodel.ReviewResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class ReviewFragment extends Fragment {
    View view;
    RecyclerView recycler_review;
    ReviewsAdapter reviewsAdapter;
    TextView no_review;
    List<ReviewListItemModel> reviewlistitemmodel;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_review, container, false);
        init();
        getReviewApi();
        return view;
    }
    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        recycler_review=view.findViewById(R.id.recycler_review);
        no_review=view.findViewById(R.id.no_review);
    }

    private void getReviewApi()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ReviewResponse> call = service.getReview(LoginPreferences.getActiveInstance(getActivity()).getToken(),LoginPreferences.getActiveInstance(getActivity()).getProviderId());
        call.enqueue(new Callback<ReviewResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ReviewResponse> call, retrofit2.Response<ReviewResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    ReviewResponse resultFile = response.body();

                    if(resultFile.getCode() == 200)
                    {
                        List<ReviewListItemModel> reviewlistmodel= resultFile.getReviewList();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                        recycler_review.setLayoutManager(mLayoutManager);
                        reviewsAdapter = new ReviewsAdapter(getActivity(),reviewlistmodel);
                        recycler_review.setAdapter(reviewsAdapter);

                        no_review.setVisibility(View.GONE);
                        recycler_review.setVisibility(View.VISIBLE);
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        no_review.setVisibility(View.VISIBLE);
                        recycler_review.setVisibility(View.GONE);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ReviewResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}