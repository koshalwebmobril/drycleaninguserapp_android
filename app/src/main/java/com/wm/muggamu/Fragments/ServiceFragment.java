package com.wm.muggamu.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Activities.BookingActivity;
import com.wm.muggamu.Adapter.ServiceListAdapter;

import com.wm.muggamu.ApiClient.RetrofitConnection;
import com.wm.muggamu.Interface.NextVisibleListener;
import com.wm.muggamu.Models.totalamountmodel.TotalAmountResponse;
import com.wm.muggamu.Models.getservicesmodel.ServicesItemModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.getservicesmodel.GetServicesResponse;
import com.wm.muggamu.Room.CartItemDatabase;
import com.wm.muggamu.Room.CartItemEntity;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.view.View.GONE;
import static com.wm.muggamu.Utils.CommonMethod.TAG;

public class ServiceFragment extends Fragment implements NextVisibleListener
{
    View view;
    RecyclerView recycler_services;
    ServiceListAdapter servicelistAdapter;
    TextView noservicefound;
    Button btn_next;
   // List<ParentCartItemsModel> cartitemslist =new ArrayList<>();
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_service, container, false);
        init();

        if(CommonMethod.isOnline(getActivity()))
        {
            getProviderServicesById();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet),getActivity());
        }
        return view;
    }
    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    public void init()
    {
        recycler_services=view.findViewById(R.id.recycler_services);
        noservicefound=view.findViewById(R.id.noservicefound);
        btn_next=view.findViewById(R.id.btn_next);

        CartItemDatabase.getInstance(requireActivity()).cartItemDao().emptyList();

        if(CartItemDatabase.getInstance(requireActivity()).cartItemDao().getAllData().size()>0)
        {
            if (CartItemDatabase.getInstance(requireActivity()).cartItemDao().getZero())
            {
                btn_next.setVisibility(GONE);
            }
            else
                {
                btn_next.setVisibility(View.VISIBLE);
              }
        }
        else
            {
              btn_next.setVisibility(GONE);
            }

        btn_next.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                 List<CartItemEntity> allcart_items= CartItemDatabase.getInstance(requireActivity()).cartItemDao().getAllData();
                 float total_amount=0;
                 for(int i=0; i<allcart_items.size();i++)
                {
                    float amount=allcart_items.get(i).getChild_quantity()*allcart_items.get(i).getQuantity_price();
                    total_amount=total_amount+amount;
                }

                 if(CommonMethod.isOnline(getActivity()))
                {
                    hitTotalAmountApi(total_amount);
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet),getActivity());
                }
                 Log.d(TAG,String.valueOf(total_amount));
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (CartItemDatabase.getInstance(requireActivity()).cartItemDao().getAllData().size()>0)
        {
            if (CartItemDatabase.getInstance(requireActivity()).cartItemDao().getZero()){
                btn_next.setVisibility(GONE);
            }else {
                btn_next.setVisibility(View.VISIBLE);
            }
        }
        else
            {
            btn_next.setVisibility(GONE);
           }
    }

    private void getProviderServicesById()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetServicesResponse> call = service.getServiceList(LoginPreferences.getActiveInstance(getActivity()).getToken(),LoginPreferences.getActiveInstance(getActivity()).getProviderId());
        call.enqueue(new Callback<GetServicesResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetServicesResponse> call, retrofit2.Response<GetServicesResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetServicesResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        recycler_services.setVisibility(View.VISIBLE);
                        noservicefound.setVisibility(GONE);
                        List<ServicesItemModel> detailslist= resultFile.getProviderServices();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                        recycler_services.setLayoutManager(mLayoutManager);
                        servicelistAdapter = new ServiceListAdapter(getActivity(),detailslist);
                        recycler_services.setAdapter(servicelistAdapter);
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        recycler_services.setVisibility(GONE);
                        noservicefound.setVisibility(View.VISIBLE);
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetServicesResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void hitTotalAmountApi(float total_amount)
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<TotalAmountResponse> call = service.gettotalamount(String.valueOf(total_amount));
        call.enqueue(new Callback<TotalAmountResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<TotalAmountResponse> call, retrofit2.Response<TotalAmountResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    TotalAmountResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        Intent i=new Intent(getActivity(), BookingActivity.class);
                        i.putExtra("total_amount",String.valueOf(resultFile.getTotalAmount()));
                        i.putExtra("delivery_fee",String.valueOf(resultFile.getFees()));
                        startActivity(i);
                    }
                    else if(resultFile.getCode() == 404)
                    {

                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<TotalAmountResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }



    @Override
    public void visibleButton() {

    }
}