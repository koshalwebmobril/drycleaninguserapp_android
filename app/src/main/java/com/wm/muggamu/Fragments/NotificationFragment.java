package com.wm.muggamu.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wm.muggamu.Activities.MainActivity;
import com.wm.muggamu.Adapter.NotificationAdapter;
import com.wm.muggamu.ApiClient.RetrofitConnection;
import com.wm.muggamu.Models.notificationmodel.NotificationModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.notificationmodel.NotificationResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class NotificationFragment extends Fragment {
    View view;
    RecyclerView recyclerview_notification;
    NotificationAdapter notificationAdapter;
      ArrayList<NotificationModel> notificationlist;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_notification, container, false);
        ((MainActivity)requireActivity()).toolbarHomeOther("Notifications");
        ((MainActivity)requireActivity()).updateBottomBar(3);
        init();
        if(CommonMethod.isOnline(getActivity()))
        {
            getNotificationApi();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet),getActivity());
        }
        return view;
    }
    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        recyclerview_notification=view.findViewById(R.id.recyclerview_notification);
    }


    public void getNotificationApi()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<NotificationResponse> call = service.getNotification(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<NotificationResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<NotificationResponse> call, retrofit2.Response<NotificationResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    NotificationResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        List<NotificationModel> notificationModelList=resultFile.getNotificationList();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                        recyclerview_notification.setLayoutManager(mLayoutManager);
                        notificationAdapter = new NotificationAdapter(getActivity(),notificationModelList);
                        recyclerview_notification.setAdapter(notificationAdapter);
                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(getActivity(), "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}