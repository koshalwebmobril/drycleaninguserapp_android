package com.wm.muggamu.Fragments;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Activities.MainActivity;
import com.wm.muggamu.Adapter.YourBookingAdapter;
import com.wm.muggamu.Models.yourbookingmodel.BookingsItemModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.yourbookingmodel.BookingResultResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class YourBookingFragment extends Fragment
{
    RecyclerView recyclerview_yourbooking;
    YourBookingAdapter yourBookingAdapter;
    ArrayList<BookingsItemModel> bookingmanagmentlist;
    View view;
    TextView no_result;
    ProgressD progressDialog;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_your_booking, container, false);
        ((MainActivity)requireActivity()).toolbarHomeOther("Your Bookings");
        ((MainActivity)requireActivity()).updateBottomBar(1);
        init();

        if(CommonMethod.isOnline(getActivity()))
        {
            getBookingApi();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet),getActivity());
        }
        return view;
    }
    private void init()
    {
        recyclerview_yourbooking=view.findViewById(R.id.recyclerview_yourbooking);
        no_result=view.findViewById(R.id.no_result);
    }

    public void getBookingApi()
    {
        progressDialog = ProgressD.show(getActivity(), getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<BookingResultResponse> call = service.getAllBooking(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<BookingResultResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<BookingResultResponse> call, Response<BookingResultResponse> response) {
                progressDialog.dismiss();
                try {
                    BookingResultResponse resultFile = response.body();
                    if (resultFile.getCode() == 200)
                    {
                        no_result.setVisibility(View.GONE);
                        recyclerview_yourbooking.setVisibility(View.VISIBLE);

                        bookingmanagmentlist = (ArrayList<BookingsItemModel>) resultFile.getBookings();
                        recyclerview_yourbooking.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        yourBookingAdapter = new YourBookingAdapter(getActivity(),bookingmanagmentlist);
                        recyclerview_yourbooking.setAdapter(yourBookingAdapter);
                    }
                    else if(resultFile.getCode() == 401)
                    {
                         Toast.makeText(getActivity(), resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        no_result.setVisibility(View.VISIBLE);
                        no_result.setText(resultFile.getMessage());
                       // Toast.makeText(getActivity(), resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                        recyclerview_yourbooking.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<BookingResultResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

}