package com.wm.muggamu.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.wm.muggamu.Activities.MainActivity;
import com.wm.muggamu.Adapter.HomeAdapter;
import com.wm.muggamu.Adapter.SliderAdapterExample;
import com.wm.muggamu.ApiClient.RetrofitConnection;
import com.wm.muggamu.Models.homepagemodel.CategoriesItemModel;
import com.wm.muggamu.Models.homepagemodel.SlidersItemModel;
import com.wm.muggamu.Models.searchcategorymodel.SearchCategoryResponse;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.homepagemodel.GetCategoryResponse;
import com.wm.muggamu.Models.removeaddressmodel.RemoveAddressResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;

import com.wm.muggamu.databinding.FragmentHomeBinding;
import com.wm.muggamu.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.view.View.GONE;

public class HomeFragment extends Fragment
{
    private FragmentHomeBinding binding;
    RecyclerView recycler_home;
    HomeAdapter homeAdapter;
    View view;
    ImageView search;
    EditText editsearch;
    TextView txtnoresult;
    List<CategoriesItemModel> categoryitemlist;
    List<CategoriesItemModel> searchcategorylist;
    RelativeLayout relative_search_category;
    SliderView sliderView;
    private SliderAdapterExample adapter;
    String notification_token;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        view = binding.getRoot();
        ((MainActivity)requireActivity()).toolbarHome();
        init();
        notification_token = FirebaseInstanceId.getInstance().getToken();
        if(CommonMethod.isOnline(getActivity()))
        {
            GetCategoryApi();
            Updatedevicetoken();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet),getActivity());
        }
        adapter = new SliderAdapterExample(getActivity());
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();
        return view;
    }
    private void init()
    {
        recycler_home=view.findViewById(R.id.recycler_home);
        search=view.findViewById(R.id.search);
        editsearch=view.findViewById(R.id.edit_search);
        txtnoresult=view.findViewById(R.id.txtnoresult);
        relative_search_category=view.findViewById(R.id.relative_search_category);
        sliderView = view.findViewById(R.id.imageSlider);
        //editsearch.setText("");


        editsearch.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if(actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    hitSearchApi(String.valueOf(editsearch.getText().toString()));
                    hideKeyboard(search);
                    return true;
                }
                return false;
            }
        });


        search.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(editsearch.getText().toString().trim().equals("") && editsearch.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(getActivity(),getString(R.string.pleaseentervalidkey), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    hitSearchApi(String.valueOf(editsearch.getText().toString()));
                    hideKeyboard(view);
                }

            }
        });
        editsearch.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d("tag",s.toString());
                if(String.valueOf(s).equals(""))
                {
                    if(CommonMethod.isOnline(getActivity()))
                    {
                        GetCategoryApi();
                       // Updatedevicetoken();
                    }
                    else
                    {
                        CommonMethod.showAlert(getString(R.string.check_internet),getActivity());
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private void GetCategoryApi()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<GetCategoryResponse> call = service.getcategoryhome(LoginPreferences.getActiveInstance(getActivity()).getToken());
        call.enqueue(new Callback<GetCategoryResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetCategoryResponse> call, retrofit2.Response<GetCategoryResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetCategoryResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        relative_search_category.setVisibility(View.VISIBLE);
                        txtnoresult.setVisibility(GONE);

                        recycler_home.setVisibility(View.VISIBLE);
                        binding.cardSearch.setVisibility(View.VISIBLE);

                        List<SlidersItemModel> sliderItemList=resultFile.getHomepageResultResponse().getSliders();
                        adapter.renewItems(sliderItemList);
                        categoryitemlist=resultFile.getHomepageResultResponse().getCategories();
                        LinearLayoutManager gridLayoutManager = new GridLayoutManager(  getActivity(), 2);
                        recycler_home.setLayoutManager(gridLayoutManager);
                        homeAdapter = new HomeAdapter(getActivity(),categoryitemlist);
                        recycler_home.setAdapter(homeAdapter);
                        homeAdapter.notifyDataSetChanged();
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        txtnoresult.setVisibility(View.VISIBLE);
                        recycler_home.setVisibility(GONE);
                        txtnoresult.setText(resultFile.getMessage());
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetCategoryResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void Updatedevicetoken()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<RemoveAddressResponse> call = service.updatetoken(LoginPreferences.getActiveInstance(getActivity()).getToken(),"2",notification_token);
        call.enqueue(new Callback<RemoveAddressResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<RemoveAddressResponse> call, retrofit2.Response<RemoveAddressResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    RemoveAddressResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                    //    Toast.makeText(getActivity(), "sucess", Toast.LENGTH_SHORT).show();
                    }
                    else if(resultFile.getCode() == 404)
                    { }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<RemoveAddressResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void hitSearchApi(String keyword)
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<SearchCategoryResponse> call = service.searchprovider(LoginPreferences.getActiveInstance(getActivity()).getToken(),keyword);
        call.enqueue(new Callback<SearchCategoryResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<SearchCategoryResponse> call, retrofit2.Response<SearchCategoryResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    SearchCategoryResponse resultFile = response.body();
                    // Toast.makeText(getActivity(), resultFile.g, Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200 )
                    {
                        txtnoresult.setVisibility(GONE);
                        recycler_home.setVisibility(View.VISIBLE);
                      //  categoryitemlist.clear();
                      //  categoryitemlist=new ArrayList<>();

                        searchcategorylist=resultFile.getCategories();
                        LinearLayoutManager gridLayoutManager = new GridLayoutManager( getActivity(), 2);
                        recycler_home.setLayoutManager(gridLayoutManager);
                        homeAdapter = new HomeAdapter(getActivity(),searchcategorylist);
                        recycler_home.setAdapter(homeAdapter);
                        homeAdapter.notifyDataSetChanged();
                    }
                    else if(resultFile.getCode() == 404)
                    {
                       // categoryitemlist.clear();
                        txtnoresult.setVisibility(View.VISIBLE);
                        recycler_home.setVisibility(GONE);
                        txtnoresult.setText(resultFile.getMessage());
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<SearchCategoryResponse> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
    @Override
    public void onResume()
    {
        super.onResume();
    }
}