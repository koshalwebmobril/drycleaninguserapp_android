package com.wm.muggamu.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Activities.AppoimentConfirmActivity;
import com.wm.muggamu.Adapter.TimeListAdapter;
import com.wm.muggamu.Interface.TimeSelectListener;
import com.wm.muggamu.Models.availabilitymanagmentmodel.AvailabilityManagmentResponse;
import com.wm.muggamu.Models.availabilitymanagmentmodel.TimeSlotsItem;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.Singleton;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import noman.weekcalendar.WeekCalendar;
import noman.weekcalendar.listener.OnDateClickListener;
import noman.weekcalendar.listener.OnWeekChangeListener;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.view.View.GONE;

public class SelectDateFragment extends Fragment implements View.OnClickListener,TimeSelectListener
{
    View view;
    RecyclerView timeRecyclerView;
    TimeSelectListener timeSelectListener;
    TimeListAdapter timeListAdapter;
    WeekCalendar weekCalendar;
    DateTime currentSelectedDate;
    private long selectedDateInMilis;
    private long currentDateTimeInMillis;
    String selectedDate;
    TextView monthTV;
    int count = 0;
    String currentDate;
    ImageView previousArrow,nextArrow;
    public List<TimeSlotsItem> timeList;
    TextView nodataresult;
    private String selectedTime="";
    Button btn_save;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_select_date, container, false);
        Singleton.LASTSELECTEDLISTPOSITION = -1;
        Singleton.LASTSELECTEDITEMPOSITION = -1;
        Singleton.LASTSELECTEDTIME = "";

        selectedTime="";
        init();
        weekCalendar.reset();
        timeSelectListener = this;
        previousArrow.setOnClickListener(this);
        nextArrow.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        weekCalendar.reset();
        timeList=new ArrayList<>();

        if(CommonMethod.isOnline(getActivity()))
        {
            getTimeSlotsApi(selectedDate);
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet),getActivity());
        }
        return view;
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        timeRecyclerView = view.findViewById(R.id.timeRecyclerView);
        weekCalendar = view.findViewById(R.id.weekCalendar);
        monthTV = view.findViewById(R.id.monthTV);
        previousArrow = view.findViewById(R.id.previousArrow);
        nextArrow = view.findViewById(R.id.nextArrow);
        nodataresult=view.findViewById(R.id.nodataresult);
        btn_save=view.findViewById(R.id.btn_save);

        setCurrentDateTime();

        weekCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(DateTime dateTime) {
                currentSelectedDate = dateTime;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                selectedDateInMilis = dateTime.getMillis();
                selectedDate = dateFormat.format(new Date(dateTime.getMillis()));
                Log.i("TAG", "SandeepSelected: " + selectedDateInMilis);

                timeList=new ArrayList<>();

                Singleton.LASTSELECTEDLISTPOSITION = -1;
                Singleton.LASTSELECTEDITEMPOSITION = -1;
                Singleton.LASTSELECTEDTIME = "";

                if(CommonMethod.isOnline(getActivity()))
                {
                    getTimeSlotsApi(selectedDate);
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet),getActivity());
                }
            }
        });

        weekCalendar.setOnWeekChangeListener(new OnWeekChangeListener() {
            @Override
            public void onWeekChange(DateTime firstDayOfTheWeek, boolean forward)
            {
                if (count > 0) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM/dd/yyyy");
                    String myDate = dateFormat.format(new Date(firstDayOfTheWeek.getMillis()));
                    String weekLastDate = dateFormat.format(new Date(firstDayOfTheWeek.plusDays(7).getMillis()));

                    String currentMonth = myDate.substring(0, myDate.indexOf("/"));
                    String weekLastDayMonth = weekLastDate.substring(0, weekLastDate.indexOf("/"));
                    Log.i("TAG", "onWeekChange1: " + currentMonth);
                    Log.i("TAG", "onWeekChange12: " + weekLastDayMonth);

                    if (currentMonth.equals(weekLastDayMonth))
                    {
                        monthTV.setText(currentMonth);
                    } else {
                        monthTV.setText(currentMonth + "/" + weekLastDayMonth);
                    }
                } else
                    {
                    count = 1;
                }
                Singleton.LASTSELECTEDLISTPOSITION = -1;
                Singleton.LASTSELECTEDITEMPOSITION = -1;
                Singleton.LASTSELECTEDTIME = "";
            }
        });


    }

    private void setCurrentDateTime()
    {
        DateTime dateTime = DateTime.now();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        selectedDateInMilis = dateTime.getMillis();
        selectedDate = dateFormat.format(new Date(dateTime.getMillis()));
        currentDate = selectedDate;
        currentSelectedDate = DateTime.now();
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("MMMM/dd/yyyy");
        String formattedDate = df.format(c);
        String currentMonth = formattedDate.substring(0, formattedDate.indexOf("/"));
        monthTV.setText(currentMonth);
        Log.d("current_time", currentMonth);
        Log.i("TAG", "setCurrentDateTime: " + currentSelectedDate);
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = (Date) formatter.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        currentDateTimeInMillis = date.getTime();
        selectedDateInMilis = currentDateTimeInMillis;
        Log.i("TAG", "SandeepCurrent: " + currentDateTimeInMillis);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.nextArrow:
                weekCalendar.moveToNext();
                currentSelectedDate = currentSelectedDate.plusDays(1);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                selectedDate = dateFormat.format(new Date(currentSelectedDate.getMillis()));
                Log.i("TAG", "onDateClick: " + selectedDate);
                selectedDateInMilis = currentSelectedDate.getMillis();

                timeList=new ArrayList<>();
                if(CommonMethod.isOnline(getActivity()))
                {
                    getTimeSlotsApi(selectedDate);
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet),getActivity());
                }
                break;

            case R.id.previousArrow:
                weekCalendar.moveToPrevious();
                currentSelectedDate = currentSelectedDate.plusDays(-1);
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
                selectedDate = dateFormat1.format(new Date(currentSelectedDate.getMillis()));
                selectedDateInMilis = currentSelectedDate.getMillis();
                timeList=new ArrayList<>();

                if(CommonMethod.isOnline(getActivity()))
                {
                    getTimeSlotsApi(selectedDate);
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet),getActivity());
                }
                break;

            case R.id.btn_save:
               if(!selectedTime.equals(""))
               {
                   Intent intent = new Intent(getActivity(), AppoimentConfirmActivity.class);
                   intent.putExtra("selected_date",selectedDate);
                   intent.putExtra("selected_time",selectedTime);
                   startActivity(intent);
               }
               else
               {
                   Toast.makeText(getActivity(), R.string.pleaseselecttime, Toast.LENGTH_SHORT).show();
               }
               break;
        }
    }

    private void getTimeSlotsApi(String selectedDate)
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(), getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<AvailabilityManagmentResponse> call = service.getAvailibilty
                (LoginPreferences.getActiveInstance(getActivity()).getToken(),
                        LoginPreferences.getActiveInstance(getActivity()).getProviderId(),
                        selectedDate);
        call.enqueue(new Callback<AvailabilityManagmentResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AvailabilityManagmentResponse> call, retrofit2.Response<AvailabilityManagmentResponse> response) {
                progressDialog.dismiss();
                try {
                    AvailabilityManagmentResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        timeList=new ArrayList<>();
                        timeList = resultFile.getTimeSlots();
                        timeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        timeListAdapter = new TimeListAdapter(getActivity(), timeList, timeSelectListener);
                        timeRecyclerView.setAdapter(timeListAdapter);

                        timeRecyclerView.setVisibility(View.VISIBLE);
                        nodataresult.setVisibility(GONE);

                    }
                    else if (resultFile.getCode() == 404)
                    {
                       // Toast.makeText(getActivity(),resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                        nodataresult.setText(resultFile.getMessage());
                        timeList.clear();

                        timeRecyclerView.setVisibility(GONE);
                        nodataresult.setVisibility(View.VISIBLE);

                        timeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        timeListAdapter = new TimeListAdapter(getActivity(), timeList, timeSelectListener);
                        timeRecyclerView.setAdapter(timeListAdapter);
                    }
                    else
                    { }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AvailabilityManagmentResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    @Override
    public void onSetAvailabilityStatus(int parentPosition, int childPosition, String time, int timeStatus)
    {
        int lastListPosition = Singleton.LASTSELECTEDLISTPOSITION;
        int lastItemPosition = Singleton.LASTSELECTEDITEMPOSITION;
        String lastSelectedTime = Singleton.LASTSELECTEDTIME;

        if(!TextUtils.isEmpty(lastSelectedTime))
        {
           // SlotsItem lasttimeItem = new SlotsItem();
            timeList.get(lastListPosition).getSlots().get(lastItemPosition).setAvailability_status("1");
            timeList.get(parentPosition).getSlots().get(childPosition).setAvailability_status("2");
        }
        else
        {
            timeList.get(parentPosition).getSlots().get(childPosition).setAvailability_status("2");
        }
        timeListAdapter.notifyDataSetChanged();
        selectedTime = time;
    }
}
