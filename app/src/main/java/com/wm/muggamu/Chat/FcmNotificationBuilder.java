package com.wm.muggamu.Chat;

import android.util.Log;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FcmNotificationBuilder {
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    private static final String TAG = "FcmNotificationBuilder";
    private static final String SERVER_API_KEY = "AAAATLGTZbM:APA91bED-3TxVS98ZULtXGEL84L-eSAjZ1-Ymee_KyFZr61rCkCfFukWk3sBkRUoA-ju6Mk6e-dv22z_EGmNYbssxeylBEKhGX4F8iVENF1WfRzguCI9w3zxkjHFtYWm6GcK7RWxphna";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APPLICATION_JSON = "application/json";
    private static final String AUTHORIZATION = "Authorization";
    private static final String AUTH_KEY = "key=" + SERVER_API_KEY;
    private static final String PROJECTID = "329396741555";
    private static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";
    private static final String GROUP_FCM_URL = "https://fcm.googleapis.com/fcm/notification";
    // json related keys
    private static final String KEY_TO = "to";
    private static final String KEY_NOTIFICATION = "notification";
    private static final String KEY_TITLE = "title";
    private static final String KEY_TEXT = "message";
    private static final String KEY_DATA = "data";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_UID = "uid";
    private static final String KEY_FCM_TOKEN = "fcm_token";
    private static final String KEY_TYPE = "type";
    private static final String KEY_BODY = "body";
    private static final String REGISTRATION_IDS = "registration_ids";

    private String mTitle;
    private String mMessage;
    private String mUsername;
    private String mUid;
    private String mFirebaseToken;
    private String mReceiverFirebaseToken;
    private String typeNotification;
    private String recieverId;
    private String msgFrom;
    private String groupId;
    private String[] registration_ids;

    private FcmNotificationBuilder() {

    }

    public static FcmNotificationBuilder initialize() {
        return new FcmNotificationBuilder();
    }

    public FcmNotificationBuilder title(String title) {
        mTitle = title;
        return this;
    }
    public FcmNotificationBuilder groupId(String grouId) {
        groupId=grouId;
        return this;
    }
    public FcmNotificationBuilder messageFrom(String msg) {
        msgFrom=msg;
        return this;
    }

    public FcmNotificationBuilder message(String message) {
        mMessage = message;
        return this;
    }

    public FcmNotificationBuilder username(String username) {
        mUsername = username;
        return this;
    }

    public FcmNotificationBuilder uid(String uid) {
        mUid = uid;
        return this;
    }

    public FcmNotificationBuilder firebaseToken(String firebaseToken) {
        mFirebaseToken = firebaseToken;
        return this;
    }

    public FcmNotificationBuilder receiverFirebaseToken(String receiverFirebaseToken) {
        mReceiverFirebaseToken = receiverFirebaseToken;
        return this;
    }
    public FcmNotificationBuilder type(String chat) {
        typeNotification = chat;
        return this;
    }
    public FcmNotificationBuilder recieverUid(String messageReceiverID) {
        recieverId = messageReceiverID;
        return this;
    }

    public FcmNotificationBuilder registration_ids(ArrayList<String> registrationids) {
        String[] array = new String[registrationids.size()];
        for(int j =0;j<registrationids.size();j++){
            array[j] = "\""+registrationids.get(j)+"\"";
        }
        registration_ids= array;
        return this;
    }

    public void send() {
        RequestBody requestBody = null;
        try {
            requestBody = RequestBody.create(MEDIA_TYPE_JSON, getValidJsonBody().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request;
        if (msgFrom.equals("group")){
             request = new Request.Builder()
                    .addHeader(CONTENT_TYPE, APPLICATION_JSON)
                    .addHeader(AUTHORIZATION, AUTH_KEY)
                     .addHeader("project_id",PROJECTID)
                    .url(GROUP_FCM_URL)
                    .post(requestBody)
                    .build();
        }else {
             request = new Request.Builder()
                    .addHeader(CONTENT_TYPE, APPLICATION_JSON)
                    .addHeader(AUTHORIZATION, AUTH_KEY)
                    .url(FCM_URL)
                    .post(requestBody)
                    .build();
        }

        Call call = new OkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e("tag", "onGetAllUsersFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.body() != null) {
                    Log.e("tag", "onResponse: " + response.body().string());
                }
            }
        });
    }


    private JSONObject getValidJsonBody() throws JSONException {
        JSONObject jsonObjectBody = new JSONObject();
       /* if (msgFrom.equals("group")) {
            JSONArray jsonArray=new JSONArray(registration_ids);
            jsonObjectBody.put(REGISTRATION_IDS,jsonArray);
            jsonObjectBody.put("operation","add");
            jsonObjectBody.put("notification_key",mFirebaseToken);
            jsonObjectBody.put("notification_key_name", groupId);
        }else {*/
            jsonObjectBody.put(KEY_TO, mReceiverFirebaseToken);
       // }
      //  Log.e(TAG,"REGID--->"+ Arrays.toString(Arrays.asList(registration_ids).toArray()));
        JSONObject jsonObjectData = new JSONObject();
        jsonObjectData.put(KEY_TITLE, mTitle);
        jsonObjectData.put(KEY_BODY, mMessage);
        jsonObjectData.put(KEY_TEXT, mMessage);
        jsonObjectData.put(KEY_USERNAME, mUsername);
        jsonObjectData.put(KEY_UID, mUid);
        jsonObjectData.put(KEY_FCM_TOKEN, mFirebaseToken);
        jsonObjectData.put(KEY_TYPE,typeNotification);
        jsonObjectBody.put(KEY_DATA, jsonObjectData);
        jsonObjectBody.put(KEY_NOTIFICATION, jsonObjectData);
        return jsonObjectBody;
    }
}
