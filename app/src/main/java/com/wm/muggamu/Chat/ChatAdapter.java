package com.wm.muggamu.Chat;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;

import java.util.ArrayList;

import static android.view.View.GONE;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {
    Context context;
    ArrayList<ChatModel> userchatlist;
    public ChatAdapter(Context context, ArrayList userchatlist)
    {
        this.context = context;
        this.userchatlist = userchatlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        ChatModel chatmodel=userchatlist.get(position);
        holder.txt_left_chat.setText(chatmodel.getMessage());
        holder.txt_right_chat.setText(chatmodel.getMessage());
        String user_id= LoginPreferences.getActiveInstance(context).getUserId();
        Log.e("tag_use_id",user_id);

        /*if(userchatlist.size()==0)
        {
            holder.linear_allchats.setVisibility(GONE);
            holder.txtnodata.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.linear_allchats.setVisibility(View.VISIBLE);
            holder.txtnodata.setVisibility(GONE);
        }*/

        if(chatmodel.getFrom().equals(user_id))
        {
            holder.relative_user_chat.setVisibility(View.VISIBLE);
            holder.relative_sender_chat.setVisibility(GONE);
        }
        else
        {
            holder.relative_user_chat.setVisibility(GONE);
            holder.relative_sender_chat.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount()
    {
        return userchatlist!=null?userchatlist.size():0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
       TextView txt_left_chat,txt_right_chat,txtnodata;
       RelativeLayout relative_sender_chat,relative_user_chat;
       LinearLayout linear_allchats;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            relative_sender_chat=itemView.findViewById(R.id.relative_sender_chat);
            relative_user_chat=itemView.findViewById(R.id.relative_user_chat);
            txt_left_chat=itemView.findViewById(R.id.txt_left_chat);
            txt_right_chat=itemView.findViewById(R.id.txt_right_chat);
        }
    }
}