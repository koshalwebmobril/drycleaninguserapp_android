package com.wm.muggamu.Chat;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.config.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity
{
    @BindView(R.id.recyclerview_chat)
    RecyclerView recyclerview_chat;

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.user_img)
    CircleImageView userImg;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.chat_edittext)
    EditText chatEdittext;
    @BindView(R.id.txt_chat)
    TextView txtChat;
    @BindView(R.id.providername)
    TextView providername;

    @BindView(R.id.nochat)
    TextView nochat;

    String userId,providerId,user_name,user_image,provider_name,provider_image,provider_device_token,notification_token;
    ChatAdapter chatAdapter;
    private DatabaseReference rootReference;
    private DatabaseReference recentDatabaseReference;
    private ArrayList<ChatModel> messageList=new ArrayList<>();
    Handler handler=new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        rootReference = FirebaseDatabase.getInstance().getReference();
        recentDatabaseReference = FirebaseDatabase.getInstance().getReference();
        userId=getIntent().getStringExtra("user_id");
        providerId=getIntent().getStringExtra("provider_id");
        provider_name=getIntent().getStringExtra("provider_name");
        provider_image=getIntent().getStringExtra("provider_img");
        provider_device_token=getIntent().getStringExtra("provider_device_token");
        user_name= LoginPreferences.getActiveInstance(this).getUserName();
        user_image=LoginPreferences.getActiveInstance(this).getUserProfile();
        recyclerview_chat=findViewById(R.id.recyclerview_chat);
        providername.setText(provider_name);
        Glide.with(this).load(provider_image).error(R.drawable.dummy).placeholder(R.drawable.dummy)
                .into(userImg);

        fetchMessages();
        if(messageList.size()==0)
        {
            recyclerview_chat.setVisibility(View.GONE);
            nochat.setVisibility(View.VISIBLE);
        }
        else
        {
            recyclerview_chat.setVisibility(View.VISIBLE);
            nochat.setVisibility(View.GONE);
        }

        chatAdapter = new ChatAdapter(ChatActivity.this,messageList);
        recyclerview_chat.setHasFixedSize(true);
        recyclerview_chat.setItemViewCacheSize(25);
        recyclerview_chat.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        recyclerview_chat.setAdapter(chatAdapter);
    }

    @OnClick({R.id.back, R.id.txt_chat})
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;
            case R.id.txt_chat:
                notification_token = FirebaseInstanceId.getInstance().getToken();
                Log.d("notification_token",notification_token);

                String strMessage="";
                strMessage=chatEdittext.getText().toString().trim();
                if (TextUtils.isEmpty(strMessage))
                {
                    Toast.makeText(ChatActivity.this, "Please enter message", Toast.LENGTH_SHORT).show();
                }
                else if(chatEdittext.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(ChatActivity.this, "No Spaces Allowed", Toast.LENGTH_SHORT).show();
                }
                else
                    {
                        sendMessage(strMessage,"Text");
                    }
                break;
        }
    }

    private void sendMessage(String message,String type) {
        String push_id;
        if (Integer.parseInt(userId) < Integer.parseInt(providerId)) {
            push_id = userId + "-" + providerId;
        } else {
            push_id = providerId + "-" + userId;
        }
        final String message_sender_reference = "messages/" + push_id;
        final String message_receiver_reference = "messages/" + push_id;
        final DatabaseReference user_message_key = rootReference.child("messages").child(push_id).push();
        final String forUid_status = providerId + "_" + "unread";
        final String message_push_id = user_message_key.getKey();
        final Long time = System.currentTimeMillis() / 1000;
        final HashMap<String, Object> message_text_body = new HashMap<>();

        message_text_body.put("message", message);
        message_text_body.put("message_id", message_push_id);
        message_text_body.put("forUid_status", forUid_status);
        message_text_body.put("time", String.valueOf(time));
        message_text_body.put("reciever_id", providerId);
        if (provider_image != null && !provider_image.isEmpty() && !provider_image.equals("null"))
        {
            message_text_body.put("reciever_image", provider_image);
        }
        else
        {
            message_text_body.put("reciever_image", "");
        }
        message_text_body.put("device_token",provider_device_token);
        message_text_body.put("fcm_token",notification_token);
        message_text_body.put("reciever_name",provider_name);
        message_text_body.put("type", type);
        message_text_body.put("sender_name", user_name);
        message_text_body.put("sender_image", user_image);
        message_text_body.put("from", userId);
        message_text_body.put("sender_id", userId);


        final HashMap<String, Object> messageBodyDetails = new HashMap<>();
        messageBodyDetails.put(message_sender_reference + "/" + message_push_id, message_text_body);
        messageBodyDetails.put(message_receiver_reference + "/" + message_push_id, message_text_body);
        chatEdittext.setText("");

        rootReference.updateChildren(messageBodyDetails, (databaseError, databaseReference) ->
        {
            if (databaseError != null){
                Log.e("Sending message", databaseError.getMessage());
            }else {
                //  input_user_message.setText("");
                chatEdittext.setCursorVisible(true);
                chatEdittext.setEnabled(true);
                sendNotification(message,userId,providerId,user_name);
            }
        });
        recentDatabaseReference.child(Constant.RECENT_FIREBASE).child(push_id).setValue(message_text_body);
    }

    private void fetchMessages()
    {
        final String push_id;
        if (Integer.parseInt(userId)<Integer.parseInt(providerId))
        {
            push_id=userId+"-"+providerId;
        }else {
            push_id=providerId+"-"+userId;
        }
        final ArrayList<String> mKeys = new ArrayList<>();
        new Thread(() -> {
            rootReference.child("messages").child(push_id)
                    .addChildEventListener(new ChildEventListener()
                    {
                        @Override
                        public void onChildAdded(@NonNull final DataSnapshot dataSnapshot, String s)
                        {
                            if (dataSnapshot.exists())
                            {
                                handler.post(() ->
                                {
                                    String key = dataSnapshot.getKey();
                                    mKeys.add(key);
                                    ChatModel message = dataSnapshot.getValue(ChatModel.class);
                                    messageList.addAll(Collections.singleton(message));
                                    recyclerview_chat.smoothScrollToPosition(chatAdapter.getItemCount() - 1);

                                    if(messageList.size()>0 || messageList!=null)
                                    {
                                        recyclerview_chat.setVisibility(View.VISIBLE);
                                        nochat.setVisibility(View.GONE);
                                    }
                                    else
                                    {
                                        recyclerview_chat.setVisibility(View.GONE);
                                        nochat.setVisibility(View.VISIBLE);
                                    }
                                    chatAdapter.notifyDataSetChanged();
                                });
                            }
                        }
                        @Override
                        public void onChildChanged(@NonNull final DataSnapshot dataSnapshot, String s) {
                            if (dataSnapshot.exists()) {
                                ChatModel message = dataSnapshot.getValue(ChatModel.class);
                                String key = dataSnapshot.getKey();
                                int index = mKeys.indexOf(key);
                                if (messageList.size() > 0 && index != -1) {
                                    messageList.set(index, message);
                                }
                                chatAdapter.notifyItemChanged(index);
                                // messageAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
        }).start();

    }


    private void sendNotification(String message, String messageSenderId, String messageReceiverID, String username) {
        //createNotificationChannel();

        Log.i("tag", "sendNotification: "+notification_token);
        Log.i("tag", "sendNotification: "+provider_device_token);

        FcmNotificationBuilder.initialize()
                .title(username)
                .message(message)
                .username(username)
                .type("chat")
                .messageFrom("single")
                .uid(String.valueOf(messageSenderId))
                .recieverUid(String.valueOf(messageReceiverID))
                .firebaseToken(notification_token)//sender
                .receiverFirebaseToken(provider_device_token)
                .send();
    }
}