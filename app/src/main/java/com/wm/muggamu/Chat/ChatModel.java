package com.wm.muggamu.Chat;

public class ChatModel
{
    private String forUid_status;
    private String from;
    private String message;
    private String message_id;
    private String reciever_id;
    private String time;
    private String type;
    private String reciever_image;
    private String reciever_name;
    private String sender_name;
    private String sender_image;
    private String fcm_token;
    private String device_token;
    private String sender_id;

    public String getSender_id()
    {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }



    public String getReciever_name() {
        return reciever_name;
    }

    public void setReciever_name(String reciever_name) {
        this.reciever_name = reciever_name;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getSender_image() {
        return sender_image;
    }

    public void setSender_image(String sender_image) {
        this.sender_image = sender_image;
    }

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getReciever_image() {
        return reciever_image;
    }

    public void setReciever_image(String reciever_image) {
        this.reciever_image = reciever_image;
    }

    public String getForUid_status() {
        return forUid_status;
    }

    public void setForUid_status(String forUid_status) {
        this.forUid_status = forUid_status;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getReciever_id() {
        return reciever_id;
    }

    public void setReciever_id(String reciever_id) {
        this.reciever_id = reciever_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
