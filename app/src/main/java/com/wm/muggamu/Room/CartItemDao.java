package com.wm.muggamu.Room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;


import java.util.List;
@Dao
public interface CartItemDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertCustomData(CartItemEntity cartItemEntity);

    @Query("SELECT * FROM cart_item_table")
    public List<CartItemEntity> getAllData();

    @Query("SELECT * FROM cart_item_table GROUP BY parent_id")
    public List<CartItemEntity> getAllCategoryList();

    @Query("SELECT * FROM cart_item_table WHERE unique_id=:childId")
    public boolean getItem(String childId);

    @Query("SELECT child_quantity FROM cart_item_table WHERE unique_id=:child_id")
    public int getItemValue(String child_id);


    /*@Query("SELECT * FROM cart_item_table WHERE child_id=:child_id AND service_id=:service_id")
    public boolean getItem(int child_id,int service_id);

    @Query("SELECT child_quantity FROM cart_item_table WHERE child_id=:child_id AND service_id=:service_id")
    public int getItemValue(int child_id,int service_id);*/


    @Query("SELECT * From cart_item_table WHERE child_quantity IS NULL")
    public boolean getZero();

    @Query("DELETE FROM cart_item_table")
    void emptyList();

    @Query("DELETE from cart_item_table WHERE unique_id=:cartItemEntity ")
    void deleteItem(String cartItemEntity);


    @Query("SELECT parent_id FROM cart_item_table GROUP BY parent_id")
    public List<Integer> getCategoryIdList();

    @Query("SELECT parent_name FROM cart_item_table WHERE parent_id=:parentId")
    public String getParentName(int parentId);

/*    @Query("SELECT * FROM cart_item_table GROUP BY parent_id")
    public List<List<CartItemEntity>> getAllCategoryDataList();*/

    @Query("SELECT child_id FROM cart_item_table WHERE parent_id=:parentId")
    public List<Integer> getChildId(int parentId);

    @Query("SELECT * FROM cart_item_table WHERE parent_id=:parentId")
    public List<CartItemEntity> getChildIdAndQuantity(int parentId);
}
