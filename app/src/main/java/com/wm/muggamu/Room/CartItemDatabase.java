package com.wm.muggamu.Room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {CartItemEntity.class}, version = 8, exportSchema = true)
public abstract class CartItemDatabase extends RoomDatabase
{
    public abstract CartItemDao cartItemDao();
    private static CartItemDatabase instance;
    public static CartItemDatabase getInstance(Context context)
    {
        if(instance == null) instance = Room.databaseBuilder(context, CartItemDatabase.class, "cartDatabase8.db")
                    .allowMainThreadQueries()
                    .build();
        return instance;
    }
}
