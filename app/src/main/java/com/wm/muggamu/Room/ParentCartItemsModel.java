package com.wm.muggamu.Room;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParentCartItemsModel implements Parcelable {

	@SerializedName("category")
	private String category;

	@SerializedName("items")
	private List<CartItemEntity> items;

	public ParentCartItemsModel(Parcel in) {
		category = in.readString();
	}

	public static final Creator<ParentCartItemsModel> CREATOR = new Creator<ParentCartItemsModel>() {
		@Override
		public ParentCartItemsModel createFromParcel(Parcel in) {
			return new ParentCartItemsModel(in);
		}

		@Override
		public ParentCartItemsModel[] newArray(int size) {
			return new ParentCartItemsModel[size];
		}
	};

	public ParentCartItemsModel() {

	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<CartItemEntity> getItems() {
		return items;
	}

	public void setItems(List<CartItemEntity> items) {
		this.items = items;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(category);
	}
}