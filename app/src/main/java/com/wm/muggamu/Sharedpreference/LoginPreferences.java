package com.wm.muggamu.Sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class LoginPreferences
{
    private static com.wm.muggamu.Sharedpreference.LoginPreferences preferences = null;
    private static SharedPreferences mPreferences;
    private SharedPreferences.Editor editor;
    private String stateId;
    private Context context;
    private String userid = "userid";
    private  String company_email="email";
    private  String company_name="name";
    private  String profile_image="profile_image";
    private  String providerid="providerid";
    private  String mobileno="mobileno";
    private  String user_id="user_id";
    private  String page_id="page_id";
    private  String serviceId="service_id";
    private  String categoryId="category_id";
    private  String service_image="service_image";
    private  String service_address="service_address";
    private  String service_name="service_name";






    public LoginPreferences(Context context) {
        this.context = context;
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context));
    }

    public SharedPreferences getmPreferences() {
        return mPreferences;
    }

    private void setmPreferences(SharedPreferences mPreferences) {
        this.mPreferences = mPreferences;
    }

    public static com.wm.muggamu.Sharedpreference.LoginPreferences getActiveInstance(Context context)
    {
        if (preferences == null) {
            preferences = new com.wm.muggamu.Sharedpreference.LoginPreferences(context);
        }
        return preferences;
    }

    public String getToken()
    {
        return mPreferences.getString(this.userid, "");
    }

    public void setToken(String token)
    {
        editor = mPreferences.edit();
        editor.putString(this.userid, token);
        editor.apply();
    }

    public void setUserName(String name)
    {
        editor = mPreferences.edit();
        editor.putString(this.company_name, name);
        editor.apply();
    }

    public String getUserId()
    {
        return mPreferences.getString(this.user_id, "");
    }

    public void setUserId(String user_id)
    {
        editor = mPreferences.edit();
        editor.putString(this.user_id, user_id);
        editor.apply();
    }


    public String getCategoryId()
    {
        return mPreferences.getString(this.categoryId, "");
    }

    public void setCategoryId(String categoryId)
    {
        editor = mPreferences.edit();
        editor.putString(this.categoryId, categoryId);
        editor.apply();
    }

    public String getUserName()
    {
        return mPreferences.getString(this.company_name, "");
    }

    public void setUserEmail(String email)
    {
        editor = mPreferences.edit();
        editor.putString(this.company_email, email);
        editor.apply();
    }

    public String getUserEmail()
    {
        return mPreferences.getString(this.company_email, "");
    }

    public static void deleteAllPreference()
    {
        mPreferences.edit().clear().apply();
    }

    public static void deleteoneitem(String selectedkey)
    {
        mPreferences.edit().remove(selectedkey).apply();
    }


    public String getUserProfile()
    {
        return mPreferences.getString(this.profile_image, "");
    }
    public void setUserProfile(String profile_image)
    {
        editor = mPreferences.edit();
        editor.putString(this.profile_image, profile_image);
        editor.apply();
    }

    public String getService_address()
    {
        return mPreferences.getString(this.service_address, "");
    }
    public void setService_address(String service_address)
    {
        editor = mPreferences.edit();
        editor.putString(this.service_address, service_address);
        editor.apply();
    }


    public String getProviderId()
    {
        return mPreferences.getString(this.providerid, "");
    }


    public void setProviderId(String providerid)
    {
        editor = mPreferences.edit();
        editor.putString(this.providerid, providerid);
        editor.apply();
    }

    public String getServiceId()
    {
        return mPreferences.getString(this.serviceId, "");
    }


    public void setServiceId(String serviceId)
    {
        editor = mPreferences.edit();
        editor.putString(this.serviceId, serviceId);
        editor.apply();
    }


    public String getService_name()
    {
        return mPreferences.getString(this.service_name, "");
    }

    public void setService_name(String service_name)
    {
        editor = mPreferences.edit();
        editor.putString(this.service_name, service_name);
        editor.apply();
    }

    public String getMobileno()
    {
        return mPreferences.getString(this.mobileno, "");
    }


    public void setMobileno(String mobileno)
    {
        editor = mPreferences.edit();
        editor.putString(this.mobileno, mobileno);
        editor.apply();
    }


    public String getPageId()
    {
        return mPreferences.getString(this.page_id, "");
    }


    public void setPageId(String page_id)
    {
        editor = mPreferences.edit();
        editor.putString(this.page_id, page_id);
        editor.apply();
    }

    public String getService_image()
    {
        return mPreferences.getString(this.service_image, "");
    }


    public void setService_image(String service_image)
    {
        editor = mPreferences.edit();
        editor.putString(this.service_image, service_image);
        editor.apply();
    }



}
