package com.wm.muggamu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamu.Interface.ChooseClothListener;
import com.wm.muggamu.Models.addpostmodel.AddPostChildsModel;
import com.wm.muggamu.R;
import com.wm.muggamu.RoomAddPost.AddPostDatabase;
import com.wm.muggamu.RoomAddPost.AddPostEntity;

import java.util.List;

public class AddPostChildAdapter extends RecyclerView.Adapter<AddPostChildAdapter.MyViewHolder> {
    List<AddPostChildsModel> itemslist;
    Context context;
    ChooseClothListener chooseClothListener;
    int parentPosition;
    String service_id,service_name;
    public AddPostChildAdapter(Context context, int parentPosition, List<AddPostChildsModel> itemslist,String service_id,String service_name) {
        this.context = context;
        this.itemslist = itemslist;
        this.parentPosition = parentPosition;
        this.service_id = service_id;
        this.service_name = service_name;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        AddPostChildsModel ItemsItem = itemslist.get(position);
        holder.item_name.setText(ItemsItem.getItemName());

        if(AddPostDatabase.getInstance(context).addPostDao().getItem(Integer.parseInt(service_id)+"-"+ ItemsItem.getChildId()))
        {
            holder.item_count.setText(""+AddPostDatabase.getInstance(context)
                    .addPostDao().getItemValue(Integer.parseInt(service_id)+"-"+ ItemsItem.getChildId()));
        }
        else
        {
            holder.item_count.setText("0");
        }

        holder.relative_plus.setOnClickListener(new View.OnClickListener()
        {
                @Override
                public void onClick(View v)
                {
                     int count= Integer.parseInt(holder.item_count.getText().toString().trim());
                     count=count+1;
                     holder.item_count.setText(String.valueOf(count));

                    AddPostEntity cartItemEntity=new AddPostEntity();
                    cartItemEntity.setService_id(Integer.parseInt(service_id));
                    cartItemEntity.setChild_id(ItemsItem.getChildId());
                    cartItemEntity.setUnique_id(Integer.parseInt(service_id)+"-"+ ItemsItem.getChildId());

                    cartItemEntity.setChild_quantity(count);
                    cartItemEntity.setParent_id(Integer.parseInt(service_id));
                    cartItemEntity.setItem_name(String.valueOf(ItemsItem.getItemName()));
                    cartItemEntity.setParent_name(service_name);
                    AddPostDatabase.getInstance(context).addPostDao().insertCustomData(cartItemEntity);
                }
            });

           holder.relative_minus.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    int count= Integer.parseInt(holder.item_count.getText().toString().trim());
                    if(count>0)
                    {
                        count=count-1;
                        holder.item_count.setText(String.valueOf(count));

                        AddPostEntity cartItemEntity=new AddPostEntity();
                        cartItemEntity.setService_id(Integer.parseInt(service_id));
                        cartItemEntity.setChild_id(ItemsItem.getChildId());
                        cartItemEntity.setUnique_id(Integer.parseInt(service_id)+"-"+ ItemsItem.getChildId());

                        cartItemEntity.setChild_quantity(count);
                        cartItemEntity.setParent_id(Integer.parseInt(service_id));
                        cartItemEntity.setItem_name(String.valueOf(ItemsItem.getItemName()));
                        cartItemEntity.setParent_name(service_name);
                        AddPostDatabase.getInstance(context).addPostDao().insertCustomData(cartItemEntity);
                        if(count==0)
                        {
                            AddPostDatabase.getInstance(context).addPostDao().deleteItem(Integer.parseInt(service_id) +"-"+ ItemsItem.getChildId());
                        }
                    }
                }
            });
    }

    @Override
    public int getItemCount() {
        return itemslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView item_name;
        TextView item_count;
        RelativeLayout relative_minus,relative_plus;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            item_name = itemView.findViewById(R.id.item_name);
            item_count = itemView.findViewById(R.id.item_count);
            relative_minus=itemView.findViewById(R.id.relative_minus);
            relative_plus=itemView.findViewById(R.id.relative_plus);
        }
    }
}