package com.wm.muggamu.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamu.Activities.AppoimentDetailsActivity;
import com.wm.muggamu.Activities.OrderDetailsActivity;
import com.wm.muggamu.Models.yourbookingmodel.BookingsItemModel;
import com.wm.muggamu.R;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class YourBookingAdapter extends RecyclerView.Adapter<YourBookingAdapter.MyViewHolder>
{
    Context context;
    List<BookingsItemModel> bookingitemlist;
    public YourBookingAdapter(Context context, List bookingitemlist)
    {
        this.context = context;
        this.bookingitemlist=bookingitemlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_your_booking, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        BookingsItemModel allbookingmodel = bookingitemlist.get(position);
        Glide.with(context).load(allbookingmodel.getProviderImage()).apply(new RequestOptions().placeholder(R.drawable.clothes_img).error(R.drawable.clothes_img)).into(holder.clothes);
        holder.provider_name.setText(StringUtils.capitalize(allbookingmodel.getProviderName()));

        if(allbookingmodel.getBookingType()==1)
        {
            holder.booking_date.setText(allbookingmodel.getDeliveryDate() +","+allbookingmodel.getDeliveryTime());
        }
        else
        {
            holder.booking_date.setText(allbookingmodel.getDate() +","+allbookingmodel.getTime());
        }

        if(allbookingmodel.getAppointmentId().equals(""))
        {
            holder.oreder_no.setText(context.getString(R.string.orderno)+"-"+" "+allbookingmodel.getOrderId());
        }
        else
        {
            holder.oreder_no.setText(context.getString(R.string.orderno)+"-"+" "+allbookingmodel.getAppointmentId());
        }

        if(allbookingmodel.getTotalAmount().equals(""))
        { }
        else
        {
            holder.linear_price.setVisibility(View.VISIBLE);
            holder.total_amount.setText(context.getString(R.string.currency_icon)+ " "+ String.valueOf(allbookingmodel.getTotalAmount()));
            holder.txtpices.setText(context.getString(R.string.pices) +" "+String.valueOf(allbookingmodel.getPieces()));
        }
        holder.itemView.setOnClickListener(view ->
        {
            if(allbookingmodel.getBookingType()==1)
            {
                Intent i=new Intent (context, OrderDetailsActivity.class);
                i.putExtra("booking_id",String.valueOf(allbookingmodel.getId()));
                i.putExtra("order_no",allbookingmodel.getProviderName());
                i.putExtra("booking_date",allbookingmodel.getDeliveryDate()+","+allbookingmodel.getDeliveryTime());
                i.putExtra("total_amount",holder.total_amount.getText().toString().trim());
                i.putExtra("pices",holder.txtpices.getText().toString().trim());
                i.putExtra("provider_name",holder.provider_name.getText().toString().trim());
                i.putExtra("getbookingtype","1");
                context.startActivity(i);
            }
            else
            {
                Intent i=new Intent (context, AppoimentDetailsActivity.class);
                i.putExtra("appoiment_id",String.valueOf(allbookingmodel.getId()));
                i.putExtra("getbookingtype","2");
                i.putExtra("booking_date",String.valueOf(allbookingmodel.getDate().trim()));
                context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return bookingitemlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView provider_name,oreder_no,booking_date,created_date,txtpices,total_amount;
        ImageView clothes;
        LinearLayout linear_price;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            clothes=itemView.findViewById(R.id.clothes);
            provider_name=itemView.findViewById(R.id.provider_name);
            oreder_no =itemView.findViewById(R.id.oreder_no_);
            booking_date=itemView.findViewById(R.id.booking_date);
            txtpices=itemView.findViewById(R.id.txtpices);
            total_amount=itemView.findViewById(R.id.total_amount);
            linear_price=itemView.findViewById(R.id.linear_price);
        }
    }
}