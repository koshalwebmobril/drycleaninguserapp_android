package com.wm.muggamu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamu.R;
import com.wm.muggamu.Room.CartItemEntity;

import java.util.List;
public class CartsChildAdapter extends RecyclerView.Adapter<CartsChildAdapter.MyViewHolder>
{
    List<CartItemEntity> itemslist;
    Context context;
    public CartsChildAdapter(Context context, List itemslist)
    {
        this.context = context;
        this.itemslist = itemslist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cartschilditems, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        CartItemEntity ItemsItem = itemslist.get(position);
        holder.txt_qty.setText(String.valueOf(ItemsItem.getChild_quantity()));
        holder.txtprice.setText(context.getString(R.string.currency_icon)+" "+String.valueOf(ItemsItem.getChild_quantity() * ItemsItem.getQuantity_price()));
        holder.txtitemname.setText("X "+ItemsItem.getItem_name());


    }

    @Override
    public int getItemCount()
    {
        return itemslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_qty,txtprice,txtitemname;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            txt_qty=itemView.findViewById(R.id.txt_qty);
            txtitemname=itemView.findViewById(R.id.txtitemname);
            txtprice=itemView.findViewById(R.id.txtprice);


        }
    }
}