package com.wm.muggamu.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.squareup.picasso.Picasso;
import com.wm.muggamu.Models.homepagemodel.SlidersItemModel;
import com.wm.muggamu.R;

import java.util.ArrayList;

public class PagerAdapterSlider extends PagerAdapter {
    private ArrayList<SlidersItemModel> postBeanses = new ArrayList<>();
    private Context mContext;


    public PagerAdapterSlider(Context mContext, ArrayList<SlidersItemModel> modelArrayList) {
        this.mContext = mContext;
        this.postBeanses = modelArrayList;
    }


    @Override
    public int getCount() {
        return postBeanses.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((LinearLayout) object);
    }

    @NonNull
    @SuppressLint("CheckResult")
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        final int pos = position;
        final SlidersItemModel feedItem = postBeanses.get(position);

        View itemView = LayoutInflater.from(mContext).inflate(R.layout.deals_item, container, false);


        AppCompatImageView imageView = itemView.findViewById(R.id.ivBanner);
        if (mContext!=null) {
            Picasso.get().load(feedItem.getPath()).into(imageView);
        }
        ((ViewPager) container).addView(itemView, 0);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
        Log.e("destroyItem", "call");

    }
}
