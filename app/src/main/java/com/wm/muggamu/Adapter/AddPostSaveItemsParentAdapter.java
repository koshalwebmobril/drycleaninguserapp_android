package com.wm.muggamu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamu.R;
import com.wm.muggamu.RoomAddPost.AddPostEntity;

import java.util.List;

public class AddPostSaveItemsParentAdapter extends RecyclerView.Adapter<AddPostSaveItemsParentAdapter.MyViewHolder>
{
    Context context;
    List<AddPostEntity> cartitemlist;

    public AddPostSaveItemsParentAdapter(Context context, List cartitemlist)
    {
        this.context = context;
        this.cartitemlist=cartitemlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bid_save_child, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         context = holder.itemView.getContext();
         AddPostEntity parentmodel = cartitemlist.get(position);
         holder.service_name.setText(String.valueOf(parentmodel.getParent_name()));
         holder.txt_qty.setText(String.valueOf(parentmodel.getChild_quantity()) +" "+"x"+" "+ String.valueOf(parentmodel.getItem_name()));

         if(position>0)
         {
             if(holder.service_name.getText().toString().trim().equals(cartitemlist.get(position-1).getParent_name()))
             {
                 holder.linear_parent_name.setVisibility(View.GONE);
             }
         }
         else
         {
             holder.linear_parent_name.setVisibility(View.VISIBLE);
         }
    }
    @Override
    public int getItemCount()
    {
        return cartitemlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_name,txt_qty,txtitemname;
        LinearLayout linear_parent_name;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            service_name=itemView.findViewById(R.id.service_name);
            txt_qty=itemView.findViewById(R.id.txt_qty);
            txtitemname=itemView.findViewById(R.id.txtitemname);
            linear_parent_name=itemView.findViewById(R.id.linear_parent_name);
        }
    }
}