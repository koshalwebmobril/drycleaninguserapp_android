package com.wm.muggamu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamu.R;
import com.wm.muggamu.Room.ParentCartItemsModel;

import java.util.List;

public class CartParentAdapter extends RecyclerView.Adapter<CartParentAdapter.MyViewHolder>
{
    Context context;
    List<ParentCartItemsModel> cartitemlist;
    String total_amount,delivery_fee;
    public CartParentAdapter(Context context, List cartitemlist, String total_amount, String delivery_fee)
    {
        this.context = context;
        this.cartitemlist=cartitemlist;
        this.total_amount=total_amount;
        this.delivery_fee=delivery_fee;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_items, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         context = holder.itemView.getContext();
         ParentCartItemsModel parentmodel = cartitemlist.get(position);
         holder.service_name.setText(String.valueOf(parentmodel.getCategory()));
         holder.txt_total_amount.setText(total_amount);

        holder.recycler_child.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        CartsChildAdapter cartsChildAdapter = new CartsChildAdapter(context,parentmodel.getItems());
        holder.recycler_child.setAdapter(cartsChildAdapter);

        /* if(position>0)
         {
          if(holder.service_name.getText().toString().trim().equals(cartitemlist.get(position-1).getParent_name()))
             {
                 holder.linear_parent_name.setVisibility(View.GONE);
             }
         }
         else
         {
             holder.linear_parent_name.setVisibility(View.VISIBLE);
         }*/

        if(position==cartitemlist.size()-1)
         {
             holder.relative_total_amount.setVisibility(View.VISIBLE);
             holder.txt_total_amount.setText(context.getString(R.string.currency_icon)+" "+total_amount);
             holder.relativedeliverfee.setVisibility(View.VISIBLE);
             holder.txt_deliveryamount.setText(context.getString(R.string.currency_icon)+" "+delivery_fee);
         }
         else
         {
             holder.relative_total_amount.setVisibility(View.GONE);
             holder.relativedeliverfee.setVisibility(View.GONE);
         }
    }
    @Override
    public int getItemCount()
    {
        return cartitemlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_name,txt_total_amount,txt_deliveryamount;
        RelativeLayout relativedeliverfee,relative_total_amount;
        RecyclerView recycler_child;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            service_name=itemView.findViewById(R.id.service_name);
            txt_total_amount=itemView.findViewById(R.id.total_amount);
            recycler_child=itemView.findViewById(R.id.recycler_child);
            relativedeliverfee=itemView.findViewById(R.id.relativedeliverfee);
            relative_total_amount=itemView.findViewById(R.id.relative_total_amount);
            txt_deliveryamount=itemView.findViewById(R.id.txt_deliveryamount);
        }
    }
}