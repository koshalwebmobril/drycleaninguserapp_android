package com.wm.muggamu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamu.Interface.ChooseClothListener;
import com.wm.muggamu.Models.addpostmodel.AddPostParentModel;
import com.wm.muggamu.R;

import java.util.ArrayList;
import java.util.List;

public class AddPostParentAdapter extends RecyclerView.Adapter<AddPostParentAdapter.MyViewHolder>{
    List<AddPostParentModel> list;
    Context context;
    ChooseClothListener chooseClothListener;
    String service_id,service_name;
    public AddPostParentAdapter(Context context, List<AddPostParentModel> list,String service_id,String service_name)
    {
        this.context = context;
        this.list = list;
        this.service_id=service_id;
        this.service_name=service_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_service_subitems, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        AddPostParentModel subitems = list.get(position);
        holder.category_name.setText(subitems.getCategory());

        holder.arrow_up.setOnClickListener(new View.OnClickListener()
         {
            @Override
            public void onClick(View v)
            {
                holder.arrow_up.setVisibility(View.GONE);
                holder.arrow_down.setVisibility(View.VISIBLE);
                holder.recycle_child.setVisibility(View.GONE);
            }
        });

        holder.arrow_down.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                holder.recycle_child.setVisibility(View.VISIBLE);
                holder.arrow_down.setVisibility(View.GONE);
                holder.arrow_up.setVisibility(View.VISIBLE);
            }
        });
        holder.recycle_child.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        AddPostChildAdapter subitemchildadapter = new AddPostChildAdapter(context,position, (ArrayList) subitems.getItems(),service_id,service_name);
        holder.recycle_child.setAdapter(subitemchildadapter);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView category_name;
        ImageView arrow_up,arrow_down;
        RecyclerView recycle_child;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            category_name=itemView.findViewById(R.id.category_name);
            recycle_child =itemView.findViewById(R.id.recycle_child);
            arrow_down=itemView.findViewById(R.id.arrow_down);
            arrow_up=itemView.findViewById(R.id.arrow_up);
        }
    }
}