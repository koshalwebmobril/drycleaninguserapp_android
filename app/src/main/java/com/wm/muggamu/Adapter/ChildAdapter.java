package com.wm.muggamu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.wm.muggamu.Interface.ChooseClothListener;
import com.wm.muggamu.Models.servicesubitemsmodel.ChildItemsModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Room.CartItemDatabase;
import com.wm.muggamu.Room.CartItemEntity;

import java.util.List;

public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.MyViewHolder> {
    List<ChildItemsModel> itemslist;
    Context context;
    ChooseClothListener chooseClothListener;
    int parentPosition;


    public ChildAdapter(Context context, int parentPosition, List<ChildItemsModel> itemslist) {
        this.context = context;
        this.itemslist = itemslist;
        this.parentPosition = parentPosition;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        ChildItemsModel ItemsItem = itemslist.get(position);
        holder.item_name.setText(ItemsItem.getItemName() +" "+ "(" +" "+ context.getString(R.string.currency_icon) +ItemsItem.getPrice() +" " + ")");
      /*  if(CartItemDatabase.getInstance(context).cartItemDao().getItem(ItemsItem.getSubitemId(),ItemsItem.getServiceId()))
        {
            holder.item_count.setText(""+CartItemDatabase.getInstance(context)
                    .cartItemDao().getItemValue(ItemsItem.getSubitemId(),ItemsItem.getServiceId()));
        }
        else
            {
                holder.item_count.setText("0");
            }*/

        if(CartItemDatabase.getInstance(context).cartItemDao().getItem(ItemsItem.getServiceId()+"-"+ ItemsItem.getSubitemId()))
        {
            holder.item_count.setText(""+CartItemDatabase.getInstance(context)
                    .cartItemDao().getItemValue(ItemsItem.getServiceId()+"-"+ ItemsItem.getSubitemId()));
        }

        else
            {
                holder.item_count.setText("0");
            }

        holder.relative_plus.setOnClickListener(new View.OnClickListener()
        {
                @Override
                public void onClick(View v)
                {
                     int count= Integer.parseInt(holder.item_count.getText().toString().trim());
                     count=count+1;
                     holder.item_count.setText(String.valueOf(count));
                     CartItemEntity cartItemEntity=new CartItemEntity();
                     cartItemEntity.setService_id(ItemsItem.getServiceId());
                     cartItemEntity.setChild_id(ItemsItem.getSubitemId());
                     cartItemEntity.setUnique_id(ItemsItem.getServiceId()+"-"+ ItemsItem.getSubitemId());


                     cartItemEntity.setChild_quantity(count);
                    cartItemEntity.setParent_id(ItemsItem.getServiceId());
                    cartItemEntity.setQuantity_price(ItemsItem.getPrice());
                    cartItemEntity.setItem_name(String.valueOf(ItemsItem.getItemName()));
                    cartItemEntity.setParent_name(ItemsItem.getService_name());

                    CartItemDatabase.getInstance(context).cartItemDao().insertCustomData(cartItemEntity);
                }
            });

            holder.relative_minus.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    int count= Integer.parseInt(holder.item_count.getText().toString().trim());
                    if(count>0)
                    {
                        count=count-1;
                        holder.item_count.setText(String.valueOf(count));
                        CartItemEntity cartItemEntity=new CartItemEntity();
                        cartItemEntity.setService_id(ItemsItem.getServiceId());
                        cartItemEntity.setChild_id(ItemsItem.getSubitemId());
                        cartItemEntity.setUnique_id(ItemsItem.getServiceId()+"-"+ ItemsItem.getSubitemId());

                        cartItemEntity.setChild_quantity(count);
                        cartItemEntity.setParent_id(ItemsItem.getServiceId());
                        cartItemEntity.setQuantity_price(ItemsItem.getPrice());
                        cartItemEntity.setItem_name(String.valueOf(ItemsItem.getItemName()));
                        cartItemEntity.setParent_name(ItemsItem.getService_name());
                        CartItemDatabase.getInstance(context).cartItemDao().insertCustomData(cartItemEntity);
                        if(count==0)
                        {
                            CartItemDatabase.getInstance(context).cartItemDao().deleteItem(ItemsItem.getServiceId()+"-"+ ItemsItem.getSubitemId());
                        }
                    }
                }
            });
    }

    @Override
    public int getItemCount() {
        return itemslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView item_name;
        TextView item_count;
        RelativeLayout relative_minus,relative_plus;

        public MyViewHolder(View itemView) {
            super(itemView);
            item_name = itemView.findViewById(R.id.item_name);
            item_count = itemView.findViewById(R.id.item_count);
            relative_minus=itemView.findViewById(R.id.relative_minus);
            relative_plus=itemView.findViewById(R.id.relative_plus);
        }
    }
}