package com.wm.muggamu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamu.Models.orderdetailsmodel.OrderDetailsModel;
import com.wm.muggamu.R;

import java.util.ArrayList;
import java.util.List;

public class OrderItemDetailsParentAdapter extends RecyclerView.Adapter<OrderItemDetailsParentAdapter.MyViewHolder>
{
    Context context;
    List<OrderDetailsModel> orderdetailslist;
    public OrderItemDetailsParentAdapter(Context context, List orderdetailslist)
    {
        this.context = context;
        this.orderdetailslist=orderdetailslist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_details_parent, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         context = holder.itemView.getContext();
         OrderDetailsModel reviewlist = orderdetailslist.get(position);
         holder.service_name.setText(String.valueOf(reviewlist.getServiceName()));

        holder.recycler_child.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        OrderDetailsChildAdapter orderDetailsChildAdapter = new OrderDetailsChildAdapter(context,(ArrayList) reviewlist.getItems());
        holder.recycler_child.setAdapter(orderDetailsChildAdapter);
    }
    @Override
    public int getItemCount()
    {
        return orderdetailslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_name;
        RecyclerView recycler_child;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            service_name=itemView.findViewById(R.id.service_name);
            recycler_child=itemView.findViewById(R.id.recycler_child);
        }
    }
}