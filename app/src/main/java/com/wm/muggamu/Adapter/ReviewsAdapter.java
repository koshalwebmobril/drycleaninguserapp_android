package com.wm.muggamu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamu.Models.reviewmodel.ReviewListItemModel;
import com.wm.muggamu.R;

import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder>
{
    Context context;
    List<ReviewListItemModel> reviewlistmodel;
    public ReviewsAdapter(Context context,List reviewlistmodel)
    {
        this.context = context;
        this.reviewlistmodel=reviewlistmodel;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reviews, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         context = holder.itemView.getContext();
         ReviewListItemModel reviewlist = reviewlistmodel.get(position);

         Glide.with(context).load(reviewlist.getUserProfileImage()).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile)).into(holder.user_profile);
         holder.username.setText(reviewlist.getUserName());
         holder.time.setText(reviewlist.getCreatedAt());
         holder.service_name.setText(reviewlist.getBookingServiceName());
         holder.txtlandary_date.setText(reviewlist.getBookingDate()+","+ reviewlist.getBookingTime());
       //  holder.txt_time.setText(reviewlist.getBookingTime());
         holder.comments.setText(reviewlist.getReview());
         holder.ratingbar.setRating(Float.parseFloat(reviewlist.getRating()));




        /* holder.itemView.setOnClickListener(view ->
        {
            Intent i=new Intent (context, ParticularServiceProvider.class);
            context.startActivity(i);
        });*/
    }
    @Override
    public int getItemCount()
    {
        return reviewlistmodel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView username,time,service_name,txtlandary_date,txt_time,comments;
        ImageView user_profile;
        RatingBar ratingbar;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            username=itemView.findViewById(R.id.username);
            time=itemView.findViewById(R.id.time_confirmed);
            service_name=itemView.findViewById(R.id.service_name);
            txtlandary_date=itemView.findViewById(R.id.txtlandary_date);
         //   txt_time=itemView.findViewById(R.id.txt_time);
            comments=itemView.findViewById(R.id.comments);
            user_profile=itemView.findViewById(R.id.provider_profile);
            ratingbar=itemView.findViewById(R.id.ratingbar);

        }
    }
}