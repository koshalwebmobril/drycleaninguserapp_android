package com.wm.muggamu.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wm.muggamu.Interface.AcceptRejectBidListner;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.allpostmodel.BidsResultModel;

import java.util.List;

public class BidsResultAdapter extends RecyclerView.Adapter<BidsResultAdapter.MyViewHolder> {
    Context context;
    List<BidsResultModel> bidsresultModelList;
    AcceptRejectBidListner acceptRejectBidListner;
    public BidsResultAdapter(Context context, List bidsresultModelList, AcceptRejectBidListner acceptRejectBidListner)
    {
        this.context = context;
        this.bidsresultModelList = bidsresultModelList;
        this.acceptRejectBidListner = acceptRejectBidListner;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bid_details, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        BidsResultModel bidsresultlist = bidsresultModelList.get(position);

        Glide.with(context).load(bidsresultlist.getProfileImage()).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(holder.profile_image1);
        holder.bid_amount.setText(String.valueOf("Amount : " +" "+context.getString(R.string.currency_icon)+ bidsresultlist.getBidAmount()));
        holder.username1.setText(bidsresultlist.getProviderName());
        holder.bid_time.setText(bidsresultlist.getCreatedAt());



        if(bidsresultlist.getUserAction()!=null)
        {
            if(bidsresultlist.getUserAction().equals("reject"))
            {
                holder.txt_accept_reject_message.setVisibility(View.VISIBLE);
                holder.txt_accept_reject_message.setTextColor(ContextCompat.getColor(context, R.color.reject_color));
                holder.txt_accept_reject_message.setText(context.getString(R.string.you_already_reject_text));
                holder.txt_accept.setVisibility(View.GONE);
                holder.txt_reject.setVisibility(View.GONE);
            }
            else if(bidsresultlist.getUserAction().equals("accept"))
            {

                holder.txt_accept_reject_message.setVisibility(View.VISIBLE);
                holder.txt_accept_reject_message.setText(context.getString(R.string.you_already_accept_text));
                holder.txt_accept_reject_message.setTextColor(ContextCompat.getColor(context, R.color.accepted_color));
                holder.txt_accept.setVisibility(View.GONE);
                holder.txt_reject.setVisibility(View.GONE);

                /* holder.txt_reject.setVisibility(View.INVISIBLE);
                holder.txt_accept.setText(context.getString(R.string.txt_accepted));
                holder.txt_accept.setTextColor(ContextCompat.getColor(context, R.color.accept_color));*/
            }
        }
        else
        {
            holder.txt_accept.setVisibility(View.VISIBLE);
            holder.txt_reject.setVisibility(View.VISIBLE);
            holder.txt_accept_reject_message.setVisibility(View.GONE);
        }

        holder.txt_accept.setOnClickListener(view ->
        {
          //  Toast.makeText(context, "accept", Toast.LENGTH_SHORT).show();
            acceptRejectBidListner.acceptreject(bidsresultlist.getPostId(),bidsresultlist.getId(),1);
        });

        holder.txt_reject.setOnClickListener(view ->
        {
            acceptRejectBidListner.acceptreject(bidsresultlist.getPostId(),bidsresultlist.getId(),0);
        });
    }

    @Override
    public int getItemCount() {
        return bidsresultModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
       ImageView profile_image1;
       TextView username1,bid_time,bid_amount,txt_accept,txt_reject,txt_accept_reject_message;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            username1=itemView.findViewById(R.id.username1);
            bid_time=itemView.findViewById(R.id.bid_time);
            profile_image1=itemView.findViewById(R.id.profile_image1);
            bid_amount=itemView.findViewById(R.id.bid_amount);
            txt_accept=itemView.findViewById(R.id.txt_accept);
            txt_reject=itemView.findViewById(R.id.txt_reject);
            txt_accept_reject_message=itemView.findViewById(R.id.txt_accept_reject_message);
        }
    }
}