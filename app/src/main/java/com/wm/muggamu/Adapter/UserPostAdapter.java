package com.wm.muggamu.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wm.muggamu.Activities.PostDetailsActivity;
import com.wm.muggamu.Models.userallpostmodel.AllPostModel;
import com.wm.muggamu.Interface.OnClickRemovePost;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.Singleton;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserPostAdapter extends RecyclerView.Adapter<UserPostAdapter.MyViewHolder> {
    Context context;
    ImageView menu_icon;
    List<AllPostModel> allPostModelList;
    OnClickRemovePost onClickRemovePost;
    Singleton singleton= Singleton.getInstance();

    public UserPostAdapter(Context context, List allPostModelList,OnClickRemovePost onClickRemovePost) {
        this.context = context;
        this.allPostModelList = allPostModelList;
        this.onClickRemovePost=onClickRemovePost;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_post, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,int position) {
        context = holder.itemView.getContext();
        AllPostModel allpostmodellist = allPostModelList.get(position);
        Glide.with(context).load(LoginPreferences.getActiveInstance(context).getUserProfile()).error(R.drawable.dummy_icon).placeholder(R.drawable.dummy_icon)
                .thumbnail(0.06f)
                .into(holder.profileImage);

        Glide.with(context).load(allpostmodellist.getPostAttachment()).error(R.drawable.clothes_img)
                .placeholder(R.drawable.clothes_img)
                .thumbnail(0.06f)
                .into(holder.serviceImg);


        holder.userName.setText(LoginPreferences.getActiveInstance(context).getUserName());
        holder.serviceName.setText(allpostmodellist.getServiceName());
        holder.createdTime.setText(allpostmodellist.getCreatedAt());
        holder.serviceDateTime.setText(allpostmodellist.getServiceDate() +  " at" + " "+allpostmodellist.getServiceTime());
        holder.service_price.setText(context.getString(R.string.currency_icon) +" "+ String.format("%.2f",allpostmodellist.getServiceAmount()));

        holder.itemView.setOnClickListener(view ->
        {
            Intent i=new Intent (context, PostDetailsActivity.class);
            i.putExtra("PostDetailsItem", (Serializable) allpostmodellist.getPostDetails());
            i.putExtra("service_name",allpostmodellist.getServiceName());
            i.putExtra("created_at",allpostmodellist.getCreatedAt());
            i.putExtra("service_day_time",allpostmodellist.getServiceDate());
            i.putExtra("service_time",holder.serviceDateTime.getText().toString().trim());
            i.putExtra("service_address",allpostmodellist.getServiceAddress());
            i.putExtra("service_remarks",String.valueOf(allpostmodellist.getRemarks()));
            i.putExtra("service_price",holder.service_price.getText().toString());
            i.putExtra("image",allpostmodellist.getPostAttachment());
            i.putExtra("post_id",String.valueOf(allpostmodellist.getId()));
            context.startActivity(i);
        });

        menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                PopupMenu popup = new PopupMenu(context, v, Gravity.END);
                popup.inflate(R.menu.poupup_menu);

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        switch (item.getItemId())
                        {
                            case R.id.remove:
                                onClickRemovePost.removepost(allPostModelList.get(position).getId(),position);
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return allPostModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.profile_image)
        CircleImageView profileImage;
        @BindView(R.id.user_name)
        TextView userName;
        @BindView(R.id.menu_icon)
        ImageView menuIcon;
        @BindView(R.id.service_img)
        ImageView serviceImg;
        @BindView(R.id.service_name)
        TextView serviceName;
        @BindView(R.id.created_time)
        TextView createdTime;

        @BindView(R.id.service_time)
        TextView servicetime;

        @BindView(R.id.service_price)
        TextView service_price;

        @BindView(R.id.service_date_time)
        TextView serviceDateTime;

        @BindView(R.id.linear_recycler)
        LinearLayout linear_recycler;



        public MyViewHolder(View itemView) {
            super(itemView);
            menu_icon = itemView.findViewById(R.id.menu_icon);
            profileImage=itemView.findViewById(R.id.profile_image);
            serviceImg=itemView.findViewById(R.id.service_img);
            serviceName=itemView.findViewById(R.id.service_name);
            userName=itemView.findViewById(R.id.user_name);
            createdTime=itemView.findViewById(R.id.created_time);
            servicetime=itemView.findViewById(R.id.service_time);
            service_price=itemView.findViewById(R.id.service_price);
            linear_recycler=itemView.findViewById(R.id.linear_recycler);
            serviceDateTime=itemView.findViewById(R.id.service_date_time);
        }
    }
}