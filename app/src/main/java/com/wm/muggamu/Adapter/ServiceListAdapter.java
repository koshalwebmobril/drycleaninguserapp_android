package com.wm.muggamu.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.wm.muggamu.Activities.ChooseServiceItemsActivity;
import com.wm.muggamu.Models.getservicesmodel.ServicesItemModel;
import com.wm.muggamu.R;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.MyViewHolder>
{
    Context context;
    List<ServicesItemModel> provideritemdetailslist;

    public ServiceListAdapter(Context context, List provideritemdetailslist)
    {
        this.context = context;
        this.provideritemdetailslist=provideritemdetailslist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_services_provider, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        ServicesItemModel itemsdetails = provideritemdetailslist.get(position);
        Picasso.get().load(itemsdetails.getThumbnailPath()).placeholder(R.drawable.image)// Place holder image from drawable folder
                .error(R.drawable.image).resize(100, 100)
                .into(holder.clothes);
        holder.service_name.setText(StringUtils.capitalize(itemsdetails.getName()));
        holder.relative_items.setOnClickListener(view ->
        {
              Intent i=new Intent (context, ChooseServiceItemsActivity.class);
              i.putExtra("service_id",String.valueOf(itemsdetails.getserviceId()));
              i.putExtra("provider_id",String.valueOf(itemsdetails.getProviderId()));
              context.startActivity(i);
        });
    }
    @Override
    public int getItemCount()
    {
        return provideritemdetailslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView service_name;
        ImageView clothes;
        RelativeLayout relative_items;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            clothes=itemView.findViewById(R.id.clothes);
            service_name=itemView.findViewById(R.id.service_name);
            relative_items=itemView.findViewById(R.id.relative_items);
        }
    }
}