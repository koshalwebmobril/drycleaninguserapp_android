package com.wm.muggamu.Adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamu.Interface.AddressListner;
import com.wm.muggamu.Interface.OnClickRemoveAddress;
import com.wm.muggamu.Models.getaddressmodel.UserAddressModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;

import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {
    Context context;
    List<UserAddressModel> useraddressmodellist;
    ImageView menu_icon;
    OnClickRemoveAddress onClickRemoveAddress;
    String page_status;
    AddressListner addressListner;

    public AddressAdapter(Context context, List useraddressmodellist,OnClickRemoveAddress onClickRemoveAddress,String page_status,AddressListner addressListner)
    {
        this.context = context;
        this.useraddressmodellist = useraddressmodellist;
        this.onClickRemoveAddress=onClickRemoveAddress;
        this.page_status=page_status;
        this.addressListner=addressListner;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_get_address, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        UserAddressModel useraddressmosdel = useraddressmodellist.get(position);

        if(useraddressmosdel.getAddressTitle()==null)
        {
            holder.address_title.setVisibility(View.GONE);
            holder.address_title.setText("");
        }
        else
            {
            holder.address_title.setVisibility(View.VISIBLE);
            holder.address_title.setText(String.valueOf(useraddressmosdel.getAddressTitle()));
           }
        holder.address.setText(useraddressmosdel.getAddress());
        if(LoginPreferences.getActiveInstance(context).getPageId().equals("1"))
        {

        }
        else
        {
            holder.itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    addressListner.addresslistner(useraddressmosdel.getAddress());
                }
            });
        }
        menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                PopupMenu popup = new PopupMenu(context, v, Gravity.END);
                popup.inflate(R.menu.poupup_menu_address);

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        switch (item.getItemId())
                        {
                            case R.id.mark_as_default:
                                 onClickRemoveAddress.removeaddress(useraddressmodellist.get(position).getId(),position,"1");
                                 break;

                            case R.id.remove:
                                onClickRemoveAddress.removeaddress(useraddressmodellist.get(position).getId(),position,"2");
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return useraddressmodellist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
       TextView address_title,address;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            address_title=itemView.findViewById(R.id.address_title);
            address=itemView.findViewById(R.id.address);
            menu_icon=itemView.findViewById(R.id.menu_icon);
        }
    }
}