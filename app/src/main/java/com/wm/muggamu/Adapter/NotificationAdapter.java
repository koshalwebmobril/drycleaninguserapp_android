package com.wm.muggamu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.muggamu.Models.notificationmodel.NotificationModel;
import com.wm.muggamu.R;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>
{
    Context context;
    List<NotificationModel> notificationModelList;
    public NotificationAdapter(Context context, List notificationModelList)
    {
        this.context = context;
        this.notificationModelList=notificationModelList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         context = holder.itemView.getContext();
         NotificationModel notificationmodel = notificationModelList.get(position);
         holder.title.setText(notificationmodel.getTitle());
         holder.title_text.setText(notificationmodel.getDescription());
         holder.timing.setText(notificationmodel.getBookedAt());

         if(notificationmodel.getBooking_type()==1)
         {
             holder.notification_image.setBackgroundResource(R.drawable.confirmed_icon);
         }
         else if(notificationmodel.getBooking_type()==2)
         {
             holder.notification_image.setBackgroundResource(R.drawable.confirmed_icon);
         }
         else if(notificationmodel.getBooking_type()==3)
         {
             holder.notification_image.setBackgroundResource(R.drawable.cancel);
         }

         else if(notificationmodel.getBooking_type()==5)   //inprogress
         {
             holder.notification_image.setBackgroundResource(R.drawable.in_process_visible);
         }

         else if(notificationmodel.getBooking_type()==7)    //Laundry Guy is now out for pickup
         {
             holder.notification_image.setBackgroundResource(R.drawable.laundry_icon);
         }

         else if(notificationmodel.getBooking_type()==8)   //picked up
         {
             holder.notification_image.setBackgroundResource(R.drawable.picked_up_truck_visible);
         }

         else if(notificationmodel.getBooking_type()==11)   //out for delivery
         {
             holder.notification_image.setBackgroundResource(R.drawable.shipped_truck_visible);
         }
         else if(notificationmodel.getBooking_type()==12)   //completed
         {
             holder.notification_image.setBackgroundResource(R.drawable.delivered_box_visible);
         }
         else
         {
             holder.notification_image.setBackgroundResource(R.drawable.notification1);
         }
         holder.itemView.setOnClickListener(view ->
        {
            /*Intent i=new Intent (context, ParticularServiceProvider.class);
            context.startActivity(i);*/
        });
    }
    @Override
    public int getItemCount()
    {
        return notificationModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView title,title_text,timing;
        ImageView notification_image;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            title_text=itemView.findViewById(R.id.title_text);
            timing=itemView.findViewById(R.id.timing);
            notification_image=itemView.findViewById(R.id.notification_image);
        }
    }
}