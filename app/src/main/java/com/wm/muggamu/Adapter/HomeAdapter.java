package com.wm.muggamu.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamu.Activities.ServiceProviderActivity;
import com.wm.muggamu.Models.homepagemodel.CategoriesItemModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder>
{
    Context context;
    List<CategoriesItemModel> categoryitemlist;
    public HomeAdapter(Context context,List categoryitemlist)
    {
        this.context = context;
        this.categoryitemlist=categoryitemlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        CategoriesItemModel selectlangaugeItem = categoryitemlist.get(position);
        Glide.with(context).load(selectlangaugeItem.getThumbnailPath()).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile)).into(holder.laundary_img);
        holder.laundry_name.setText(selectlangaugeItem.getTitle());

        holder.itemView.setOnClickListener(view ->
        {
            Intent i=new Intent (context, ServiceProviderActivity.class);
            i.putExtra("category_id",String.valueOf(selectlangaugeItem.getId()));
            i.putExtra("category_name",String.valueOf(selectlangaugeItem.getTitle()));
           // i.putExtra("category_status","1");
            LoginPreferences.getActiveInstance(context).setCategoryId(String.valueOf(selectlangaugeItem.getId()));
            context.startActivity(i);
        });
    }
    @Override
    public int getItemCount()
    {
        return categoryitemlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView laundry_name;
        ImageView laundary_img;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            laundary_img=itemView.findViewById(R.id.laundary_img);
            laundry_name=itemView.findViewById(R.id.laundry_name);
        }
    }
}