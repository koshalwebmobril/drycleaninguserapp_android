package com.wm.muggamu.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamu.Activities.ParticularServiceProvider;
import com.wm.muggamu.Models.businesstimemodel.ProviderListItemModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class ServiceProviderAdapter extends RecyclerView.Adapter<ServiceProviderAdapter.MyViewHolder>
{
    Context context;
    List<ProviderListItemModel> providerlistitemmodellist;
    public ServiceProviderAdapter(Context context,List providerlistitemmodellist)
    {
        this.context = context;
        this.providerlistitemmodellist=providerlistitemmodellist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_serviceprovider, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        ProviderListItemModel providerlistitemmodel = providerlistitemmodellist.get(position);
        Glide.with(context).load(providerlistitemmodel.getProfileImagePath()).apply(new RequestOptions().placeholder(R.drawable.image).error(R.drawable.image)).into(holder.provider_image);
        holder.landary_name.setText(StringUtils.capitalize(providerlistitemmodel.getName().toLowerCase().trim()));
        holder.time.setText(providerlistitemmodel.getOpenTime() + " - " + providerlistitemmodel.getCloseTime());
        String ratingcount = String.format("%.1f",providerlistitemmodel.getRating());
        holder.rating_count.setText(String.valueOf(ratingcount));

        holder.address.setText(providerlistitemmodel.getAddress());
        BigDecimal bd = new BigDecimal(providerlistitemmodel.getDistance()).setScale(2, RoundingMode.HALF_UP);
        holder.distance.setText(String.valueOf(bd.doubleValue()+" "+ context.getString(R.string.kmaway)));

        holder.itemView.setOnClickListener(view ->
        {
            Intent i=new Intent (context, ParticularServiceProvider.class);
            i.putExtra("position",position);
            i.putExtra("service_name",holder.landary_name.getText().toString().trim());
            i.putExtra("time",providerlistitemmodel.getOpenTime() + " - " + providerlistitemmodel.getCloseTime());
            i.putExtra("address",providerlistitemmodel.getAddress());
            i.putExtra("rating",holder.rating_count.getText().toString());
            i.putExtra("service_image",providerlistitemmodel.getProfileImagePath());
            i.putExtra("service_model",providerlistitemmodellist.get(position));
            i.putExtra("distance",holder.distance.getText().toString().trim());
            context.startActivity(i);
            LoginPreferences.getActiveInstance(context).setProviderId(String.valueOf(providerlistitemmodel.getId()));
            LoginPreferences.getActiveInstance(context).setService_image(providerlistitemmodel.getProfileImagePath());
            LoginPreferences.getActiveInstance(context).setService_address(providerlistitemmodel.getAddress());
            LoginPreferences.getActiveInstance(context).setService_name(holder.landary_name.getText().toString().trim());
        });
    }
    @Override
    public int getItemCount()
    {
        return providerlistitemmodellist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView landary_name,time,address,rating_count,distance;
        ImageView provider_image;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            provider_image=itemView.findViewById(R.id.provider_image);
            landary_name=itemView.findViewById(R.id.landary_name);
            time=itemView.findViewById(R.id.time_confirmed);
            address=itemView.findViewById(R.id.address);
            rating_count=itemView.findViewById(R.id.rating_count);
            distance=itemView.findViewById(R.id.distance);

        }
    }
}