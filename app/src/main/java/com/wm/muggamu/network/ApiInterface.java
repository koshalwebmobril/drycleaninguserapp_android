package com.wm.muggamu.network;



import com.wm.muggamu.Models.acceptbidresponse.AcceptBidsResponse;
import com.wm.muggamu.Models.addaddressresponse.AddAddressResponse;
import com.wm.muggamu.Models.addpostmodel.AddPostResponse;
import com.wm.muggamu.Models.allpostmodel.AllPostDataResponse;
import com.wm.muggamu.Models.appoimentdetailmodel.AppoimentdetailsResponse;
import com.wm.muggamu.Models.availabilitymanagmentmodel.AvailabilityManagmentResponse;
import com.wm.muggamu.Models.bookingconfirmmodel.BookingConfirmResponse;
import com.wm.muggamu.Models.createappoimentmodel.AppoimentResponse;
import com.wm.muggamu.Models.searchcategorymodel.SearchCategoryResponse;
import com.wm.muggamu.Models.yourbookingmodel.BookingResultResponse;
import com.wm.muggamu.Models.contactusmodel.ContactUsResponse;
import com.wm.muggamu.Models.createpostmodel.CreatePostResponse;
import com.wm.muggamu.Models.getaddressmodel.GetAddressResponse;
import com.wm.muggamu.Models.homepagemodel.GetCategoryResponse;
import com.wm.muggamu.Models.getprofilemodel.GetProfileResponse;
import com.wm.muggamu.Models.getproviderbycategorymodel.GetProviderByCategoryResponse;
import com.wm.muggamu.Models.getservicesmodel.GetServicesResponse;
import com.wm.muggamu.Models.servicesubitemsmodel.GetServiceSubitemsResponse;
import com.wm.muggamu.Models.loginmodel.LoginResponse;
import com.wm.muggamu.Models.notificationmodel.NotificationResponse;
import com.wm.muggamu.Models.orderdetailsmodel.OrderDetailsResponse;
import com.wm.muggamu.Models.otpverifymodel.OtpVerifiyResponse;
import com.wm.muggamu.Models.profileupdatemodel.ProfileUpdateResponse;
import com.wm.muggamu.Models.removeaddressmodel.RemoveAddressResponse;
import com.wm.muggamu.Models.postremovemodel.RemovePostResponse;
import com.wm.muggamu.Models.reviewmodel.ReviewResponse;
import com.wm.muggamu.Models.reviewsubmitmodel.ReviewSubmitResponse;
import com.wm.muggamu.Models.serviceonpostmodel.ServiceOnPostResponse;
import com.wm.muggamu.Models.totalamountmodel.TotalAmountResponse;
import com.wm.muggamu.Models.trackordermodel.TrackOrderDetailsResponse;
import com.wm.muggamu.Models.updateprofilemodel.UpdateProfileResponse;
import com.wm.muggamu.Models.userallpostmodel.UserAllPostResponse;
import com.wm.muggamu.Utils.UrlApi;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface
{
    @FormUrlEncoded
    @POST(UrlApi.LOGIN)
    Call<LoginResponse> LoginUser(@Field("mobile") String mobile,
                                  @Field("type") String type,
                                  @Field("device_type") String deviceType,
                                  @Field("device_token") String deviceToken);


    @FormUrlEncoded
    @POST(UrlApi.OtpVerifie)
    Call<OtpVerifiyResponse> OtpVerifie(@Field("id") String user_id, @Field("otp") String otp,
                                        @Field("type") String type,
                                        @Field("device_type") String deviceType,
                                        @Field("device_token") String deviceToken,
                                        @Field("latitude") String latitude,
                                        @Field("longitude") String longitude);



    @FormUrlEncoded
    @POST(UrlApi.CONTACTUS)
    Call<ContactUsResponse> contactus(@Header("Authorization") String token,
                                      @Field("subject") String subject, @Field("comments") String comments);

    @GET(UrlApi.GetProfile)
    Call<GetProfileResponse> GetProfile(@Header("Authorization") String token);

    @GET(UrlApi.GETNOTIFICATION)
    Call<NotificationResponse> getNotification(@Header("Authorization") String token);

    @GET(UrlApi.SERVICEONPOST)
    Call<ServiceOnPostResponse> servicePost(@Header("Authorization") String token);


    @GET(UrlApi.ALLPOST)
    Call<UserAllPostResponse> allPost(@Header("Authorization") String token);

    @Multipart
    @POST(UrlApi.UPDATEPROFILE)
    Call<UpdateProfileResponse> updateprofile(@Header("Authorization") String token,
                                              @Part("name")  RequestBody  name,
                                              @Part("email") RequestBody email,
                                              @Part MultipartBody.Part image,
                                              @Part("latitude") RequestBody latitude,
                                              @Part("longitude") RequestBody longitude);

    @Multipart
    @POST(UrlApi.UPDATEPROFILE)
    Call<UpdateProfileResponse> updateNullprofile(@Header("Authorization") String token,
                                              @Part("name")  RequestBody  name,
                                              @Part MultipartBody.Part image,
                                              @Part("latitude") RequestBody latitude,
                                              @Part("longitude") RequestBody longitude);


    @Multipart
    @POST(UrlApi.CREATEPOST)
    Call<CreatePostResponse> createPost(@Header("Authorization") String token,
                                              @Part("subitem_id")  RequestBody  child_item_id,
                                              @Part("quantity")  RequestBody  child_quantity,
                                              @Part("service_id")  RequestBody  service_id,
                                             @Part("service_date") RequestBody service_date,
                                              @Part("service_time") RequestBody service_time,
                                              @Part("service_address") RequestBody service_address,
                                             @Part("service_amount") RequestBody service_amount,
                                              @Part("remarks") RequestBody remarks,
                                              @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST(UrlApi.CREATEBOOKING)
    Call<BookingConfirmResponse> createBooking(@Header("Authorization") String token,
                                               @Field("provider_id") String provider_id,
                                               @Field("service_id") String service_id,
                                               @Field("subitem_id") String subitem_id,
                                               @Field("quantity") String quantity,
                                               @Field("pickup_location") String pickup_location,
                                               @Field("pickup_date") String pickup_date,
                                               @Field("pickup_time") String pickup_time,
                                               @Field("delivery_date") String delivery_date,
                                               @Field("delivery_time") String delivery_time,
                                               @Field("amount") String amount);


    @Multipart
    @POST(UrlApi.UPDATEPROFILE)
    Call<ProfileUpdateResponse> updateprofile1(@Header("Authorization") String token,
                                              @Part("name")  RequestBody  name,
                                              @Part MultipartBody.Part image,
                                               @Part("latitude")  RequestBody  latitude,
                                               @Part("longitude")  RequestBody  longitude);

    @Multipart
    @POST(UrlApi.RATEPROVIDER)
    Call<ReviewSubmitResponse> reviewsubmit(@Header("Authorization") String token,
                                            @Part("provider_id")  RequestBody  provider_id,
                                            @Part("booking_id")  RequestBody  booking_id,
                                            @Part("rating")  RequestBody  rating,
                                            @Part("review")  RequestBody  review,
                                            @Part MultipartBody.Part image,
                                            @Part("appointment_id")  RequestBody  appointment_id);

    @GET(UrlApi.GETCATEGORYHOME)
    Call<GetCategoryResponse> getcategoryhome(@Header("Authorization") String token);

    @GET(UrlApi.SUBITEMSONPOST)
    Call<AddPostResponse> getAddPost(@Header("Authorization") String token);

    @GET(UrlApi.GETADDRESSAPI)
    Call<GetAddressResponse> getAddress(@Header("Authorization") String token);

    @GET(UrlApi.GETALLBOOKING)
    Call<BookingResultResponse> getAllBooking(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(UrlApi.GETPROVIDERBYCATEGORY)
    Call<GetProviderByCategoryResponse> getProviderByCategory(@Header("Authorization") String token, @Field("category_id") String category_id);


    @FormUrlEncoded
    @POST(UrlApi.UPDATETOKEN)
    Call<RemoveAddressResponse> updatetoken(@Header("Authorization") String token ,
                                            @Field("device_type") String deviceType,
                                            @Field("device_token") String deviceToken);


    @FormUrlEncoded
    @POST(UrlApi.ORDERDETAILS)
    Call<OrderDetailsResponse> orderDetailsResponse(@Header("Authorization") String token, @Field("booking_id") String booking_id);



   /* @FormUrlEncoded
    @POST(UrlApi.getappointmentDetails)
    Call<OrderDetailsResponse> orderDetailsResponse(@Header("Authorization") String token, @Field("booking_id") String booking_id);

*/

    @FormUrlEncoded
    @POST(UrlApi.GETPROVIDERDETAILSBYID)
    Call<GetServicesResponse> getServiceList(@Header("Authorization") String token, @Field("provider_id") String provider_id);



    @FormUrlEncoded
    @POST(UrlApi.GETPROVIDERAVAILABILITY)
    Call<AvailabilityManagmentResponse> getAvailibilty(@Header("Authorization") String token, @Field("provider_id") String provider_id, @Field("date") String date);

    @FormUrlEncoded
    @POST(UrlApi.getappointmentDetails)
    Call<AppoimentdetailsResponse> Appoimentdetails(@Header("Authorization") String token, @Field("appointment_id") String appointment_id);

    @FormUrlEncoded
    @POST(UrlApi.CREATEAPPOIMENT)
    Call<AppoimentResponse> createAppoiment(@Header("Authorization") String token, @Field("provider_id") String provider_id, @Field("date") String date
            , @Field("time") String time);



    @FormUrlEncoded
    @POST(UrlApi.GETTOTALFEE)
    Call<TotalAmountResponse> gettotalamount(@Field("amount") String amount);


    @FormUrlEncoded
    @POST(UrlApi.TRACKORDER)
    Call<TrackOrderDetailsResponse> trackorder(@Header("Authorization") String token, @Field("booking_id") String booking_id);


    @FormUrlEncoded
    @POST(UrlApi.REVIEW)
    Call<ReviewResponse> getReview(@Header("Authorization") String token, @Field("provider_id") String provider_id);


    @FormUrlEncoded
    @POST(UrlApi.DELETEPOST)
    Call<RemovePostResponse> removePost(@Header("Authorization") String token, @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST(UrlApi.SEARCHPROVIDER)
    Call<GetProviderByCategoryResponse> searchcategory(@Header("Authorization") String token, @Field("keyword") String keyword,@Field("category_id") String category_id);


    @FormUrlEncoded
    @POST(UrlApi.SEARCHCATEGORY)
    Call<SearchCategoryResponse> searchprovider(@Header("Authorization") String token, @Field("keyword") String keyword);




    @FormUrlEncoded
    @POST(UrlApi.ALLBIDSONPOST)
    Call<AllPostDataResponse> postdetails(@Header("Authorization") String token, @Field("post_id") String post_id);


    @FormUrlEncoded
    @POST(UrlApi.ACCEPTBIDSAPI)
    Call<AcceptBidsResponse> acceptbid(@Header("Authorization") String token, @Field("post_id") String post_id,@Field("bid_id") String bid_id);


    @FormUrlEncoded
    @POST(UrlApi.REJECTBIDSAPI)
    Call<AcceptBidsResponse> rejectbid(@Header("Authorization") String token, @Field("post_id") String post_id,@Field("bid_id") String bid_id);


    @FormUrlEncoded
    @POST(UrlApi.ADDADDRESSAPI)
    Call<AddAddressResponse> addAddress(@Header("Authorization") String token, @Field("address") String address,@Field("city") String city,@Field("latitude") String latitude,@Field("longitude") String longitude,@Field("identifier") String identifier
   ,@Field("landmark") String landmark,@Field("zipcode") String zipcode,@Field("is_default") String is_default);



    @FormUrlEncoded
    @POST(UrlApi.EDITADDRESSAPI)
    Call<AddAddressResponse> EditAddress(@Header("Authorization") String token, @Field("address") String address, @Field("landmark") String landmark, @Field("city") String city,@Field("zipcode") String zipcode,@Field("address_id") String address_id);



    @FormUrlEncoded
    @POST(UrlApi.REMOVEADDRESS)
    Call<RemoveAddressResponse> removeAddress(@Header("Authorization") String token, @Field("address_id") String address_id);


    @FormUrlEncoded
    @POST(UrlApi.MARKASDEFAULT)
    Call<RemoveAddressResponse> markasdefault(@Header("Authorization") String token, @Field("address_id") String address_id);




    @FormUrlEncoded
    @POST(UrlApi.GETSERVICESUBITEMS)
    Call<GetServiceSubitemsResponse> getserviceSubitems(@Header("Authorization") String token, @Field("service_id") String service_id,@Field("provider_id") String provider_id);

}
