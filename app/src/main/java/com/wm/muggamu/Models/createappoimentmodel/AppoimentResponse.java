package com.wm.muggamu.Models.createappoimentmodel;

import com.google.gson.annotations.SerializedName;

public class AppoimentResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("appointment")
	private Appointment appointment;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public Appointment getAppointment(){
		return appointment;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}