package com.wm.muggamu.Models.userallpostmodel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PostDetailsItem implements Serializable {

	@SerializedName("post_id")
	private int postId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("subitem_quantity")
	private int subitemQuantity;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("subitem_name")
	private String subitemName;

	@SerializedName("id")
	private int id;

	@SerializedName("subitem_id")
	private int subitemId;

	public int getPostId(){
		return postId;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getSubitemQuantity(){
		return subitemQuantity;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getSubitemName(){
		return subitemName;
	}

	public int getId(){
		return id;
	}

	public int getSubitemId(){
		return subitemId;
	}
}