package com.wm.muggamu.Models.getproviderbycategorymodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.wm.muggamu.Models.businesstimemodel.ProviderListItemModel;

public class GetProviderByCategoryResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	@SerializedName("providerList")
	private List<ProviderListItemModel> providerList;

	public int getCode(){
		return code;
	}
	public String getMessage(){
		return message;
	}

	public boolean isError(){
		return error;
	}

	public List<ProviderListItemModel> getProviderList(){
		return providerList;
	}
}