package com.wm.muggamu.Models.totalamountmodel;

import com.google.gson.annotations.SerializedName;

public class TotalAmountResponse {

	@SerializedName("fees")
	private String fees;

	@SerializedName("code")
	private int code;

	@SerializedName("total_amount")
	private String totalAmount;

	@SerializedName("error")
	private boolean error;

	public String getFees(){
		return fees;
	}

	public int getCode(){
		return code;
	}

	public String getTotalAmount(){
		return totalAmount;
	}

	public boolean isError(){
		return error;
	}
}