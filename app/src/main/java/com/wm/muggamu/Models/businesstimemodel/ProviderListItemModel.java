package com.wm.muggamu.Models.businesstimemodel;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProviderListItemModel implements Serializable
{

	@SerializedName("gender")
	private Object gender;

	@SerializedName("distance")
	private double distance;

	@SerializedName("date_of_birth")
	private Object dateOfBirth;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("rating")
	private double rating;

	@SerializedName("open_time")
	private String openTime;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("profile_image_path")
	private String profileImagePath;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("type")
	private int type;

	@SerializedName("is_admin")
	private int isAdmin;

	@SerializedName("profile_image_name")
	private String profileImageName;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("id")
	private int id;

	@SerializedName("service_id")
	private int service_id;

	@SerializedName("email")
	private String email;

	@SerializedName("document_uploaded")
	private int documentUploaded;

	@SerializedName("longitude")
	private String longitude;

	@SerializedName("business_time")
	private List<BusinessTimeItem> businessTime;

	@SerializedName("address")
	private String address;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("verified")
	private int verified;

	@SerializedName("email_verified_at")
	private Object emailVerifiedAt;

	@SerializedName("close_time")
	private String closeTime;

	@SerializedName("document_verified")
	private int documentVerified;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("name")
	private String name;

	@SerializedName("is_approved")
	private int isApproved;

	@SerializedName("status")
	private int status;

	public Object getGender(){
		return gender;
	}

	public double getDistance(){
		return distance;
	}

	public Object getDateOfBirth(){
		return dateOfBirth;
	}

	public String getLatitude(){
		return latitude;
	}

	public double getRating(){
		return rating;
	}

	public String getOpenTime(){
		return openTime;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getProfileImagePath(){
		return profileImagePath;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public int getType(){
		return type;
	}

	public int getIsAdmin(){
		return isAdmin;
	}

	public String getProfileImageName(){
		return profileImageName;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getId(){
		return id;
	}

	public String getEmail(){
		return email;
	}

	public int getDocumentUploaded(){
		return documentUploaded;
	}

	public String getLongitude(){
		return longitude;
	}

	public List<BusinessTimeItem> getBusinessTime(){
		return businessTime;
	}

	public String getAddress(){
		return address;
	}

	public String getMobile(){
		return mobile;
	}

	public int getVerified(){
		return verified;
	}

	public Object getEmailVerifiedAt(){
		return emailVerifiedAt;
	}

	public String getCloseTime(){
		return closeTime;
	}

	public int getDocumentVerified(){
		return documentVerified;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public String getName(){
		return name;
	}

	public int getIsApproved(){
		return isApproved;
	}

	public int getStatus(){
		return status;
	}
}