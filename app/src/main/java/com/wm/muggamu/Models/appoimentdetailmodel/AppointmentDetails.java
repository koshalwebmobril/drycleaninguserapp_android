package com.wm.muggamu.Models.appoimentdetailmodel;

import com.google.gson.annotations.SerializedName;

public class AppointmentDetails{

	@SerializedName("date")
	private String date;

	@SerializedName("is_rated")
	private String is_rated;

	@SerializedName("provider_device_token")
	private String providerDeviceToken;

	@SerializedName("appointment_status")
	private int appointmentStatus;

	@SerializedName("provider_address")
	private String providerAddress;

	@SerializedName("appointment_id")
	private String appointmentId;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("is_provider_accepted")
	private int isProviderAccepted;

	@SerializedName("booked_at")
	private String bookedAt;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	@SerializedName("time")
	private String time;

	@SerializedName("provider_profile_image")
	private String providerProfileImage;

	@SerializedName("provider_name")
	private String providerName;

	public String getDate(){
		return date;
	}

	public String getProviderDeviceToken(){
		return providerDeviceToken;
	}

	public int getAppointmentStatus(){
		return appointmentStatus;
	}

	public String getProviderAddress(){
		return providerAddress;
	}

	public String getAppointmentId(){
		return appointmentId;
	}

	public int getUserId(){
		return userId;
	}

	public int getIsProviderAccepted(){
		return isProviderAccepted;
	}

	public String getBookedAt(){
		return bookedAt;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getId(){
		return id;
	}

	public String getTime(){
		return time;
	}

	public String getProviderProfileImage(){
		return providerProfileImage;
	}

	public String getProviderName(){
		return providerName;
	}

	public String getisrated()
	{
		return is_rated;
	}
}