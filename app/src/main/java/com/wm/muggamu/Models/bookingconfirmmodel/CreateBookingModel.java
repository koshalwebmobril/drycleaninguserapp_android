package com.wm.muggamu.Models.bookingconfirmmodel;

import com.google.gson.annotations.SerializedName;

public class CreateBookingModel {

	@SerializedName("booking_id")
	private int bookingId;

	@SerializedName("pickup_date")
	private String pickupDate;


	@SerializedName("provider_image")
	private String provider_image;

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("total_amount")
	private double totalAmount;

	@SerializedName("pickup_location")
	private String pickupLocation;

	@SerializedName("pickup_time")
	private String pickupTime;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("provider_name")
	private String providerName;

	@SerializedName("order_id")
	private String orderId;

	public int getBookingId(){
		return bookingId;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public double getTotalAmount(){
		return totalAmount;
	}

	public String getPickupLocation(){
		return pickupLocation;
	}

	public String getPickupTime(){
		return pickupTime;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public String getProviderName(){
		return providerName;
	}

	public String getProvider_image(){
		return provider_image;
	}

	public String getOrderId(){
		return orderId;
	}
}