package com.wm.muggamu.Models.orderdetailsmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.wm.muggamu.Models.bookingdetailsmodel.BookingDetails;

public class OrderDetailsResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("itemDetails")
	private List<OrderDetailsModel> itemDetails;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	@SerializedName("bookingDetails")
	private BookingDetails bookingDetails;

	public int getCode(){
		return code;
	}

	public String getMessage(){
		return message;
	}

	public List<OrderDetailsModel> getItemDetails(){
		return itemDetails;
	}

	public boolean isError(){
		return error;
	}

	public BookingDetails getBookingDetails(){
		return bookingDetails;
	}
}