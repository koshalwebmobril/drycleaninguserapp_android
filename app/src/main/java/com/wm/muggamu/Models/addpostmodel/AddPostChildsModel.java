package com.wm.muggamu.Models.addpostmodel;

import com.google.gson.annotations.SerializedName;

public class AddPostChildsModel {

	@SerializedName("item_name")
	private String itemName;

	@SerializedName("id")
	private int id;

	@SerializedName("item_category")
	private String itemCategory;

	@SerializedName("status")
	private int status;

	public String getItemName(){
		return itemName;
	}

	public int getChildId(){
		return id;
	}

	public String getItemCategory(){
		return itemCategory;
	}

	public int getStatus(){
		return status;
	}
}