package com.wm.muggamu.Models.getprofilemodel;

import com.google.gson.annotations.SerializedName;

public class GetProfileResponse{

	@SerializedName("userInfo")
	private GetProfileModel getProfileModel;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	public GetProfileModel getGetProfileModel(){
		return getProfileModel;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}
}