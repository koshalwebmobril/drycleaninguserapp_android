package com.wm.muggamu.Models.allpostmodel;

import com.google.gson.annotations.SerializedName;

public class BidsResultModel {

	@SerializedName("user_action")
	private Object userAction;

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("post_id")
	private int postId;

	@SerializedName("bid_amount")
	private double bidAmount;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("provider_name")
	private String providerName;

	@SerializedName("status")
	private int status;

	public Object getUserAction(){
		return userAction;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public int getPostId(){
		return postId;
	}

	public double getBidAmount(){
		return bidAmount;
	}

	public int getProviderId(){
		return providerId;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public String getProviderName(){
		return providerName;
	}

	public int getStatus(){
		return status;
	}
}