package com.wm.muggamu.Models.bookingdetailsmodel;

import com.google.gson.annotations.SerializedName;

public class BookingDetails{

	@SerializedName("pickup_location")
	private String pickupLocation;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("provider_image")
	private String providerImage;

	@SerializedName("delivery_fee")
	private double deliveryFee;

	@SerializedName("pickup_date")
	private String pickupDate;

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("total_amount")
	private double totalAmount;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	@SerializedName("booking_status")
	private int booking_status;

	@SerializedName("pickup_time")
	private String pickupTime;

	@SerializedName("provider_name")
	private String providerName;


	@SerializedName("is_rated")
	private String is_rated;



	@SerializedName("order_id")
	private String orderId;


	@SerializedName("provider_device_token")
	private String providerdevicetoken;

	public String getPickupLocation(){
		return pickupLocation;
	}

	public String getisrated()
	{
		return is_rated;
	}

	public int getBooking_status(){
		return booking_status;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public String getProviderImage(){
		return providerImage;
	}


	public String getProviderDeviceToken(){
		return providerdevicetoken;
	}

	public double getDeliveryFee(){
		return deliveryFee;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public int getUserId(){
		return userId;
	}

	public double getTotalAmount(){
		return totalAmount;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getId(){
		return id;
	}

	public String getPickupTime(){
		return pickupTime;
	}

	public String getProviderName(){
		return providerName;
	}

	public String getOrderId(){
		return orderId;
	}
}