package com.wm.muggamu.Models.yourbookingmodel;

import com.google.gson.annotations.SerializedName;

public class BookingsItemModel {

	@SerializedName("date")
	private String date;

	@SerializedName("provider_mobile")
	private String providerMobile;

	@SerializedName("appointment_status")
	private int appointmentStatus;

	@SerializedName("pickup_location")
	private String pickupLocation;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("appointment_id")
	private String appointmentId;

	@SerializedName("booking_status")
	private int bookingStatus;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("provider_image")
	private String providerImage;

	@SerializedName("pieces")
	private int pieces;

	@SerializedName("pickup_date")
	private String pickupDate;

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("total_amount")
	private String totalAmount;

	@SerializedName("is_provider_accepted")
	private int isProviderAccepted;

	@SerializedName("booked_at")
	private String bookedAt;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("booking_type")
	private int bookingType;

	@SerializedName("id")
	private int id;

	@SerializedName("pickup_time")
	private String pickupTime;

	@SerializedName("time")
	private String time;

	@SerializedName("provider_name")
	private String providerName;

	@SerializedName("order_id")
	private String orderId;

	public String getDate(){
		return date;
	}

	public String getProviderMobile(){
		return providerMobile;
	}

	public int getAppointmentStatus(){
		return appointmentStatus;
	}

	public String getPickupLocation(){
		return pickupLocation;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getAppointmentId(){
		return appointmentId;
	}

	public int getBookingStatus(){
		return bookingStatus;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public String getProviderImage(){
		return providerImage;
	}

	public int getPieces(){
		return pieces;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public int getUserId(){
		return userId;
	}

	public String getTotalAmount(){
		return totalAmount;
	}

	public int getIsProviderAccepted(){
		return isProviderAccepted;
	}

	public String getBookedAt(){
		return bookedAt;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getBookingType(){
		return bookingType;
	}

	public int getId(){
		return id;
	}

	public String getPickupTime(){
		return pickupTime;
	}

	public String getTime(){
		return time;
	}

	public String getProviderName(){
		return providerName;
	}

	public String getOrderId(){
		return orderId;
	}
}