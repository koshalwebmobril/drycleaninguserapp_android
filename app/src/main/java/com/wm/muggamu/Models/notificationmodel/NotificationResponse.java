package com.wm.muggamu.Models.notificationmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class NotificationResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("notificationList")
	private List<NotificationModel> notificationList;

	@SerializedName("message")
	private String message;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public String getMessage(){
		return message;
	}

	public List<NotificationModel> getNotificationList(){
		return notificationList;
	}

	public boolean isError(){
		return error;
	}
}