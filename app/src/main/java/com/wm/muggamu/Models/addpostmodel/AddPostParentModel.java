package com.wm.muggamu.Models.addpostmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AddPostParentModel {

	@SerializedName("id")
	private int id;

	@SerializedName("category")
	private String category;

	@SerializedName("items")
	private List<AddPostChildsModel> items;

	public int getId(){
		return id;
	}

	public String getCategory(){
		return category;
	}

	public List<AddPostChildsModel> getItems(){
		return items;
	}
}