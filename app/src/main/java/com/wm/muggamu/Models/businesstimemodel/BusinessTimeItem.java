package com.wm.muggamu.Models.businesstimemodel;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessTimeItem implements Serializable {

	@SerializedName("dayname_time")
	private String daynameTime;

	protected BusinessTimeItem(Parcel in) {
		daynameTime = in.readString();
	}



	public String getDaynameTime(){
		return daynameTime;
	}

}