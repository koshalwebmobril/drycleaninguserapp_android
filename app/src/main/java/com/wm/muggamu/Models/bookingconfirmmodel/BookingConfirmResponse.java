package com.wm.muggamu.Models.bookingconfirmmodel;

import com.google.gson.annotations.SerializedName;

public class BookingConfirmResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("bookingResult")
	private CreateBookingModel createBookingModel;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public CreateBookingModel getCreateBookingModel(){
		return createBookingModel;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}