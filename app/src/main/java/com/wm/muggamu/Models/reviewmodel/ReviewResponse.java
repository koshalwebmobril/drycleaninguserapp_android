package com.wm.muggamu.Models.reviewmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ReviewResponse{

	@SerializedName("reviewList")
	private List<ReviewListItemModel> reviewList;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	public List<ReviewListItemModel> getReviewList(){
		return reviewList;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}
}