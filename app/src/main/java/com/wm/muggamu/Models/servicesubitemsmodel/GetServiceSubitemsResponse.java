package com.wm.muggamu.Models.servicesubitemsmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetServiceSubitemsResponse
{
	@SerializedName("code")
	private int code;
	@SerializedName("subitemList")
	private List<ParentItemsModel> subitemList;
	@SerializedName("error")
	private boolean error;
	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public List<ParentItemsModel> getSubitemList(){
		return subitemList;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}