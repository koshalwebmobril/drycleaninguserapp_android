package com.wm.muggamu.Models.searchcategorymodel;

import com.google.gson.annotations.SerializedName;

public class CategoriesItem{

	@SerializedName("thumbnail_path")
	private String thumbnailPath;

	@SerializedName("details")
	private String details;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	public String getThumbnailPath(){
		return thumbnailPath;
	}

	public String getDetails(){
		return details;
	}

	public int getId(){
		return id;
	}

	public String getTitle(){
		return title;
	}
}