package com.wm.muggamu.Models.orderdetailsmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrderDetailsModel {

	@SerializedName("service_name")
	private String serviceName;

	@SerializedName("id")
	private int id;

	@SerializedName("items")
	private List<OrderDetailsItemsModel> items;

	public String getServiceName(){
		return serviceName;
	}

	public int getId(){
		return id;
	}

	public List<OrderDetailsItemsModel> getItems(){
		return items;
	}
}