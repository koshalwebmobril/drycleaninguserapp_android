package com.wm.muggamu.Models.loginmodel;

import com.google.gson.annotations.SerializedName;

public class LoginResponse{

	@SerializedName("loginResult")
	private LoginModel loginModel;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public LoginModel getLoginModel(){
		return loginModel;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}