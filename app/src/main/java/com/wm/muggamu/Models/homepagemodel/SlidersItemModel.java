package com.wm.muggamu.Models.homepagemodel;

import com.google.gson.annotations.SerializedName;

public class SlidersItemModel {

	@SerializedName("path")
	private String path;

	@SerializedName("id")
	private int id;

	public String getPath(){
		return path;
	}

	public int getId(){
		return id;
	}
}