package com.wm.muggamu.Models.reviewmodel;

import com.google.gson.annotations.SerializedName;

public class ReviewListItemModel {

	@SerializedName("image")
	private Object image;

	@SerializedName("booking_service_name")
	private String bookingServiceName;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("booking_date")
	private String bookingDate;

	@SerializedName("rating")
	private String rating;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("booking_time")
	private String bookingTime;

	@SerializedName("booking_id")
	private int bookingId;

	@SerializedName("user_profile_image")
	private String userProfileImage;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("review")
	private String review;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	public Object getImage(){
		return image;
	}

	public String getBookingServiceName(){
		return bookingServiceName;
	}

	public String getUserName(){
		return userName;
	}

	public String getBookingDate(){
		return bookingDate;
	}

	public String getRating(){
		return rating;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getBookingTime(){
		return bookingTime;
	}

	public int getBookingId(){
		return bookingId;
	}

	public String getUserProfileImage(){
		return userProfileImage;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getUserId(){
		return userId;
	}

	public String getReview(){
		return review;
	}

	public int getProviderId(){
		return providerId;
	}

	public int getId(){
		return id;
	}
}