package com.wm.muggamu.Models.trackordermodel;

import com.google.gson.annotations.SerializedName;

public class TrackOrderDetailsResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("trackResult")
	private TrackOrderResultModel trackOrderResultModel;

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public TrackOrderResultModel getTrackOrderResultModel(){
		return trackOrderResultModel;
	}
}