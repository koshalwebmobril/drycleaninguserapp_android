package com.wm.muggamu.Models.appoimentdetailmodel;

import com.google.gson.annotations.SerializedName;

public class AppoimentdetailsResponse
{

	@SerializedName("code")
	private int code;

	@SerializedName("appointmentDetails")
	private AppointmentDetails appointmentDetails;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public AppointmentDetails getAppointmentDetails(){
		return appointmentDetails;
	}

	public boolean isError(){
		return error;
	}
}