package com.wm.muggamu.Models.homepagemodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class HomepageResultResponse {

	@SerializedName("categories")
	private List<CategoriesItemModel> categories;

	@SerializedName("sliders")
	private List<SlidersItemModel> sliders;

	public List<CategoriesItemModel> getCategories(){
		return categories;
	}

	public List<SlidersItemModel> getSliders(){
		return sliders;
	}
}