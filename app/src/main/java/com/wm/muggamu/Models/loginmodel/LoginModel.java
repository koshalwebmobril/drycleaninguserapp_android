package com.wm.muggamu.Models.loginmodel;

import com.google.gson.annotations.SerializedName;

public class LoginModel {

	@SerializedName("otp")
	private int otp;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private String type;

	public int getOtp(){
		return otp;
	}

	public int getId(){
		return id;
	}

	public String getType(){
		return type;
	}
}