package com.wm.muggamu.Models.getaddressmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetAddressResponse{

	@SerializedName("userAddress")
	private List<UserAddressModel> userAddress;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	public List<UserAddressModel> getUserAddress(){
		return userAddress;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}
}