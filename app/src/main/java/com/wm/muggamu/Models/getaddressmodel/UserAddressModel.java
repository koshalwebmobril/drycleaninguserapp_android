package com.wm.muggamu.Models.getaddressmodel;

import com.google.gson.annotations.SerializedName;

public class UserAddressModel {

	@SerializedName("zipcode")
	private String zipcode;

	@SerializedName("country")
	private Object country;

	@SerializedName("identifier")
	private Object identifier;

	@SerializedName("address")
	private String address;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("city")
	private String city;

	@SerializedName("id")
	private int id;

	@SerializedName("state")
	private Object state;

	@SerializedName("landmark")
	private String landmark;


	public String getZipcode(){
		return zipcode;
	}

	public Object getCountry(){
		return country;
	}



	public String getAddress(){
		return address;
	}

	public int getUserId(){
		return userId;
	}

	public String getCity(){
		return city;
	}

	public Object getAddressTitle(){
		return identifier;
	}

	public int getId(){
		return id;
	}

	public Object getState(){
		return state;
	}

	public String getLandmark(){
		return landmark;
	}
}