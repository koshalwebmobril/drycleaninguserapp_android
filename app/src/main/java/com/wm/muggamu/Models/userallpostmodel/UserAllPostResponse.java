package com.wm.muggamu.Models.userallpostmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class UserAllPostResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("message")
	private String message;

	@SerializedName("postResult")
	private List<AllPostModel> postResult;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public String getMessage(){
		return message;
	}

	public List<AllPostModel> getPostResult(){
		return postResult;
	}

	public boolean isError(){
		return error;
	}
}