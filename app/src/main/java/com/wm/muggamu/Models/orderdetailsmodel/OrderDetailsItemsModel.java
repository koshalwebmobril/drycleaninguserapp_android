package com.wm.muggamu.Models.orderdetailsmodel;

import com.google.gson.annotations.SerializedName;

public class OrderDetailsItemsModel {

	@SerializedName("booking_id")
	private int bookingId;

	@SerializedName("subitem_rate")
	private float subitemRate;

	@SerializedName("service_id")
	private int serviceId;

	@SerializedName("subitem_quantity")
	private String subitemQuantity;

	@SerializedName("subitem_name")
	private String subitemName;

	@SerializedName("id")
	private int id;

	@SerializedName("subitem_total_price")
	private float subitemTotalPrice;

	@SerializedName("order_id")
	private String orderId;

	@SerializedName("subitem_id")
	private int subitemId;

	public int getBookingId(){
		return bookingId;
	}

	public float getSubitemRate(){
		return subitemRate;
	}

	public int getServiceId(){
		return serviceId;
	}

	public String getSubitemQuantity(){
		return subitemQuantity;
	}

	public String getSubitemName(){
		return subitemName;
	}

	public int getId(){
		return id;
	}

	public float getSubitemTotalPrice(){
		return subitemTotalPrice;
	}

	public String getOrderId(){
		return orderId;
	}

	public int getSubitemId(){
		return subitemId;
	}
}