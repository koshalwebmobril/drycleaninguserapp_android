package com.wm.muggamu.Models.addpostmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AddPostResponse {

	@SerializedName("code")
	private int code;

	@SerializedName("subitemList")
	private List<AddPostParentModel> subitemList;

	@SerializedName("error")
	private boolean error;



	public int getCode(){
		return code;
	}

	public List<AddPostParentModel> getSubitemList(){
		return subitemList;
	}

	public boolean isError(){
		return error;
	}
}