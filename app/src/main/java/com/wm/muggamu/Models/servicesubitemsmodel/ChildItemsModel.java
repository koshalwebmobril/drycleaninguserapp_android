package com.wm.muggamu.Models.servicesubitemsmodel;

import com.google.gson.annotations.SerializedName;

public class ChildItemsModel {

	@SerializedName("price")
	private float price;

	@SerializedName("service_id")
	private int serviceId;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("item_name")
	private String itemName;

	@SerializedName("service_name")
	private String service_name;

	@SerializedName("id")
	private int id;

	@SerializedName("subitem_id")
	private int subitemId;

	@SerializedName("status")
	private int status;

	public float getPrice(){
		return price;
	}

	public int getServiceId(){
		return serviceId;
	}

	public int getProviderId(){
		return providerId;
	}

	public String getItemName(){
		return itemName;
	}

	public String getService_name(){
		return service_name;
	}

	public int getId(){
		return id;
	}

	public int getSubitemId(){
		return subitemId;
	}

	public int getStatus(){
		return status;
	}
}