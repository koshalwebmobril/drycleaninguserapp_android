package com.wm.muggamu.Models.updateprofilemodel;

import com.google.gson.annotations.SerializedName;

public class UpdateProfileResponse{

	@SerializedName("userInfo")
	private UpdateProfile1Model updateProfileModel;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public UpdateProfile1Model getUpdateProfileModel(){
		return updateProfileModel;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}