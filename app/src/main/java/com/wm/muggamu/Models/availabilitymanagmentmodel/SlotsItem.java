package com.wm.muggamu.Models.availabilitymanagmentmodel;

import com.google.gson.annotations.SerializedName;

public class SlotsItem {

	@SerializedName("id")
	private int id;

	@SerializedName("time")
	private String time;

	@SerializedName("status")
	private int status;

	@SerializedName("availability_status")
	private String availability_status;

	/*String time1;
	int timeStatus;

	public SlotsItem(String time1, int timeStatus)
	{
		this.time1 = time1;
		this.timeStatus = timeStatus;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getTimeStatus() {
		return timeStatus;
	}

	public void setTimeStatus(int timeStatus) {
		this.timeStatus = timeStatus;
	}*/

	public String getAvailability_status() {
		return availability_status;
	}

	public void setAvailability_status(String availability_status) {
		this.availability_status = availability_status;
	}

	private String selectstatus;

	public String getSelected() {
		return selectstatus;
	}

	public void setSelected(String selected) {
		selectstatus = selectstatus;
	}


	public int getId() {
		return id;
	}

	public String getSlot() {
		return time;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}