package com.wm.muggamu.Models.notificationmodel;

import com.google.gson.annotations.SerializedName;

public class NotificationModel {

	@SerializedName("booking_id")
	private int bookingId;

	@SerializedName("notification_type")
	private int notificationType;

	@SerializedName("booking_status")
	private int booking_type;

	@SerializedName("post_id")
	private Object postId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("booked_at")
	private String bookedAt;

	@SerializedName("description")
	private String description;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	public int getBookingId(){
		return bookingId;
	}


	public int getBooking_type(){
		return booking_type;
	}

	public int getNotificationType(){
		return notificationType;
	}

	public Object getPostId(){
		return postId;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getUserId(){
		return userId;
	}

	public String getBookedAt(){
		return bookedAt;
	}

	public String getDescription(){
		return description;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public String getTitle(){
		return title;
	}
}