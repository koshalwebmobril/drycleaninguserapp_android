package com.wm.muggamu.Models.getservicesmodel;

import com.google.gson.annotations.SerializedName;

public class ServicesItemModel {

	@SerializedName("thumbnail_path")
	private String thumbnailPath;

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("thumbnail_name")
	private String thumbnailName;

	@SerializedName("service_name")
	private String name;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("service_id")
	private int service_id;

	@SerializedName("thumbnail_mime")
	private String thumbnailMime;

	@SerializedName("thumbnail_size")
	private int thumbnailSize;

	@SerializedName("status")
	private int status;

	public String getThumbnailPath(){
		return thumbnailPath;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getThumbnailName(){
		return thumbnailName;
	}

	public String getName(){
		return name;
	}

	public int getProviderId(){
		return providerId;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public int getserviceId(){
		return service_id;
	}

	public String getThumbnailMime(){
		return thumbnailMime;
	}

	public int getThumbnailSize(){
		return thumbnailSize;
	}

	public int getStatus(){
		return status;
	}
}