package com.wm.muggamu.Models.homepagemodel;

import com.google.gson.annotations.SerializedName;

public class GetCategoryResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("homepageResult")
	private HomepageResultResponse homepageResultResponse;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public HomepageResultResponse getHomepageResultResponse(){
		return homepageResultResponse;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}