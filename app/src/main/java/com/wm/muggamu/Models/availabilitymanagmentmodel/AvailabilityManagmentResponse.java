package com.wm.muggamu.Models.availabilitymanagmentmodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AvailabilityManagmentResponse
{
	@SerializedName("code")
	private int code;

	@SerializedName("message")
	private String message;

	public String getMessage() {
		return message;
	}

	@SerializedName("timeSlots")
	private List<TimeSlotsItem> timeSlots;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<TimeSlotsItem> getTimeSlots(){
		return timeSlots;
	}

	public boolean isError(){
		return error;
	}
}