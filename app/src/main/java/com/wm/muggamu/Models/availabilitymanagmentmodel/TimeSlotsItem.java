package com.wm.muggamu.Models.availabilitymanagmentmodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimeSlotsItem  {

	@SerializedName("slots")
	private List<SlotsItem> slots;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;



	public List<SlotsItem> getSlots(){
		return slots;
	}

	public int getId(){
		return id;
	}

	public String getTitle(){
		return title;
	}
}