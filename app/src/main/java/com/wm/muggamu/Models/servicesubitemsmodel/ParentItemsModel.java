package com.wm.muggamu.Models.servicesubitemsmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ParentItemsModel {

	@SerializedName("id")
	private int id;

	@SerializedName("category")
	private String category;

	@SerializedName("items")
	private List<ChildItemsModel> items;

	public int getId(){
		return id;
	}

	public String getCategory(){
		return category;
	}

	public List<ChildItemsModel> getItems(){
		return items;
	}
}