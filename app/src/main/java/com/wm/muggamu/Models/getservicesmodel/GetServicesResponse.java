package com.wm.muggamu.Models.getservicesmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetServicesResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("providerServices")
	private List<ServicesItemModel> providerServices;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<ServicesItemModel> getProviderServices(){
		return providerServices;
	}

	public boolean isError(){
		return error;
	}
}