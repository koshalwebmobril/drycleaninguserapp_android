package com.wm.muggamu.Models.trackordermodel;

import com.google.gson.annotations.SerializedName;

public class TrackOrderResultModel {

	@SerializedName("shipped_at")
	private String shippedAt;

	@SerializedName("pickup_location")
	private String pickupLocation;

	@SerializedName("pickup_driver_id")
	private int pickupDriverId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("pickup_date")
	private String pickupDate;

	@SerializedName("delivery_date")
	private String deliveryDate;

	@SerializedName("total_amount")
	private double totalAmount;

	@SerializedName("picked_up_at")
	private String pickedUpAt;

	@SerializedName("in_process")
	private String inProcess;

	@SerializedName("id")
	private int id;

	@SerializedName("pickup_time")
	private String pickupTime;

	@SerializedName("confirmed_at")
	private String confirmedAt;

	@SerializedName("order_id")
	private String orderId;

	@SerializedName("delivered_at")
	private String deliveredAt;

	@SerializedName("driver_mobile_number")
	private String driver_mobile_number;

	@SerializedName("delivery_driver_id")
	private int deliveryDriverId;

	public String getShippedAt(){
		return shippedAt;
	}

	public String getPickupLocation(){
		return pickupLocation;
	}

	public int getPickupDriverId(){
		return pickupDriverId;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public String getDeliveryDate(){
		return deliveryDate;
	}

	public double getTotalAmount(){
		return totalAmount;
	}

	public String getPickedUpAt(){
		return pickedUpAt;
	}

	public String getInProcess(){
		return inProcess;
	}

	public int getId(){
		return id;
	}

	public String getPickupTime(){
		return pickupTime;
	}

	public String getConfirmedAt(){
		return confirmedAt;
	}

	public String getOrderId(){
		return orderId;
	}

	public String getDeliveredAt(){
		return deliveredAt;
	}

	public String getDriver_mobile_number(){
		return driver_mobile_number;
	}

	public int getDeliveryDriverId(){
		return deliveryDriverId;
	}
}