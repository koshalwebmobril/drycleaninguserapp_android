package com.wm.muggamu.Models.serviceonpostmodel;

import com.google.gson.annotations.SerializedName;

public class ServicesListsModel {

	@SerializedName("service_name")
	private String name;

	@SerializedName("id")
	private int id;

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}
}