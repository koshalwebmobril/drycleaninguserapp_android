package com.wm.muggamu.Models.otpverifymodel;

import com.google.gson.annotations.SerializedName;

public class OtpVerifiyResponse{

	@SerializedName("userInfo")
	private OtpVerifiyModel otpVerifiyModel;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public OtpVerifiyModel getOtpVerifiyModel(){
		return otpVerifiyModel;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}