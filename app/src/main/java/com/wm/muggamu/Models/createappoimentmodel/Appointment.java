package com.wm.muggamu.Models.createappoimentmodel;

import com.google.gson.annotations.SerializedName;

public class Appointment{

	@SerializedName("booking_date")
	private String bookingDate;

	@SerializedName("appointment_id")
	private String appointmentId;

	@SerializedName("booking_time")
	private String bookingTime;

	@SerializedName("id")
	private int id;

	@SerializedName("provider_name")
	private String providerName;

	@SerializedName("provider_image")
	private String providerImage;

	public String getBookingDate(){
		return bookingDate;
	}

	public String getAppointmentId(){
		return appointmentId;
	}

	public String getBookingTime(){
		return bookingTime;
	}

	public int getId(){
		return id;
	}

	public String getProviderName(){
		return providerName;
	}

	public String getProviderImage(){
		return providerImage;
	}
}