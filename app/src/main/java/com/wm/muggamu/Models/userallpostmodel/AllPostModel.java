package com.wm.muggamu.Models.userallpostmodel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AllPostModel implements Serializable
{
	@SerializedName("service_time")
	private String serviceTime;

	@SerializedName("service_name")
	private String serviceName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("post_details")
	private List<PostDetailsItem> postDetails;

	@SerializedName("service_address")
	private String serviceAddress;

	@SerializedName("service_amount")
	private double serviceAmount;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("service_date")
	private String serviceDate;

	@SerializedName("service_id")
	private int serviceId;

	@SerializedName("id")
	private int id;

	@SerializedName("post_attachment")
	private String postAttachment;

	@SerializedName("remarks")
	private String remarks;

	@SerializedName("status")
	private int status;

	public String getServiceTime(){
		return serviceTime;
	}

	public String getServiceName(){
		return serviceName;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public List<PostDetailsItem> getPostDetails(){
		return postDetails;
	}

	public String getServiceAddress(){
		return serviceAddress;
	}

	public double getServiceAmount(){
		return serviceAmount;
	}

	public int getUserId(){
		return userId;
	}

	public String getServiceDate(){
		return serviceDate;
	}

	public int getServiceId(){
		return serviceId;
	}

	public int getId(){
		return id;
	}

	public String getPostAttachment(){
		return postAttachment;
	}

	public String getRemarks(){
		return remarks;
	}

	public int getStatus(){
		return status;
	}
}