package com.wm.muggamu.Models.serviceonpostmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ServiceOnPostResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("serviceList")
	private List<ServicesListsModel> serviceList;

	@SerializedName("error")
	private boolean error;

	public int getCode(){
		return code;
	}

	public List<ServicesListsModel> getServiceList(){
		return serviceList;
	}

	public boolean isError(){
		return error;
	}
}