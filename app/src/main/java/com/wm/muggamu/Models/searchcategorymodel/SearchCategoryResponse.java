package com.wm.muggamu.Models.searchcategorymodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.wm.muggamu.Models.homepagemodel.CategoriesItemModel;

public class SearchCategoryResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("message")
	private String message;


	public String getMessage() {
		return message;
	}

	@SerializedName("categories")
	private List<CategoriesItemModel> categories;

	@SerializedName("error")
	private boolean error;

	@SerializedName("sliders")
	private String sliders;

	public int getCode(){
		return code;
	}

	public List<CategoriesItemModel> getCategories(){
		return categories;
	}

	public boolean isError(){
		return error;
	}

	public String getSliders(){
		return sliders;
	}
}