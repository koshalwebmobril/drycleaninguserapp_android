package com.wm.muggamu.Models.allpostmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AllPostDataResponse{

	@SerializedName("bidsResult")
	private List<BidsResultModel> bidsResult;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	public List<BidsResultModel> getBidsResult(){
		return bidsResult;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}
}