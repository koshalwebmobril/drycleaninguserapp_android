package com.wm.muggamu.Models.yourbookingmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BookingResultResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	@SerializedName("bookings")
	private List<BookingsItemModel> bookings;

	public int getCode(){
		return code;
	}

	public String getMessage(){
		return message;
	}

	public boolean isError(){
		return error;
	}

	public List<BookingsItemModel> getBookings(){
		return bookings;
	}
}