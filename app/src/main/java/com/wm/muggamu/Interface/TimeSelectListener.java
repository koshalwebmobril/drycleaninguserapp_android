package com.wm.muggamu.Interface;

public interface TimeSelectListener
{
    void onSetAvailabilityStatus(int parentPosition, int childPosition, String time, int timeStatus);
}
