package com.wm.muggamu.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamu.ApiClient.RetrofitConnection;
import com.wm.muggamu.Chat.ChatActivity;
import com.wm.muggamu.Models.appoimentdetailmodel.AppoimentdetailsResponse;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;

import static android.view.View.GONE;

public class AppoimentDetailsActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView service_image,back;
    String address,appoiment_id;
    TextView txt_selecteddate,txt_selectedtime,search_address,service_name;
    Button btn_chat;
    AppoimentdetailsResponse resultFile;
    TextView review_status;
    String booking_date;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appoiment_details);
        init();
        address=LoginPreferences.getActiveInstance(this).getService_address();
        appoiment_id=getIntent().getStringExtra("appoiment_id");
        booking_date=getIntent().getStringExtra("booking_date");
        if(CommonMethod.isOnline(AppoimentDetailsActivity.this))
        {
            hitappoimentdetailsApi(appoiment_id);
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet),AppoimentDetailsActivity.this);
        }
    }
    public void init()
    {
        service_image=findViewById(R.id.service_image);
        txt_selecteddate=findViewById(R.id.txt_selecteddate);
        txt_selectedtime=findViewById(R.id.txt_selectedtime);
        search_address=findViewById(R.id.search_address);
        btn_chat=findViewById(R.id.btn_chat);
        back=findViewById(R.id.back);
        service_name=findViewById(R.id.service_name);
        review_status=findViewById(R.id.review_status);
        btn_chat.setOnClickListener(this);
        back.setOnClickListener(this);
        review_status.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.btn_chat:
                Intent i=new Intent(AppoimentDetailsActivity.this, ChatActivity.class);
                i.putExtra("provider_name",resultFile.getAppointmentDetails().getProviderName());
                i.putExtra("user_id",String.valueOf(resultFile.getAppointmentDetails().getUserId()));
                i.putExtra("provider_id",String.valueOf(resultFile.getAppointmentDetails().getProviderId()));
                i.putExtra("provider_img",resultFile.getAppointmentDetails().getProviderProfileImage());
                i.putExtra("provider_device_token",resultFile.getAppointmentDetails().getProviderDeviceToken());
                startActivity(i);
                break;


            case R.id.review_status:
                Intent review=new Intent(AppoimentDetailsActivity.this, ReviewPostActivity.class);
                review.putExtra("appointment_id",String.valueOf(resultFile.getAppointmentDetails().getId()));
                review.putExtra("booking_id",String.valueOf(""));
                review.putExtra("provider_id",String.valueOf(String.valueOf(resultFile.getAppointmentDetails().getProviderId())));
                startActivity(review);
                finish();
                break;
        }
    }
    private void hitappoimentdetailsApi(String appoiment_id)
    {
        final ProgressD progressDialog = ProgressD.show(this, getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<AppoimentdetailsResponse> call = service.Appoimentdetails(LoginPreferences.getActiveInstance(this).getToken(),
                appoiment_id);
        call.enqueue(new Callback<AppoimentdetailsResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AppoimentdetailsResponse> call, retrofit2.Response<AppoimentdetailsResponse> response) {
                progressDialog.dismiss();
                try {
                     resultFile = response.body();
                  //  Toast.makeText(AppoimentDetailsActivity.this,resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        Glide.with(AppoimentDetailsActivity.this).load(resultFile.getAppointmentDetails().getProviderProfileImage()).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile)).into(service_image);
                        txt_selecteddate.setText(booking_date);

                        search_address.setText(resultFile.getAppointmentDetails().getProviderAddress());
                        service_name.setText(resultFile.getAppointmentDetails().getProviderName());
                        txt_selectedtime.setText(resultFile.getAppointmentDetails().getTime());

                        if(resultFile.getAppointmentDetails().getAppointmentStatus() ==4 && resultFile.getAppointmentDetails().getisrated().equals("false"))
                        {
                            review_status.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            review_status.setVisibility(GONE);
                        }


                       /* if(resultFile.getAppointmentDetails().getAppointmentStatus() == 4)
                        {
                            review_status.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            review_status.setVisibility(GONE);
                        }*/
                    }
                    else if (resultFile.getCode() == 404)
                    { }

                    else
                    { }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AppoimentdetailsResponse> call, Throwable t) {
                Toast.makeText(AppoimentDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

}