package com.wm.muggamu.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.contactusmodel.ContactUsResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ContactUsActivity extends AppCompatActivity {

    ImageView back;
    EditText subject_name,comments;
    Button btn_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        back=findViewById(R.id.back);
        comments=findViewById(R.id.comments);
        subject_name=findViewById(R.id.subject_name);
        btn_submit=findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                // Toast.makeText(ContactUsActivity.this, "Details Submit Successfully", Toast.LENGTH_SHORT).show();
                if (CommonMethod.isOnline(ContactUsActivity.this))
                {
                    hideKeyboard((Button)v);
                    if(validation())
                    {
                        hitContactUsApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), ContactUsActivity.this);
                }
            }
        });




        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

    private boolean validation()
    {
        if(TextUtils.isEmpty(subject_name.getText().toString().trim()))
        {
            Toast.makeText(ContactUsActivity.this, "Please enter subject", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(comments.getText().toString().trim()))
        {
            Toast.makeText(ContactUsActivity.this, "Please enter comments", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public void hitContactUsApi()
    {
        final ProgressD progressDialog = ProgressD.show(ContactUsActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL1)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ContactUsResponse> call = service.contactus(LoginPreferences.getActiveInstance(ContactUsActivity.this).getToken(),subject_name.getText().toString().trim(),comments.getText().toString().trim());
        call.enqueue(new Callback<ContactUsResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ContactUsResponse> call, retrofit2.Response<ContactUsResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    ContactUsResponse resultFile = response.body();
                    Toast.makeText(ContactUsActivity.this,resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        //finish();
                        subject_name.setText("");
                        comments.setText("");
                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(ContactUsActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ContactUsResponse> call, Throwable t)
            {
                Toast.makeText(ContactUsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}