package com.wm.muggamu.Activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.wm.muggamu.Fragments.HomeFragment;
import com.wm.muggamu.Fragments.NotificationFragment;
import com.wm.muggamu.Fragments.UserPostFragment;
import com.wm.muggamu.Fragments.YourBookingFragment;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    ActivityMainBinding binding;
    private FragmentTransaction ft;
    private Fragment currentFragment;
    String user_name,mail_address,profile_image;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.layoutContent.toolbar.toolbarMain);
        init();
        if(getIntent().getExtras() != null)
        {
            String pagestatus= getIntent().getStringExtra("type");
            if(pagestatus.equals("1") || pagestatus.equals("3"))
            {
                Fragment currentFragment = new YourBookingFragment();
                loadFragmentOther(currentFragment);
            }
            else if(pagestatus.equals("2"))
            {
                Fragment currentFragment = new UserPostFragment();
                loadFragmentOther(currentFragment);
            }
            else
            {
                Fragment homeFragment = new HomeFragment();
                loadHomeFragment(homeFragment);
            }
        }
        else
        {
            Fragment homeFragment = new HomeFragment();
            loadHomeFragment(homeFragment);
        }
    }

    private void init()
    {
        user_name= LoginPreferences.getActiveInstance(MainActivity.this).getUserName();
        mail_address=LoginPreferences.getActiveInstance(MainActivity.this).getUserEmail();
        profile_image=LoginPreferences.getActiveInstance(MainActivity.this).getUserProfile();
        binding.drawerMenuItems.firstName.setText(user_name);
        binding.drawerMenuItems.gmail.setText(mail_address);
        Glide.with(MainActivity.this).load(profile_image).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(binding.drawerMenuItems.userImage);

        binding.layoutContent.toolbar.imgMenu.setOnClickListener(MainActivity.this);
        binding.drawerMenuItems.relativeProfile.setOnClickListener(this);
        binding.drawerMenuItems.relativecontactus.setOnClickListener(this);
        binding.drawerMenuItems.relativeprivacypolicy.setOnClickListener(this);
        binding.drawerMenuItems.relativetermcondition.setOnClickListener(this);
        binding.drawerMenuItems.linearLogout.setOnClickListener(this);
        binding.drawerMenuItems.crossbutton.setOnClickListener(this);
        binding.layoutContent.toolbar.addPost.setOnClickListener(this);
        binding.drawerMenuItems.relativeAddress.setOnClickListener(this);


        binding.layoutContent.bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        binding.layoutContent.bottomNavigation.setItemIconTintList(null);
    }


    public void toolbarHomeOther(String title_name)
    {
        binding.layoutContent.toolbar.lnMain.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.logoMain.setVisibility(View.INVISIBLE);
        binding.layoutContent.toolbar.txtTitleother.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.txtTitleother.setText(title_name);
        binding.layoutContent.toolbar.addPost.setVisibility(View.GONE);

        binding.layoutContent.toolbar.lnOtherTool.setVisibility(View.GONE);
        binding.layoutContent.bottomNavigation.setVisibility(View.VISIBLE);
        binding.layoutContent.bottomNavigation.getMenu().getItem(0).setChecked(true);
    }


    public void toolbarHomeForPost(String title_name)
    {
        binding.layoutContent.toolbar.lnMain.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.logoMain.setVisibility(View.INVISIBLE);
        binding.layoutContent.toolbar.txtTitleother.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.txtTitleother.setText(title_name);
        binding.layoutContent.toolbar.addPost.setVisibility(View.VISIBLE);

        binding.layoutContent.toolbar.lnOtherTool.setVisibility(View.GONE);
        binding.layoutContent.bottomNavigation.setVisibility(View.VISIBLE);
        binding.layoutContent.bottomNavigation.getMenu().getItem(0).setChecked(true);
    }


    public void updateBottomBar(int i)
    {
        binding.layoutContent.bottomNavigation.getMenu().getItem(i).setChecked(true);
    }


    public void toolbarHome()
    {
        binding.layoutContent.toolbar.lnMain.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.logoMain.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.txtTitleother.setVisibility(View.INVISIBLE);
        binding.layoutContent.toolbar.lnOtherTool.setVisibility(View.GONE);
        binding.layoutContent.bottomNavigation.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.addPost.setVisibility(View.GONE);
        binding.layoutContent.bottomNavigation.getMenu().getItem(0).setChecked(true);
    }


    private void loadHome(String anim)
    {
        ft = getSupportFragmentManager().beginTransaction();
        currentFragment = new HomeFragment();
        if (anim.equals("1"))
        {
            ft.setCustomAnimations(R.anim.left_in, R.anim.right_out);
        }
        ft.replace(R.id.frameMain, currentFragment);
        ft.commit();
        //getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.imgMenu)
        {
            if (!binding.drawerLayout.isDrawerOpen(GravityCompat.START))
                binding.drawerLayout.openDrawer(GravityCompat.START);
            else binding.drawerLayout.closeDrawer(GravityCompat.START);
        }

        if(v.getId() == R.id.relative_profile)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,ProfileActivity.class);
            startActivity(i);
        }

        if(v.getId() == R.id.add_post)
        {
            Intent i=new Intent(MainActivity.this,AddPostActivity.class);
          //  i.putExtra("first_time","1");
            startActivity(i);
        }
         if(v.getId() == R.id.relative_address)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,GetAddressActivity.class);
            LoginPreferences.getActiveInstance(MainActivity.this).setPageId("1");
            startActivity(i);
        }

        if(v.getId() == R.id.relativecontactus)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,ContactUsActivity.class);
            startActivity(i);
        }

        if(v.getId() == R.id.relativeprivacypolicy)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,PrivacyPolicyActivity.class);
            i.putExtra("page_title",getString(R.string.privacypolicy));
            startActivity(i);
        }

        if(v.getId() == R.id.relativetermcondition)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(MainActivity.this,TermConditionActivity.class);
            i.putExtra("page_title", "Terms & Conditions");
            startActivity(i);
        }

        if(v.getId() == R.id.crossbutton)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        }
        if(v.getId() == R.id.linearLogout)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getString(R.string.really_exit))
                    .setMessage(getString(R.string.are_sure_logout))
                    .setPositiveButton(getString(R.string.yes), (dialog, which) ->
                    {
                        Intent i=new Intent(MainActivity.this,LoginActivity.class);
                        LoginPreferences.deleteAllPreference();
                        startActivity(i);
                    })
                    .setNegativeButton(getString(R.string.no), null).show();
        }


    }

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item ->
    {
        Fragment f=getSupportFragmentManager().findFragmentById(R.id.frameMain);
        switch (item.getItemId())
        {
            case R.id.navHome:
                if(!(f instanceof HomeFragment))
                {
                    Fragment homeFragment = new HomeFragment();
                    loadHomeFragment(homeFragment);
                }
                return true;



            case R.id.navyourbooking:
                if(!(f instanceof YourBookingFragment))
                {
                    Fragment  currentFragment = new YourBookingFragment();
                    loadFragmentOther(currentFragment);
                }
                return true;



            case R.id.navplus:
                if(!(f instanceof UserPostFragment))
                {
                    Fragment  currentFragment = new UserPostFragment();
                    loadFragmentOther(currentFragment);
                }
                return true;

            case R.id.navnotification:
                if(!(f instanceof NotificationFragment))
                {
                    Fragment  currentFragment = new NotificationFragment();
                    loadFragmentOther(currentFragment);
                }
                return true;
        }
        return false;
    };


    private void loadHomeFragment(Fragment fragment)
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
       // fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out);
        fragmentTransaction.replace(R.id.frameMain, fragment);
        fragmentTransaction.commit();
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
    private void loadFragmentOther(Fragment fragment)
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameMain, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed()
    {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        }
        Fragment  currentFragment =getSupportFragmentManager().findFragmentById(R.id.frameMain);
        if(currentFragment instanceof YourBookingFragment ||currentFragment instanceof UserPostFragment|| currentFragment instanceof NotificationFragment)
        {
            // loadHome("0");
            Fragment homeFragment = new HomeFragment();
            loadHomeFragment(homeFragment);
        }
        else if(getSupportFragmentManager().getBackStackEntryCount() > 0)
        {
            getSupportFragmentManager().popBackStack();
        }

        else if(getSupportFragmentManager().getBackStackEntryCount()==0)
        {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getString(R.string.app_name))
                    .setMessage(getString(R.string.are_sure_exit))
                    .setPositiveButton(getString(R.string.yes), (dialog, which) -> com.wm.muggamu.Activities.MainActivity.this.finishAffinity())
                    .setNegativeButton(getString(R.string.no), null).show();
        }
    }



}