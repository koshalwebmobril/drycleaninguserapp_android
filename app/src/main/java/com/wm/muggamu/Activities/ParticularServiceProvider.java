package com.wm.muggamu.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.tabs.TabLayout;
import com.wm.muggamu.Adapter.ParticularServiceProviderAdapter;
import com.wm.muggamu.Fragments.ReviewFragment;
import com.wm.muggamu.Fragments.SelectDateFragment;
import com.wm.muggamu.Fragments.ServiceFragment;
import com.wm.muggamu.Models.businesstimemodel.BusinessTimeItem;
import com.wm.muggamu.Models.businesstimemodel.ProviderListItemModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;

import java.util.ArrayList;
import java.util.List;

public class ParticularServiceProvider extends AppCompatActivity {

    ViewPager simpleViewPager;
    TabLayout tabLayout;
    private ArrayList<Fragment> fragmentArrayList;
    private ArrayList<String> titleList;
    TextView service_provider_heading,open_close_time,address,txtrating,txt_distance;
    ImageView back;
    ImageView service_image;
    AutoCompleteTextView autoCompleteState;
    private ArrayAdapter<String> timeSlotAdapter;
    private ProviderListItemModel providerListItemModel;
    private List<BusinessTimeItem> businessTimeItemList;
    ArrayList<String> stateListName;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_particular_service_provider);
         init();

         if(LoginPreferences.getActiveInstance(this).getCategoryId().equals(getString(R.string.categoryid)))
         {

             autoCompleteState.setVisibility(View.VISIBLE);

             providerListItemModel= (ProviderListItemModel) getIntent().getSerializableExtra("service_model");
             businessTimeItemList=providerListItemModel.getBusinessTime();
             stateListName= new ArrayList<String>();
             for(int i = 0; i < businessTimeItemList.size() ; i++)
             {
                 stateListName.add(businessTimeItemList.get(i).getDaynameTime());
             }
             autoCompleteState.setOnClickListener(new View.OnClickListener()
             {
                 @Override
                 public void onClick(View v)
                 {
                     initilizeStateSpinner();
                 }
             });
         }

         if(LoginPreferences.getActiveInstance(ParticularServiceProvider.this).getCategoryId().equals(getString(R.string.categoryid)))
        {
            fragmentArrayList.add(new ServiceFragment());
            titleList.add("Services");
        }
        else
        {
            fragmentArrayList.add(new SelectDateFragment());
            titleList.add("Select Date");
        }

        fragmentArrayList.add(new ReviewFragment());
        titleList.add("Review");

        tabLayout.setupWithViewPager(simpleViewPager);
        simpleViewPager.setOffscreenPageLimit(3);

        ParticularServiceProviderAdapter adapter = new ParticularServiceProviderAdapter(this.getSupportFragmentManager(), fragmentArrayList,titleList);
        simpleViewPager.setAdapter(adapter);
        simpleViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void init()
    {
        autoCompleteState=findViewById(R.id.autoCompleteState);
        simpleViewPager = (ViewPager) findViewById(R.id.simpleViewPager);
        tabLayout = (TabLayout)findViewById(R.id.simpleTabLayout);
        fragmentArrayList=new ArrayList<>();
        titleList=new ArrayList<>();
        back=findViewById(R.id.back);
        service_provider_heading=findViewById(R.id.service_provider_heading);
        open_close_time=findViewById(R.id.open_close_time);
        address=findViewById(R.id.address);
        txtrating=findViewById(R.id.ratingtxt);
        txt_distance=findViewById(R.id.txt_distance);
        service_image=findViewById(R.id.service_image);

        service_provider_heading.setText(getIntent().getStringExtra("service_name"));
        open_close_time.setText(getIntent().getStringExtra("time"));
        address.setText(getIntent().getStringExtra("address"));
        txtrating.setText(getIntent().getStringExtra("rating"));
        txt_distance.setText(getIntent().getStringExtra("distance"));
        Glide.with(this).load(getIntent().getStringExtra("service_image")).
                apply(new RequestOptions().placeholder(R.drawable.image).error(R.drawable.image)).into(service_image);
        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initilizeStateSpinner()
    {
        timeSlotAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, stateListName);
        autoCompleteState.setAdapter(timeSlotAdapter);
        autoCompleteState.requestFocus();
        autoCompleteState.showDropDown();
        autoCompleteState.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                autoCompleteState.setText("");
               /* state_id = stateListId.get(position);
                state_tax = state_tax_list.get(position);*/
            }
        });
    }
}