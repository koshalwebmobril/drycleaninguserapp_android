package com.wm.muggamu.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Adapter.AddressAdapter;
import com.wm.muggamu.ApiClient.RetrofitConnection;
import com.wm.muggamu.Interface.AddressListner;
import com.wm.muggamu.Interface.OnClickRemoveAddress;
import com.wm.muggamu.Models.getaddressmodel.UserAddressModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.getaddressmodel.GetAddressResponse;
import com.wm.muggamu.Models.removeaddressmodel.RemoveAddressResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.content.ContentValues.TAG;

public class GetAddressActivity extends AppCompatActivity implements OnClickRemoveAddress,AddressListner {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.address_titile)
    TextView addressTitile;
    @BindView(R.id.no_address)
    TextView noaddress;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.recycler_address)
    RecyclerView recyclerAddress;
    AddressAdapter addressAdapter;
    @BindView(R.id.adress_text)
    TextView adressText;
    @BindView(R.id.btn_addnewaddress)
    TextView btnAddnewaddress;
    OnClickRemoveAddress onClickRemoveAddress;
    List<UserAddressModel> addresslist;
    AddressListner addressListner;
    private int requestCode;
    private int resultCode;
    private Intent data;
    String page_status="";
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private final static int PLACE_PICKER_REQUEST1 = 999;
    private String address_lat;
    String latitude,longitude;
    String address,city;
    private String zipcode;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_address);
        ButterKnife.bind(this);
        onClickRemoveAddress=this;
        addressListner=this;
        if(CommonMethod.isOnline(GetAddressActivity.this))
        {
            getAddress();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet),GetAddressActivity.this);
        }
    }

    public void getAddress()
    {
        final ProgressD progressDialog = ProgressD.show(GetAddressActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetAddressResponse> call = service.getAddress(LoginPreferences.getActiveInstance(GetAddressActivity.this).getToken());
        call.enqueue(new Callback<GetAddressResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAddressResponse> call, Response<GetAddressResponse> response) {
                progressDialog.dismiss();
                try {
                    GetAddressResponse resultFile = response.body();
                    if (resultFile.getCode() == 200)
                    {
                        noaddress.setVisibility(View.GONE);
                        recyclerAddress.setVisibility(View.VISIBLE);

                        addresslist = resultFile.getUserAddress();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(GetAddressActivity.this, RecyclerView.VERTICAL, false);
                        recyclerAddress.setLayoutManager(mLayoutManager);
                        addressAdapter = new AddressAdapter(GetAddressActivity.this, addresslist,onClickRemoveAddress,page_status,addressListner);
                        recyclerAddress.setAdapter(addressAdapter);
                    }
                    else if (resultFile.getCode() == 401)
                    {
                        // Toast.makeText(GetAddressActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    } else if (resultFile.getCode() == 404) {
                        noaddress.setVisibility(View.VISIBLE);
                        recyclerAddress.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAddressResponse> call, Throwable t) {
                Toast.makeText(GetAddressActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @OnClick({R.id.back,R.id.btn_addnewaddress})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back:
               finish();
                break;

            case R.id.btn_addnewaddress:
                checkLocationPermission();
                checkGPS();
                break;
        }
    }


    @Override
    public void removeaddress(int address_id,int position,String mark_clcik)
    {
        if(mark_clcik.equals("1"))
        {
            final ProgressD progressDialog = ProgressD.show(GetAddressActivity.this, getResources().getString(R.string.logging_in), true, false, null);
            ApiInterface service = RetrofitConnection.getInstance().createService();

            Call<RemoveAddressResponse> call = service.markasdefault(LoginPreferences.getActiveInstance(GetAddressActivity.this).getToken(),String.valueOf(address_id));
            call.enqueue(new Callback<RemoveAddressResponse>()
            {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(Call<RemoveAddressResponse> call, Response<RemoveAddressResponse> response) {
                    progressDialog.dismiss();
                    try
                    {
                        RemoveAddressResponse resultFile = response.body();
                        Toast.makeText(GetAddressActivity.this,resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                        if(resultFile.getCode() == 200)
                        {
                            addressAdapter.notifyDataSetChanged();
                        }
                        else if(resultFile.getCode() == 401)
                        {

                        }
                        else if (resultFile.getCode() == 404)
                        {
                        }
                    } catch (Exception e) {
                        Log.e("Login Faild", e.toString());
                    }
                }

                @Override
                public void onFailure(Call<RemoveAddressResponse> call, Throwable t) {
                    Toast.makeText(GetAddressActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        }
        else
        {
            final ProgressD progressDialog = ProgressD.show(GetAddressActivity.this, getResources().getString(R.string.logging_in), true, false, null);
            ApiInterface service = RetrofitConnection.getInstance().createService();
            Call<RemoveAddressResponse> call = service.removeAddress(LoginPreferences.getActiveInstance(GetAddressActivity.this).getToken(),String.valueOf(address_id));
            call.enqueue(new Callback<RemoveAddressResponse>()
            {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(Call<RemoveAddressResponse> call, Response<RemoveAddressResponse> response) {
                    progressDialog.dismiss();
                    try {
                        RemoveAddressResponse resultFile = response.body();
                        Toast.makeText(GetAddressActivity.this,resultFile.getMessage(), Toast.LENGTH_SHORT).show();

                        if(resultFile.getCode() == 200)
                        {
                            addresslist.remove(position);
                            addressAdapter.notifyDataSetChanged();
                        }
                        else if(resultFile.getCode() == 401)
                        {
                            // Toast.makeText(GetAddressActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                        } else if (resultFile.getCode() == 404) {
                        }
                    } catch (Exception e) {
                        Log.e("Login Faild", e.toString());
                    }
                }

                @Override
                public void onFailure(Call<RemoveAddressResponse> call, Throwable t) {
                    Toast.makeText(GetAddressActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        }
    }

    @Override
    public void addresslistner(String address)
    {
        SharedPreferences.Editor editor = getSharedPreferences("AddressPerference", MODE_PRIVATE).edit();
        editor.putString("address", address);
        editor.apply();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("ADDRESS_RESULT",address);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(GetAddressActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
            {
                openPlacePicker1();
            }
    }

    private void checkLocationPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION))
            {
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(GetAddressActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            }
            else
            {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }
    public void openPlacePicker1()
    {
        if (!Places.isInitialized())
        {
            Places.initialize(getApplicationContext(),getResources().getString(R.string.googlekey));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.ADDRESS,Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this);
        startActivityForResult(intent, PLACE_PICKER_REQUEST1);
    }

    private final static int PLACE_PICKER_REQUEST = 999;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
        checkPermissionOnActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case PLACE_PICKER_REQUEST:
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    Geocoder geocoder = new Geocoder(this);
                    try
                    {
                         List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude,place.getLatLng().longitude, 1);
                         address = addresses.get(0).getAddressLine(0);

                        /* String address1= addressall.replace(addresses.get(0).getLocality(), "");
                         String address2= address1.replace(addresses.get(0).getCountryName(), "");
                         String address3= address2.replace(addresses.get(0).getPostalCode(), "");
                         String address4= address3.replace(addresses.get(0).getAdminArea(), "");
                         address=address4.replaceAll("[-+.^:,]","");*/

                         city = addresses.get(0).getLocality();
                        zipcode = addresses.get(0).getPostalCode();
                        final LatLng latlng = place.getLatLng();
                        String s1= String.valueOf(latlng);
                        String[] s21 = s1.split(":");
                        String s23 = String.valueOf(s21[1]);
                        String s24 = s23.replaceAll("\\(", "");
                        address_lat = s24.replaceAll("\\)", "");
                        String[] latlong =  address_lat.split(",");
                         latitude = latlong[0];
                         longitude = latlong[1];
                         Log.i("TAG", "onClick: " + latitude);
                       //
                       // openAlertDailog();
                        Intent i=new Intent(GetAddressActivity.this,AddAddressActivity.class);
                        i.putExtra("address",address);
                        i.putExtra("city",city);
                        i.putExtra("zipcode",zipcode);
                        i.putExtra("latitude",latitude);
                        i.putExtra("longitude",longitude);
                        startActivity(i);
                        finish();
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
            }
        }
        else if (resultCode == AutocompleteActivity.RESULT_ERROR)
        {
            Status status = Autocomplete.getStatusFromIntent(data);
            Log.i(TAG, status.getStatusMessage());

        } else if (resultCode == RESULT_CANCELED)
        {

        }
    }

    private void checkPermissionOnActivityResult(int requestCode, int resultCode, Intent data) {
    }
}