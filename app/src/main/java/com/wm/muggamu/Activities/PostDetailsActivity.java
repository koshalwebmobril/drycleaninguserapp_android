package com.wm.muggamu.Activities;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Adapter.AddPostDetailsAdapter;
import com.wm.muggamu.Adapter.BidsResultAdapter;
import com.wm.muggamu.Interface.AcceptRejectBidListner;
import com.wm.muggamu.Models.userallpostmodel.PostDetailsItem;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.acceptbidresponse.AcceptBidsResponse;
import com.wm.muggamu.Models.allpostmodel.AllPostDataResponse;
import com.wm.muggamu.Models.allpostmodel.BidsResultModel;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class PostDetailsActivity extends AppCompatActivity  implements AcceptRejectBidListner{
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.Change_Password)
    TextView ChangePassword;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.service_img)
    ImageView serviceImg;
    @BindView(R.id.service_name)
    TextView serviceName;
    @BindView(R.id.service_time)
    TextView serviceTime;
    @BindView(R.id.service_address)
    TextView serviceAddress;
    @BindView(R.id.service_remarks)
    TextView serviceRemarks;
    @BindView(R.id.service_txt)
    TextView serviceTxt;
    @BindView(R.id.service_price)
    TextView servicePrice;
    @BindView(R.id.relative_post)
    LinearLayout relativePost;
    @BindView(R.id.profile_image1)
    CircleImageView profileImage1;
    @BindView(R.id.username1)
    TextView username1;
    @BindView(R.id.bid_price_info)
    TextView bidPriceInfo;
    @BindView(R.id.bid_time)
    TextView bidTime;
    @BindView(R.id.linear_bid_details)
    LinearLayout linearBidDetails;
    @BindView(R.id.linear_not_bid_yet)
    LinearLayout linearNotBidYet;
    @BindView(R.id.linear_item)
    LinearLayout linearItem;
    @BindView(R.id.recycle_userbid_details)
    RecyclerView recycleUserbidDetails;

    @BindView(R.id.recyclerview_postdetails)
    RecyclerView recyclerview_postdetails;

    BidsResultAdapter bidsResultAdapter;
    AcceptRejectBidListner acceptRejectBidListner;
    List<PostDetailsItem> postdetailslist;
    AddPostDetailsAdapter addPostDetailsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        acceptRejectBidListner= (AcceptRejectBidListner) this;

        ButterKnife.bind(this);
        Glide.with(this).load(LoginPreferences.getActiveInstance(this).getUserProfile()).error(R.drawable.dummy_icon).placeholder(R.drawable.dummy_icon)
                .thumbnail(0.06f)
                .into(profileImage);

        username.setText(LoginPreferences.getActiveInstance(this).getUserName());

        Glide.with(this).load(getIntent().getStringExtra("image")).error(R.drawable.clothes_img).placeholder(R.drawable.clothes_img)
                .thumbnail(0.06f)
                .into(serviceImg);

        postdetailslist= (List<PostDetailsItem>) getIntent().getSerializableExtra("PostDetailsItem");

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(PostDetailsActivity.this, RecyclerView.VERTICAL, false);
        recyclerview_postdetails.setLayoutManager(mLayoutManager);
        addPostDetailsAdapter = new AddPostDetailsAdapter(PostDetailsActivity.this, postdetailslist);
        recyclerview_postdetails.setAdapter(addPostDetailsAdapter);

        serviceName.setText(getIntent().getStringExtra("service_name"));
        serviceTime.setText(getIntent().getStringExtra("service_time"));
        servicePrice.setText(getIntent().getStringExtra("service_price"));
        serviceAddress.setText(getIntent().getStringExtra("service_address"));

       String serviceremark=getIntent().getStringExtra("service_remarks");
       if(serviceremark.equals("null"))
       {
           serviceRemarks.setText(getString(R.string.normarks));
       }
       else
       {
           serviceRemarks.setText(getIntent().getStringExtra("service_remarks"));
       }

       getBidsResult();
    }

    @OnClick(R.id.back)
    public void onClick()
    {
        finish();
    }

    public void getBidsResult()
    {
        final ProgressD progressDialog = ProgressD.show(PostDetailsActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<AllPostDataResponse> call = service.postdetails(LoginPreferences.getActiveInstance(PostDetailsActivity.this).getToken(),getIntent().getStringExtra("post_id"));
        call.enqueue(new Callback<AllPostDataResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AllPostDataResponse> call, Response<AllPostDataResponse> response)
            {
                progressDialog.dismiss();
                try {
                    AllPostDataResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        List<BidsResultModel> BidsResultModellist = resultFile.getBidsResult();
                       if(BidsResultModellist.size()>0)
                       {
                           LinearLayoutManager mLayoutManager = new LinearLayoutManager(PostDetailsActivity.this, RecyclerView.VERTICAL, false);
                           recycleUserbidDetails.setLayoutManager(mLayoutManager);
                           bidsResultAdapter = new BidsResultAdapter(PostDetailsActivity.this, BidsResultModellist,acceptRejectBidListner);
                           recycleUserbidDetails.setAdapter(bidsResultAdapter);
                       }
                       else
                       { }
                    }
                    else if(resultFile.getCode() == 401)
                    {
                        Toast.makeText(PostDetailsActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                    else if(resultFile.getCode() == 404)
                    {
                       // Toast.makeText(PostDetailsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AllPostDataResponse> call, Throwable t) {
                Toast.makeText(PostDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void acceptreject(int post_id, int bid_id, int status)
    {
        if(status==1)
        {
            acceptApi(String.valueOf(post_id),String.valueOf(bid_id));
        }
        else
        {
            rejectApi(String.valueOf(post_id),String.valueOf(bid_id));
        }
    }

    public void acceptApi(String post_id,String bid_id)
    {
        final ProgressD progressDialog = ProgressD.show(PostDetailsActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<AcceptBidsResponse> call = service.acceptbid(LoginPreferences.getActiveInstance(PostDetailsActivity.this).getToken(),post_id,bid_id);
        call.enqueue(new Callback<AcceptBidsResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AcceptBidsResponse> call, Response<AcceptBidsResponse> response) {
                progressDialog.dismiss();
                try
                {
                    AcceptBidsResponse resultFile = response.body();
                    Toast.makeText(PostDetailsActivity.this, resultFile.getMessage() ,Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        getBidsResult();
                    }
                    else if(resultFile.getCode() == 401)
                    {
                        Toast.makeText(PostDetailsActivity.this, resultFile.getMessage() ,Toast.LENGTH_SHORT).show();
                    }
                    else if(resultFile.getCode() == 404)
                    {

                    }
                } catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AcceptBidsResponse> call, Throwable t) {
                Toast.makeText(PostDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    public void rejectApi(String post_id,String bid_id)
    {
        final ProgressD progressDialog = ProgressD.show(PostDetailsActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<AcceptBidsResponse> call = service.rejectbid(LoginPreferences.getActiveInstance(PostDetailsActivity.this).getToken(),post_id,bid_id);
        call.enqueue(new Callback<AcceptBidsResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AcceptBidsResponse> call, Response<AcceptBidsResponse> response) {
                progressDialog.dismiss();
                try {
                    AcceptBidsResponse resultFile = response.body();
                    Toast.makeText(PostDetailsActivity.this, resultFile.getMessage() ,Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        getBidsResult();
                    }
                    else if(resultFile.getCode() == 401)
                    {
                        Toast.makeText(PostDetailsActivity.this, resultFile.getMessage() ,Toast.LENGTH_SHORT).show();
                    }
                    else if(resultFile.getCode() == 404)
                    {

                    }
                } catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AcceptBidsResponse> call, Throwable t) {
                Toast.makeText(PostDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

}