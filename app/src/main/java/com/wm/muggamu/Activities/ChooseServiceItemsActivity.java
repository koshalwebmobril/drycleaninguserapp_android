package com.wm.muggamu.Activities;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Adapter.ParentAdapter;
import com.wm.muggamu.Models.servicesubitemsmodel.ParentItemsModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.servicesubitemsmodel.GetServiceSubitemsResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ChooseServiceItemsActivity extends AppCompatActivity
{
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.address_titile)
    TextView addressTitile;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.recyclerview_items)
    RecyclerView recyclerviewItems;
    public List<ParentItemsModel> Subitemdetailslist = new ArrayList<>();
    ParentAdapter serviceitemadapter;
    @BindView(R.id.no_booking)
    TextView noBooking;


    @BindView(R.id.btn_save)
    Button btn_save;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_booking);
        ButterKnife.bind(this);
        if(CommonMethod.isOnline(ChooseServiceItemsActivity.this))
        {
            getSubItemApi();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet),ChooseServiceItemsActivity.this);
        }
    }

    @OnClick({R.id.back,R.id.btn_save})
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.btn_save:
                 finish();
                break;
        }
    }


    private void getSubItemApi()
    {
        final ProgressD progressDialog = ProgressD.show(ChooseServiceItemsActivity.this, getResources().getString(R.string.logging_in), true, false, null);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<GetServiceSubitemsResponse> call = service.getserviceSubitems(LoginPreferences.getActiveInstance(this).getToken(), getIntent().getStringExtra("service_id"),getIntent().getStringExtra("provider_id"));
        call.enqueue(new Callback<GetServiceSubitemsResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetServiceSubitemsResponse> call, Response<GetServiceSubitemsResponse> response) {
                progressDialog.dismiss();
                try {
                    GetServiceSubitemsResponse resultFile = response.body();
                    // Toast.makeText(ChooseClothesActivity.this, resultFile, Toast.LENGTH_SHORT).show();
                    if (resultFile.getCode() == 200)
                    {
                        btn_save.setVisibility(View.VISIBLE);
                        noBooking.setVisibility(View.GONE);
                        recyclerviewItems.setVisibility(View.VISIBLE);
                        Subitemdetailslist = resultFile.getSubitemList();
                        recyclerviewItems.setLayoutManager(new LinearLayoutManager(ChooseServiceItemsActivity.this, LinearLayoutManager.VERTICAL, false));
                        serviceitemadapter = new ParentAdapter(ChooseServiceItemsActivity.this, Subitemdetailslist);
                        recyclerviewItems.setAdapter(serviceitemadapter);
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        noBooking.setVisibility(View.VISIBLE);
                        recyclerviewItems.setVisibility(View.GONE);
                        noBooking.setText(resultFile.getMessage());
                        btn_save.setVisibility(View.GONE);
                    }
                    else {
                    }
                } catch (Exception e) {
                    Log.e("Forgot Password  Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetServiceSubitemsResponse> call, Throwable t) {
                Toast.makeText(ChooseServiceItemsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}