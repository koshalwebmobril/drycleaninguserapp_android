package com.wm.muggamu.Activities;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Adapter.AddPostParentAdapter;
import com.wm.muggamu.Models.addpostmodel.AddPostParentModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.addpostmodel.AddPostResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ChooseBidItemsActivity extends AppCompatActivity
{
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;

    @BindView(R.id.recyclerview_items)
    RecyclerView recyclerviewItems;
    public List<AddPostParentModel> addpostparentmodellist = new ArrayList<>();
    AddPostParentAdapter addpostparentadapter;
    @BindView(R.id.no_booking)
    TextView noBooking;

    @BindView(R.id.btn_confirm)
    Button btn_confirm;
    String service_id,service_name;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_biditems);
        ButterKnife.bind(this);
        service_id=getIntent().getStringExtra("service_id");
        service_name=getIntent().getStringExtra("service_name");
        LoginPreferences.getActiveInstance(this).setServiceId(service_id);
        getSubItemApi();
    }

    @OnClick({R.id.back,R.id.btn_confirm})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.btn_confirm:
                 finish();
                 break;
        }
    }


    private void getSubItemApi()
    {
        final ProgressD progressDialog = ProgressD.show(ChooseBidItemsActivity.this, getResources().getString(R.string.logging_in), true, false, null);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<AddPostResponse> call = service.getAddPost(LoginPreferences.getActiveInstance(this).getToken());
        call.enqueue(new Callback<AddPostResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AddPostResponse> call, Response<AddPostResponse> response) {
                progressDialog.dismiss();
                try {
                    AddPostResponse resultFile = response.body();
                    // Toast.makeText(ChooseClothesActivity.this, resultFile, Toast.LENGTH_SHORT).show();
                    if (resultFile.getCode() == 200)
                    {
                        /*noBooking.setVisibility(View.GONE);
                        recyclerviewItems.setVisibility(View.VISIBLE);*/

                        addpostparentmodellist = resultFile.getSubitemList();
                        recyclerviewItems.setLayoutManager(new LinearLayoutManager(ChooseBidItemsActivity.this, LinearLayoutManager.VERTICAL, false));
                        addpostparentadapter = new AddPostParentAdapter(ChooseBidItemsActivity.this, addpostparentmodellist,service_id,service_name);
                        recyclerviewItems.setAdapter(addpostparentadapter);
                    }
                    else if(resultFile.getCode() == 404)
                    {
                       // noBooking.setVisibility(View.VISIBLE);
                      //  recyclerviewItems.setVisibility(View.GONE);
                      //  noBooking.setText(resultFile.getMessage());
                    }
                    else {
                    }
                } catch (Exception e) {
                    Log.e("Forgot Password  Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AddPostResponse> call, Throwable t) {
                Toast.makeText(ChooseBidItemsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}