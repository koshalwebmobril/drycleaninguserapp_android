package com.wm.muggamu.Activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Adapter.CartParentAdapter;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.bookingconfirmmodel.BookingConfirmResponse;
import com.wm.muggamu.Room.CartItemDatabase;
import com.wm.muggamu.Room.CartItemEntity;
import com.wm.muggamu.Room.ParentCartItemsModel;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class BookingActivity extends AppCompatActivity {
    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.address_titile)
    TextView addressTitile;

    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;


    @BindView(R.id.txt_pickupdate)
    TextView txtPickupdate;

    @BindView(R.id.cardpickup_date)
    CardView cardserviceDate;

    @BindView(R.id.txt_deliver_date)
    TextView txtDeliverDate;

    @BindView(R.id.card_delivery_date)
    CardView cardDeliveryDate;
    @BindView(R.id.txt_pickuptime)
    TextView txtPickuptime;

    @BindView(R.id.card_pickup_time)
    CardView cardPickupTime;

    @BindView(R.id.txt_delivery_time)
    TextView txtDeliveryTime;
    @BindView(R.id.recycler_booking)
    RecyclerView recyclerBooking;
    @BindView(R.id.service_date)
    ImageView serviceDate;
    @BindView(R.id.delivery_date)
    ImageView deliveryDate;
    @BindView(R.id.service_pickup_time)
    ImageView servicePickupTime;
    @BindView(R.id.delivery_time)
    ImageView deliveryTime;
    @BindView(R.id.card_delivery_time)
    CardView cardDeliveryTime;
    @BindView(R.id.pin_location)
    ImageView pinLocation;
    @BindView(R.id.search_address)
    TextView searchAddress;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    RecyclerView recycler_booking;
    private int mHour, mMinute;
    private int mYear, mMonth, mDay;
    CartParentAdapter cartParentAdapter;
    String serviceIds = "";
    String subItemId = "";
    String subItemQty = "";

    String total_amount;
    double subitemtotal;
    String delivery_fee;
    List<ParentCartItemsModel> cartitemslist =new ArrayList<>();
    String provider_id;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        ButterKnife.bind(this);

        SharedPreferences preferences = getSharedPreferences("AddressPerference", 0);
        preferences.edit().remove("address").commit();

        provider_id=LoginPreferences.getActiveInstance(this).getProviderId();
        recycler_booking=findViewById(R.id.recycler_booking);
        total_amount = getIntent().getStringExtra("total_amount");
        delivery_fee = getIntent().getStringExtra("delivery_fee");
        subitemtotal= Double.parseDouble(total_amount)-Double.parseDouble(delivery_fee);

        List<Integer> parentIdList = CartItemDatabase.getInstance(this).cartItemDao().getCategoryIdList();
        cartitemslist =new ArrayList<>();
        for(int i = 0; i < parentIdList.size(); i++)
        {
            String parentNameList = CartItemDatabase.getInstance(this).cartItemDao().getParentName(parentIdList.get(i));
            List<CartItemEntity> childIdList = CartItemDatabase.getInstance(this).cartItemDao().getChildIdAndQuantity(parentIdList.get(i));
            ParentCartItemsModel parentCartItemsModel=new ParentCartItemsModel();
            parentCartItemsModel.setCategory(parentNameList);
            parentCartItemsModel.setItems(childIdList);
            cartitemslist.add(parentCartItemsModel);
            Log.i("TAG", "onClick111: " + childIdList);
        }
        Log.i("TAG", "onClick111: " + cartitemslist);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recycler_booking.setLayoutManager(mLayoutManager);
        cartParentAdapter = new CartParentAdapter(this,cartitemslist, total_amount,delivery_fee);
        recycler_booking.setAdapter(cartParentAdapter);
    }
    @OnClick({R.id.back, R.id.cardpickup_date, R.id.card_delivery_date, R.id.card_pickup_time, R.id.card_delivery_time, R.id.relative_search_location, R.id.btn_confirm})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.btn_confirm:
                 serviceIds = "";
                 subItemId = "";
                 subItemQty = "";

                List<Integer> parentIdList = CartItemDatabase.getInstance(this).cartItemDao().getCategoryIdList();
                serviceIds= TextUtils.join(",",parentIdList);
                for(int i = 0; i < parentIdList.size(); i++)
                {
                    List<CartItemEntity> childIdList = CartItemDatabase.getInstance(this).cartItemDao().getChildIdAndQuantity(parentIdList.get(i));
                    Log.i("TAG", "onClick111: " + childIdList);
                    subItemId = subItemId + "[";
                    subItemQty = subItemQty + "[";
                    for (int j = 0; j < childIdList.size(); j++)
                    {
                        if (j == 0)
                        {
                            subItemId = subItemId + childIdList.get(j).getChild_id();
                            subItemQty = subItemQty + childIdList.get(j).getChild_quantity();

                        } else {
                            subItemId = subItemId + "," + childIdList.get(j).getChild_id();
                            subItemQty = subItemQty + "," + childIdList.get(j).getChild_quantity();

                        }
                    }
                    if (parentIdList.size() == 0) {
                        subItemId = subItemId + "]";
                        subItemQty = subItemQty + "]";

                    } else {
                        if (i == parentIdList.size() - 1) {
                            subItemId = subItemId + "]";
                            subItemQty = subItemQty + "]";
                        } else {
                            subItemId = subItemId + "]^";
                            subItemQty = subItemQty + "]^";
                        }
                    }
                }
                Log.i("TAG", "onClick: " + serviceIds);
                Log.i("TAG", "onClick1: " + subItemId);
                Log.i("TAG", "onClick11: " + subItemQty);
                bookingApi();
                break;

            case R.id.relative_search_location:
                Intent i = new Intent(BookingActivity.this, GetAddressActivity.class);
                LoginPreferences.getActiveInstance(BookingActivity.this).setPageId("2");
                startActivityForResult(i,101);
                break;

            case R.id.cardpickup_date:
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(BookingActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                txtPickupdate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
                break;


            case R.id.card_delivery_date:
                final Calendar c2 = Calendar.getInstance();
                mYear = c2.get(Calendar.YEAR);
                mMonth = c2.get(Calendar.MONTH);
                mDay = c2.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog1 = new DatePickerDialog(BookingActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                txtDeliverDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog1.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog1.show();
                break;


            case R.id.card_pickup_time:
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                if (minute < 10) {
                                    txtPickuptime.setText(hourOfDay + ":" + "0" + minute);
                                } else {
                                    txtPickuptime.setText(hourOfDay + ":" + minute);
                                }
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
                break;
            case R.id.card_delivery_time:
                final Calendar c3 = Calendar.getInstance();
                mHour = c3.get(Calendar.HOUR_OF_DAY);
                mMinute = c3.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog1 = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                if (minute < 10) {
                                    txtDeliveryTime.setText(hourOfDay + ":" + "0" + minute);
                                } else {
                                    txtDeliveryTime.setText(hourOfDay + ":" + minute);
                                }
                            }
                        }, mHour, mMinute, false);
                timePickerDialog1.show();
                break;
        }


    }

    public void bookingApi()
    {
        final ProgressD progressDialog = ProgressD.show(BookingActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<BookingConfirmResponse> call = service.createBooking(LoginPreferences.getActiveInstance(BookingActivity.this).getToken(),
                provider_id,serviceIds, subItemId, subItemQty, searchAddress.getText().toString().trim(),
                txtPickupdate.getText().toString().trim(),
                txtPickuptime.getText().toString().trim(), txtDeliverDate.getText().toString(),
                txtDeliveryTime.getText().toString().trim()
                , String.valueOf(subitemtotal));
        call.enqueue(new Callback<BookingConfirmResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<BookingConfirmResponse> call, Response<BookingConfirmResponse> response) {
                progressDialog.dismiss();
                try {
                    BookingConfirmResponse resultFile = response.body();
                    Toast.makeText(BookingActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if (resultFile.getCode() == 200)
                    {
                            DecimalFormat df = new DecimalFormat("0.00");

                            Intent i=new Intent(BookingActivity.this,BookingConfirmActivity.class);
                           i.putExtra("shop_name",resultFile.getCreateBookingModel().getProviderName());
                           i.putExtra("order_no",String.valueOf(resultFile.getCreateBookingModel().getOrderId()));
                           i.putExtra("total_amount",String.valueOf(df.format(resultFile.getCreateBookingModel().getTotalAmount())));
                           i.putExtra("pickup_date_time",resultFile.getCreateBookingModel().getPickupDate()+" at "+resultFile.getCreateBookingModel().getPickupTime());
                           i.putExtra("delivery_date_time",resultFile.getCreateBookingModel().getDeliveryDate()+" at "+resultFile.getCreateBookingModel().getDeliveryTime());
                           i.putExtra("pickup_address",resultFile.getCreateBookingModel().getPickupLocation());
                           i.putExtra("image",resultFile.getCreateBookingModel().getProvider_image());
                           startActivity(i);
                           finish();
                           CartItemDatabase.getInstance(getApplicationContext()).cartItemDao().emptyList();
                           SharedPreferences preferences = getSharedPreferences("AddressPerference", 0);
                           preferences.edit().remove("address").commit();
                    }
                    else if (resultFile.getCode() == 401)
                    {
                        //  Toast.makeText(BookingActivity.this, , Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<BookingConfirmResponse> call, Throwable t) {
                Toast.makeText(BookingActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
       // searchAddress.setText(AddressPreferences.getActiveInstance(BookingActivity.this).getAddress());
        SharedPreferences prefs = getSharedPreferences("AddressPerference", MODE_PRIVATE);
        searchAddress.setText(prefs.getString("address", ""));//"No name defined" is the default value.
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == 101)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                String selectedLocation = data.getStringExtra("ADDRESS_RESULT");
               // searchAddress.setText(selectedLocation);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
               // searchAddress.setText("");
            }
        }
    }
}