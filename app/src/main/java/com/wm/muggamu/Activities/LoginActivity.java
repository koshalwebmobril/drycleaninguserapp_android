package com.wm.muggamu.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hbb20.CountryCodePicker;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.loginmodel.LoginResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener
{
    Button btn_login;
    EditText mobile;
    private CountryCodePicker ccp;
    String county_code;
    String notification_token;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ccp=findViewById(R.id.ccp);

        btn_login=findViewById(R.id.btn_login);
        mobile=findViewById(R.id.mobile);
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_login:

                if (CommonMethod.isOnline(this))
                {
                    county_code=ccp.getSelectedCountryCode();
                    notification_token = FirebaseInstanceId.getInstance().getToken();
                    hideKeyboard((Button) v);
                    if (validation())
                    {
                        hitLoginUserApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;

        }
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private boolean validation()
    {
        if (TextUtils.isEmpty(mobile.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter Mobile No", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void hitLoginUserApi()
    {
        final ProgressD progressDialog = ProgressD.show(LoginActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<LoginResponse> call = service.LoginUser(county_code + mobile.getText().toString() ,"2","2",notification_token);
        call.enqueue(new Callback<LoginResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    LoginResponse resultFile = response.body();
                    Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        Intent i=new Intent(LoginActivity.this, OtpVerificationActivity.class);
                        i.putExtra("mobileno",county_code + mobile.getText().toString().trim());
                        LoginPreferences.getActiveInstance(LoginActivity.this).setUserId(String.valueOf(resultFile.getLoginModel().getId()));
                        startActivity(i);
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t)
            {
                Toast.makeText(LoginActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    @Override
    public void onBackPressed() {
        finishAffinity();
        finish();
    }
}