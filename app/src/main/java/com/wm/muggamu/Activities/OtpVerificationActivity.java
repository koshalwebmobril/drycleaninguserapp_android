package com.wm.muggamu.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Models.loginmodel.LoginResponse;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.otpverifymodel.OtpVerifiyResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.GPSTracker;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import in.aabhasjindal.otptextview.OtpTextView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class OtpVerificationActivity extends AppCompatActivity implements View.OnClickListener
{
    Button btn_confirm;
    private OtpTextView otpTextView;
    String otpuser,user_id;
    ImageView back_arrow;
    String notification_token;
    String user_lat,user_lng;
    GPSTracker gpsTracker;
    TextView resend_otp;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification_otp);
        btn_confirm=findViewById(R.id.btn_confirm);
        otpTextView=findViewById(R.id.otp_view);
        btn_confirm.setOnClickListener(this);
        back_arrow=findViewById(R.id.back_arrow);
        resend_otp=findViewById(R.id.resend_otp);
        back_arrow.setOnClickListener(this);
        resend_otp.setOnClickListener(this);
        notification_token = FirebaseInstanceId.getInstance().getToken();
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_confirm:
                if (CommonMethod.isOnline(OtpVerificationActivity.this))
                {
                    notification_token = FirebaseInstanceId.getInstance().getToken();
                    hideKeyboard((Button)v);
                    checkPermission();
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), OtpVerificationActivity.this);
                }
                break;

            case R.id.back_arrow:
                 finish();
                 break;

            case R.id.resend_otp:
                hitLoginUserApi();
                break;

        }
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private void hitOtpVerifyApi()
    {
        final ProgressD progressDialog = ProgressD.show(OtpVerificationActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<OtpVerifiyResponse> call = service.OtpVerifie(LoginPreferences.getActiveInstance(this).getUserId(),otpuser,"2","2",notification_token,user_lat,user_lng);
        call.enqueue(new Callback<OtpVerifiyResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<OtpVerifiyResponse> call, retrofit2.Response<OtpVerifiyResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    OtpVerifiyResponse resultFile = response.body();
                    Toast.makeText(OtpVerificationActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        Intent i=new Intent(OtpVerificationActivity.this,MainActivity.class);
                        startActivity(i);
                        LoginPreferences.getActiveInstance(OtpVerificationActivity.this).setToken("Bearer "+ resultFile.getOtpVerifiyModel().getToken());
                        if(resultFile.getOtpVerifiyModel().getName()==null)
                        {
                            LoginPreferences.getActiveInstance(OtpVerificationActivity.this).setUserName("");
                            LoginPreferences.getActiveInstance(OtpVerificationActivity.this).setUserEmail("");
                            LoginPreferences.getActiveInstance(OtpVerificationActivity.this).setUserProfile("");
                        }
                        else
                        {
                            LoginPreferences.getActiveInstance(OtpVerificationActivity.this).setUserName((String) resultFile.getOtpVerifiyModel().getName());
                            LoginPreferences.getActiveInstance(OtpVerificationActivity.this).setMobileno(resultFile.getOtpVerifiyModel().getMobile());
                            LoginPreferences.getActiveInstance(OtpVerificationActivity.this).setUserEmail(resultFile.getOtpVerifiyModel().getEmail());
                            LoginPreferences.getActiveInstance(OtpVerificationActivity.this).setUserProfile(resultFile.getOtpVerifiyModel().getProfileImagePath());
                        }
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<OtpVerifiyResponse> call, Throwable t)
            {
                Toast.makeText(OtpVerificationActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS();
        }
        else
        {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(this, "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }
    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(OtpVerificationActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            otpuser=otpTextView.getOTP();
            if(otpuser.length() < 4)
            {
                Toast.makeText(OtpVerificationActivity.this, "Please enter OTP first", Toast.LENGTH_SHORT).show();
                return;
            }
            else
            {
                gpsTracker=new GPSTracker(this);
                user_lat=String.valueOf(gpsTracker.getLatitude());
                user_lng=String.valueOf(gpsTracker.getLongitude());
                hitOtpVerifyApi();
            }
        }
        }

    private void hitLoginUserApi()
    {
        final ProgressD progressDialog = ProgressD.show(OtpVerificationActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<LoginResponse> call = service.LoginUser(getIntent().getStringExtra("mobileno") ,"2","2",notification_token);
        call.enqueue(new Callback<LoginResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    LoginResponse resultFile = response.body();
                    Toast.makeText(OtpVerificationActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {

                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t)
            {
                Toast.makeText(OtpVerificationActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
    }
