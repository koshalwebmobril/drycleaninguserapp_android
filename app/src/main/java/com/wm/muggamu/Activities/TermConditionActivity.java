package com.wm.muggamu.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wm.muggamu.R;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;

public class TermConditionActivity extends AppCompatActivity {

    String pagetitle;
    TextView title; ImageView back;
    WebView mwebview;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        back=findViewById(R.id.back);
        mwebview=findViewById(R.id.mwebview);
        title=findViewById(R.id.title);
        pagetitle=getIntent().getStringExtra("page_title");
        title.setText(pagetitle);
        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mwebview.getSettings().setJavaScriptEnabled(true); // enable javascript
        final Activity activity = this;
        final ProgressD progressDialog = ProgressD.show(TermConditionActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        mwebview.setWebViewClient(new WebViewClient()
        {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                progressDialog.dismiss();
            }
            @Override
            public void onPageFinished(WebView view, String url)
            {
                super.onPageFinished(view, url);
                progressDialog.dismiss();
            }
        });
        mwebview.loadUrl(UrlApi.BASE_URL2+UrlApi.TERMS);
    }
}