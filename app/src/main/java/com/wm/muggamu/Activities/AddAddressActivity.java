package com.wm.muggamu.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.addaddressresponse.AddAddressResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;
import com.xw.repo.XEditText;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.content.ContentValues.TAG;

public class AddAddressActivity extends AppCompatActivity
{
    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.address_titile)
    TextView addressTitile;

    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.address1)
    XEditText address1;

    @BindView(R.id.landmark)
    XEditText landmark;

    @BindView(R.id.city)
    XEditText city1;

    @BindView(R.id.zip_code)
    XEditText zipCode;

    @BindView(R.id.title)
    XEditText title;

    @BindView(R.id.checkbox)
    CheckBox checkbox;

    @BindView(R.id.btn_save)
    Button btnSave;
    String latitude,longitude;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private final static int PLACE_PICKER_REQUEST1 = 999;
    private int requestCode;
    private int resultCode;
    private Intent data;
    String address,city;
    private String zipcode,address_lat;
    String is_default;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);

        if(getIntent().getExtras() != null)
        {
             address1.setText(getIntent().getStringExtra("address"));
             city1.setText(getIntent().getStringExtra("city"));
             zipCode.setText(getIntent().getStringExtra("zipcode"));

             latitude=getIntent().getStringExtra("latitude");
             longitude=getIntent().getStringExtra("longitude");
        }
        else
        {
        }
    }
    @OnClick({R.id.btn_save,R.id.back,R.id.address1,R.id.checkbox})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back:
               // finish();
                Intent getaddress=new Intent(AddAddressActivity.this,GetAddressActivity.class);
                startActivity(getaddress);
                finish();
                break;

            case R.id.btn_save:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((Button) view);
                    if (validation())
                    {
                        hitaddAddressApi();
                    }
                } else
                    {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                  }
                break;

            case R.id.address1:
                   if(address1.length()<=0)
                        {
                            checkLocationPermission();
                            checkGPS();
                        }
                        else
                        {
                            //  address.setFocusable(true);
                            // address.setSelection(address.getText().length());
                        }
                        break;

            case R.id.checkbox:
                if(checkbox.isChecked())
                {
                    is_default="1";
                }
                else
                {
                    is_default="";
                }
                break;
        }
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private boolean validation()
    {
        if(title.getText().toString().trim().length()>25)
          {
              Toast.makeText(AddAddressActivity.this, "Title max to 25 char", Toast.LENGTH_SHORT).show();
               return false;
          }
        else if(TextUtils.isEmpty(address1.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Address", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(city1.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter city", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(zipCode.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter zip code", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(zipCode.getText().toString().trim().length()<5 || zipCode.getText().toString().trim().length()>7 )
        {
            Toast.makeText(AddAddressActivity.this, "Zip code must be 5 to 7 digit", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!checkbox.isChecked())
        {
            Toast.makeText(this, "Please Select Mark as Default", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    public void hitaddAddressApi()
    {
        final ProgressD progressDialog = ProgressD.show(AddAddressActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<AddAddressResponse> call = service.addAddress(LoginPreferences.getActiveInstance(AddAddressActivity.this).getToken(),address1.getText().toString().trim(),city1.getText().toString().trim(),latitude,longitude,title.getText().toString().trim(),
                landmark.getText().toString().trim(),zipCode.getText().toString().trim(),is_default);
        call.enqueue(new Callback<AddAddressResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    AddAddressResponse resultFile = response.body();
                    Toast.makeText(AddAddressActivity.this,resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                      Intent i=new Intent(AddAddressActivity.this,GetAddressActivity.class);
                      startActivity(i);
                      finish();
                    }

                    else if(resultFile.getCode()== 401)
                    {
                       // Toast.makeText(AddAddressActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t)
            {
                Toast.makeText(AddAddressActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void checkLocationPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION))
            {
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(AddAddressActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            }
            else
            {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }
    public void openPlacePicker1()
    {
        if (!Places.isInitialized())
        {
            Places.initialize(getApplicationContext(),getResources().getString(R.string.googlekey));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.ADDRESS,Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this);
        startActivityForResult(intent, PLACE_PICKER_REQUEST1);
    }

    private final static int PLACE_PICKER_REQUEST = 999;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.data = data;
        checkPermissionOnActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case PLACE_PICKER_REQUEST:
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    Geocoder geocoder = new Geocoder(this);
                    try
                    {
                        List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude,place.getLatLng().longitude, 1);
                         address = addresses.get(0).getAddressLine(0);

                        /* String address1= addressall.replace(addresses.get(0).getLocality(), "");
                         String address2= address1.replace(addresses.get(0).getCountryName(), "");
                         String address3= address2.replace(addresses.get(0).getPostalCode(), "");
                         String address4= address3.replace(addresses.get(0).getAdminArea(), "");
                         address=address4.replaceAll("[-+.^:,]","");*/

                        city = addresses.get(0).getLocality();
                        zipcode = addresses.get(0).getPostalCode();
                        final LatLng latlng = place.getLatLng();
                        String s1= String.valueOf(latlng);
                        String[] s21 = s1.split(":");
                        String s23 = String.valueOf(s21[1]);
                        String s24 = s23.replaceAll("\\(", "");
                        address_lat = s24.replaceAll("\\)", "");
                        String[] latlong =  address_lat.split(",");
                        latitude = latlong[0];
                        longitude = latlong[1];
                        Log.i("TAG", "onClick: " + latitude);
                        //
                        // openAlertDailog();
                        Intent i=new Intent(AddAddressActivity.this,AddAddressActivity.class);
                        i.putExtra("address",address);
                        i.putExtra("city",city);
                        i.putExtra("zipcode",zipcode);
                        i.putExtra("latitude",latitude);
                        i.putExtra("longitude",longitude);
                        startActivity(i);
                        finish();
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
            }
        }
        else if (resultCode == AutocompleteActivity.RESULT_ERROR)
        {
            Status status = Autocomplete.getStatusFromIntent(data);
            Log.i(TAG, status.getStatusMessage());

        } else if (resultCode == RESULT_CANCELED)
        {

        }
    }

    private void checkPermissionOnActivityResult(int requestCode, int resultCode, Intent data) {
    }

    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(AddAddressActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            openPlacePicker1();
        }
    }
}