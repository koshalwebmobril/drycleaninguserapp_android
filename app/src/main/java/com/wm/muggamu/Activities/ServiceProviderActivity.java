package com.wm.muggamu.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wm.muggamu.Adapter.ServiceProviderAdapter;
import com.wm.muggamu.Models.businesstimemodel.ProviderListItemModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.getproviderbycategorymodel.GetProviderByCategoryResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
public class ServiceProviderActivity extends AppCompatActivity {
    RecyclerView recyclerview_nearbylaundry;
    ServiceProviderAdapter serviceProviderAdapter;
    String category_id,category_name;
    ImageView back,search;
    EditText editsearch;
    TextView txtnoresult,service_name,service_name1;
    List<ProviderListItemModel> providerlistitemmodellist;
    String providerimage;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider);
        init();
        if(CommonMethod.isOnline(this))
        {
            getProviderByCategory();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet),this);
        }
    }
    public void init()
    {
        recyclerview_nearbylaundry=findViewById(R.id.recyclerview_nearbylaundry);
        category_id=getIntent().getStringExtra("category_id");
        back=findViewById(R.id.back);
        editsearch=findViewById(R.id.editsearch);
        txtnoresult=findViewById(R.id.txtnoresult);
        search=findViewById(R.id.search);
        service_name=findViewById(R.id.service_name);
        service_name1=findViewById(R.id.service_name1);
        service_name.setText(getIntent().getStringExtra("category_name"));
        service_name1.setText(getString(R.string.Nearby)+" "+ getIntent().getStringExtra("category_name"));



        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        editsearch.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    hitSearchApi(String.valueOf(editsearch.getText().toString()));
                    hideKeyboard(back);
                    return true;
                }
                return false;
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(editsearch.getText().toString().trim().equals("") && editsearch.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(ServiceProviderActivity.this,getString(R.string.pleaseentervalidkey), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    hitSearchApi(String.valueOf(editsearch.getText().toString()));
                    hideKeyboard(view);
                }
            }
        });


        editsearch.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d("tag",s.toString());
                if(String.valueOf(s).equals(""))
                {
                    if(CommonMethod.isOnline(ServiceProviderActivity.this))
                    {
                        getProviderByCategory();
                    }
                    else
                    {
                        CommonMethod.showAlert(getString(R.string.check_internet),ServiceProviderActivity.this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private void getProviderByCategory()
    {
        final ProgressD progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetProviderByCategoryResponse> call = service.getProviderByCategory(LoginPreferences.getActiveInstance(this).getToken(),category_id);
        call.enqueue(new Callback<GetProviderByCategoryResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetProviderByCategoryResponse> call, retrofit2.Response<GetProviderByCategoryResponse> response)
            {
                progressDialog.dismiss();
                txtnoresult.setVisibility(View.GONE);
                recyclerview_nearbylaundry.setVisibility(View.VISIBLE);
                try
                {
                    GetProviderByCategoryResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                         providerlistitemmodellist= resultFile.getProviderList();
                         recyclerview_nearbylaundry.setLayoutManager(new LinearLayoutManager(ServiceProviderActivity.this, LinearLayoutManager.VERTICAL, false));
                         serviceProviderAdapter = new ServiceProviderAdapter(ServiceProviderActivity.this,providerlistitemmodellist);
                         recyclerview_nearbylaundry.setAdapter(serviceProviderAdapter);
                    }
                    else
                    {
                         Toast.makeText(ServiceProviderActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetProviderByCategoryResponse> call, Throwable t)
            {
                Toast.makeText(ServiceProviderActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void hitSearchApi(String keyword)
    {
        final ProgressD progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetProviderByCategoryResponse> call = service.searchcategory(LoginPreferences.getActiveInstance(ServiceProviderActivity.this).getToken(),keyword,category_id);
        call.enqueue(new Callback<GetProviderByCategoryResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetProviderByCategoryResponse> call, retrofit2.Response<GetProviderByCategoryResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetProviderByCategoryResponse resultFile = response.body();
                    // Toast.makeText(getActivity(), resultFile.g, Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        recyclerview_nearbylaundry.setVisibility(View.VISIBLE);
                        txtnoresult.setVisibility(View.GONE);
                        providerlistitemmodellist.clear();

                        providerlistitemmodellist= resultFile.getProviderList();
                        recyclerview_nearbylaundry.setLayoutManager(new LinearLayoutManager(ServiceProviderActivity.this, LinearLayoutManager.VERTICAL, false));
                        serviceProviderAdapter = new ServiceProviderAdapter(ServiceProviderActivity.this,providerlistitemmodellist);
                        recyclerview_nearbylaundry.setAdapter(serviceProviderAdapter);
                        //  myServiceAdapter.notifyDataSetChanged();
                    }
                    else if(resultFile.getCode() == 404)
                    {
                        providerlistitemmodellist.clear();
                        txtnoresult.setVisibility(View.VISIBLE);
                        recyclerview_nearbylaundry.setVisibility(View.GONE);
                        txtnoresult.setText(resultFile.getMessage());
                    }
                    else
                    {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetProviderByCategoryResponse> call, Throwable t)
            {
                Toast.makeText(ServiceProviderActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }






}