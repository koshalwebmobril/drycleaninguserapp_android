package com.wm.muggamu.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.wm.muggamu.ApiClient.RetrofitConnection;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.trackordermodel.TrackOrderDetailsResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.network.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackOrderActivity extends AppCompatActivity
{
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.order_no)
    TextView orderNo;
    @BindView(R.id.txtprice)
    TextView txtprice;

    @BindView(R.id.order_date_time)
    TextView orderDateTime;
    @BindView(R.id.img_confiremed)
    ImageView imgConfiremed;
    @BindView(R.id.txt_order_confirm)
    TextView txtOrderConfirm;
    @BindView(R.id.time_confirmed)
    TextView time_confirmed;
    @BindView(R.id.img_confiremed_invisible)
    ImageView imgConfiremedInvisible;
    @BindView(R.id.txt_order_confirm_invisible)
    TextView txtOrderConfirmInvisible;
    @BindView(R.id.confirmedlinevisible)
    View confirmedlinevisible;
    @BindView(R.id.confirmedlineinvisible)
    View confirmedlineinvisible;
    @BindView(R.id.img_pickedup_visible)
    ImageView imgPickedupVisible;
    @BindView(R.id.txt_visiblepicked_up)
    TextView txtVisiblepickedUp;
    @BindView(R.id.picked_visible_up_time)
    TextView pickedVisibleUpTime;
    @BindView(R.id.relative_pickup_visible)
    RelativeLayout relativePickupVisible;

    @BindView(R.id.img_pickedup)
    ImageView imgPickedup;
    @BindView(R.id.txt_picked_up)
    TextView txtPickedUp;
   /* @BindView(R.id.txtpickupaddress)
    TextView txtpickupaddress;*/
    @BindView(R.id.picked_up_time)
    TextView pickedUpTime;
    @BindView(R.id.relative_invisible_pickedup)
    RelativeLayout relativeInvisiblePickedup;
    @BindView(R.id.picked_line_visible)
    View pickedLineVisible;
    @BindView(R.id.picked_invisible)
    View pickedInvisible;
    @BindView(R.id.img_inprocess_visible)
    ImageView imgInprocessVisible;
    @BindView(R.id.txt_visibleinprocess_up)
    TextView txtVisibleinprocessUp;
    @BindView(R.id.picked_inprocess_time)
    TextView pickedInprocessTime;
    @BindView(R.id.relative_inprocess_visible)
    RelativeLayout relativeInprocessVisible;
    @BindView(R.id.img_inprocess)
    ImageView imgInprocess;
    @BindView(R.id.txt_inprocess)
    TextView txtInprocess;
    @BindView(R.id.relative_invisible_inprocess)
    RelativeLayout relativeInvisibleInprocess;
    @BindView(R.id.inprocess_line_visible)
    View inprocessLineVisible;
    @BindView(R.id.inprocess_line_invisible)
    View inprocessLineInvisible;
    @BindView(R.id.img_shiped_visible)
    ImageView imgShipedVisible;
    @BindView(R.id.txt_shiped)
    TextView txtShiped;
    @BindView(R.id.picked_shiped_time)
    TextView pickedShipedTime;
    @BindView(R.id.relative_shiped_visible)
    RelativeLayout relativeShipedVisible;
    @BindView(R.id.img_shiped_invisible)
    ImageView imgShipedInvisible;
    @BindView(R.id.txt_invisible_shiped)
    TextView txtInvisibleShiped;
    @BindView(R.id.relative_shiped_invisible)
    RelativeLayout relativeShipedInvisible;
    @BindView(R.id.deliverd_line_visible)
    View deliverdLineVisible;
    @BindView(R.id.deliverd_line_invisible)
    View deliverd_line_invisible;
    @BindView(R.id.img_delivered_visible)
    ImageView imgDeliveredVisible;
    @BindView(R.id.txt_deliverd)
    TextView txtDeliverd;
    @BindView(R.id.deliverd_inprocess_time)
    TextView deliverdInprocessTime;
    @BindView(R.id.relative_delivered_visible)
    RelativeLayout relativeDeliveredVisible;
    @BindView(R.id.img_deliverd_invisible)
    ImageView imgDeliverdInvisible;
    @BindView(R.id.txt_invisible_deliverd)
    TextView txtInvisibleDeliverd;
    @BindView(R.id.relative_delivered_invisible)
    RelativeLayout relativeDeliveredInvisible;
    @BindView(R.id.relative_confiremed_visible)
    RelativeLayout relativeConfiremedVisible;
    @BindView(R.id.relative_confiremed_invisible)
    RelativeLayout relativeConfiremedInvisible;
    @BindView(R.id.cardcaller)
    CardView cardcaller;

    @BindView(R.id.txtpices)
    TextView txtpices;
    String drivermobileno;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_order);
        ButterKnife.bind(this);
        getTrackOrderApi();
    }

    @OnClick({R.id.back,R.id.cardcaller})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.cardcaller:
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", drivermobileno, null));
                startActivity(intent);
                break;
        }
    }


    private void getTrackOrderApi()
    {
        final ProgressD progressDialog = ProgressD.show(TrackOrderActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<TrackOrderDetailsResponse> call = service.trackorder(LoginPreferences.getActiveInstance(TrackOrderActivity.this).getToken(), getIntent().getStringExtra("booking_id"));
        call.enqueue(new Callback<TrackOrderDetailsResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<TrackOrderDetailsResponse> call, Response<TrackOrderDetailsResponse> response) {
                progressDialog.dismiss();
                try {
                    TrackOrderDetailsResponse resultFile = response.body();
                    if (resultFile.getCode() == 200)
                    {
                        orderNo.setText(getIntent().getStringExtra("order_no"));
                        orderDateTime.setText(getIntent().getStringExtra("booking_date"));
                        txtprice.setText(getIntent().getStringExtra("total_amount"));
                        txtpices.setText(getIntent().getStringExtra("item_count"));


                        if((TextUtils.isEmpty(resultFile.getTrackOrderResultModel().getConfirmedAt())))
                        {
                           relativeConfiremedInvisible.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            relativeConfiremedInvisible.setVisibility(View.GONE);
                            relativeConfiremedVisible.setVisibility(View.VISIBLE);
                            time_confirmed.setText(resultFile.getTrackOrderResultModel().getConfirmedAt());
                        }

                        if((TextUtils.isEmpty(resultFile.getTrackOrderResultModel().getPickedUpAt())))
                        {
                            confirmedlineinvisible.setVisibility(View.VISIBLE);
                            relativeInvisiblePickedup.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            confirmedlinevisible.setVisibility(View.VISIBLE);
                            relativePickupVisible.setVisibility(View.VISIBLE);
                            pickedVisibleUpTime.setText(resultFile.getTrackOrderResultModel().getPickedUpAt());
                        }


                        if((TextUtils.isEmpty(resultFile.getTrackOrderResultModel().getInProcess())))
                        {
                            pickedInvisible.setVisibility(View.VISIBLE);
                            relativeInvisibleInprocess.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            pickedLineVisible.setVisibility(View.VISIBLE);
                            relativeInprocessVisible.setVisibility(View.VISIBLE);
                            pickedInprocessTime.setText(resultFile.getTrackOrderResultModel().getInProcess());

                        }


                        if((TextUtils.isEmpty(resultFile.getTrackOrderResultModel().getShippedAt())))
                        {
                            inprocessLineInvisible.setVisibility(View.VISIBLE);
                            relativeShipedInvisible.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            inprocessLineVisible.setVisibility(View.VISIBLE);
                            relativeShipedVisible.setVisibility(View.VISIBLE);
                            pickedShipedTime.setText(resultFile.getTrackOrderResultModel().getShippedAt());
                        }

                        if((TextUtils.isEmpty(resultFile.getTrackOrderResultModel().getDeliveredAt())))
                        {
                            deliverd_line_invisible.setVisibility(View.VISIBLE);
                            relativeDeliveredInvisible.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            deliverdLineVisible.setVisibility(View.VISIBLE);
                            relativeDeliveredVisible.setVisibility(View.VISIBLE);
                            deliverdInprocessTime.setText(resultFile.getTrackOrderResultModel().getDeliveredAt());
                        }

                        if((TextUtils.isEmpty(resultFile.getTrackOrderResultModel().getDriver_mobile_number())))
                        {
                            cardcaller.setVisibility(View.GONE);
                        }
                        else
                        {
                            cardcaller.setVisibility(View.VISIBLE);
                            drivermobileno=resultFile.getTrackOrderResultModel().getDriver_mobile_number();
                        }
                    }
                    else if (resultFile.getCode() == 404)
                    {

                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<TrackOrderDetailsResponse> call, Throwable t) {
                Toast.makeText(TrackOrderActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


}