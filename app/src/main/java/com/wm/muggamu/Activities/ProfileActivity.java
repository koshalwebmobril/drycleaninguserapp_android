package com.wm.muggamu.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.developers.imagezipper.ImageZipper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wm.muggamu.Models.getprofilemodel.GetProfileModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.getprofilemodel.GetProfileResponse;
import com.wm.muggamu.Models.updateprofilemodel.UpdateProfileResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.GPSTracker;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener
{
    private static final int REQUEST_IMAGE =  999;
    TextView tvtitle;
    EditText user_name,email_address,mobile_number,address;
    ImageView ivback;
    ImageView profile_image;
    File imageZipperFile;
    Dialog dialog;
    Button btn_changes;
    String profileimage;
    String user_lat,user_lng;
    GPSTracker gpsTracker;
    GetProfileModel getProfileModel;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
        if(CommonMethod.isOnline(this))
        {
            getProfileApi();
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet),this);
        }
        ivback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void init()
    {
        tvtitle=findViewById(R.id.tvtitle);
        tvtitle.setText("Profile");
        user_name=findViewById(R.id.user_name);
        email_address=findViewById(R.id.email_address);
        mobile_number=findViewById(R.id.mobile_number);
        ivback=findViewById(R.id.ivback);
        profile_image=findViewById(R.id.profile_image);
        btn_changes=findViewById(R.id.btn_changes);
        btn_changes.setOnClickListener(this);
        profile_image.setOnClickListener(this);
        mobile_number.setEnabled(false);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.profile_image:
                Dexter.withContext(this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    openDialogToUpdateProfilePIC();
                                }
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                                permissionToken.continuePermissionRequest();
                            }
                        }).check();
                break;


            case R.id.btn_changes:
                checkPermission();
                break;
        }
    }
    public void getProfileApi()
    {
        final ProgressD progressDialog = ProgressD.show(ProfileActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetProfileResponse> call = service.GetProfile(LoginPreferences.getActiveInstance(ProfileActivity.this).getToken());
        call.enqueue(new Callback<GetProfileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetProfileResponse> call, retrofit2.Response<GetProfileResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetProfileResponse resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                         getProfileModel = resultFile.getGetProfileModel();
                        mobile_number.setText(getProfileModel.getMobile());
                        profileimage=getProfileModel.getProfileImagePath();
                        Glide.with(ProfileActivity.this).load(getProfileModel.getProfileImagePath()).error(R.drawable.profile).placeholder(R.drawable.profile)
                                .into(profile_image);
                        if(getProfileModel.getName()==null)
                        {
                            user_name.setHint("Please enter name");
                        }
                        else
                        {
                            user_name.setText(getProfileModel.getName());
                        }

                        if(getProfileModel.getEmail()==null)
                        {
                            email_address.setHint("Please enter email");
                            email_address.setEnabled(true);
                        }
                        else
                        {
                            email_address.setText(getProfileModel.getEmail());
                            email_address.setEnabled(false);
                        }
                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(ProfileActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetProfileResponse> call, Throwable t)
            {
                Toast.makeText(ProfileActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    public void hitUpdateProfileApi(String user_name,String email,File imageZipperFile,String user_lat,String user_lng)
    {
        final ProgressD progressDialog = ProgressD.show(ProfileActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        MultipartBody.Part documentBody = null;
        if (imageZipperFile != null)
        {
            RequestBody document1 = RequestBody.create(MediaType.parse("multipart/form-data"), imageZipperFile);
            documentBody = MultipartBody.Part.createFormData("profile_image", imageZipperFile.getName(), document1);
        }
        RequestBody username= RequestBody.create(MediaType.parse("multipart/form-data"), user_name);
        RequestBody email_id = RequestBody.create(MediaType.parse("multipart/form-data"), email);
        RequestBody userlat = RequestBody.create(MediaType.parse("multipart/form-data"), user_lat);
        RequestBody userlng = RequestBody.create(MediaType.parse("multipart/form-data"), user_lng);
        Call<UpdateProfileResponse> call = service.updateprofile(LoginPreferences.getActiveInstance(ProfileActivity.this).getToken()
                ,username,email_id,documentBody,userlat,userlng);
        call.enqueue(new Callback<UpdateProfileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, retrofit2.Response<UpdateProfileResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    UpdateProfileResponse resultFile = response.body();
                    Toast.makeText(ProfileActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();

                    if(resultFile.getCode() == 200)
                    {
                        LoginPreferences.getActiveInstance(ProfileActivity.this).setUserName(resultFile.getUpdateProfileModel().getName());
                        LoginPreferences.getActiveInstance(ProfileActivity.this).setUserEmail(resultFile.getUpdateProfileModel().getEmail());
                        LoginPreferences.getActiveInstance(ProfileActivity.this).setUserProfile(resultFile.getUpdateProfileModel().getProfileImagePath());

                        Intent i=new Intent(ProfileActivity.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(ProfileActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t)
            {
                Toast.makeText(ProfileActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void hitUpdateProfileNULLApi(String user_name,File imageZipperFile,String user_lat,String user_lng)
    {
        final ProgressD progressDialog = ProgressD.show(ProfileActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        MultipartBody.Part documentBody = null;
        if (imageZipperFile != null)
        {
            RequestBody document1 = RequestBody.create(MediaType.parse("multipart/form-data"), imageZipperFile);
            documentBody = MultipartBody.Part.createFormData("profile_image", imageZipperFile.getName(), document1);
        }
        RequestBody username= RequestBody.create(MediaType.parse("multipart/form-data"), user_name);
        RequestBody userlat = RequestBody.create(MediaType.parse("multipart/form-data"), user_lat);
        RequestBody userlng = RequestBody.create(MediaType.parse("multipart/form-data"), user_lng);
        Call<UpdateProfileResponse> call = service.updateNullprofile(LoginPreferences.getActiveInstance(ProfileActivity.this).getToken()
                ,username,documentBody,userlat,userlng);
        call.enqueue(new Callback<UpdateProfileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, retrofit2.Response<UpdateProfileResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    UpdateProfileResponse resultFile = response.body();
                    Toast.makeText(ProfileActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();

                    if(resultFile.getCode() == 200)
                    {
                        LoginPreferences.getActiveInstance(ProfileActivity.this).setUserName(resultFile.getUpdateProfileModel().getName());
                        LoginPreferences.getActiveInstance(ProfileActivity.this).setUserEmail(resultFile.getUpdateProfileModel().getEmail());
                        LoginPreferences.getActiveInstance(ProfileActivity.this).setUserProfile(resultFile.getUpdateProfileModel().getProfileImagePath());
                        Intent i=new Intent(ProfileActivity.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(ProfileActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t)
            {
                Toast.makeText(ProfileActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }



    private void launchCameraIntent() {
        Intent intent = new Intent(ProfileActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    public void openDialogToUpdateProfilePIC()
    {
        dialog = new Dialog(ProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_curved_bg_inset);

        dialog.setContentView(R.layout.dialog_select);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        LinearLayout cameraLayout = dialog.findViewById(R.id.cameraLayout);
        LinearLayout galleryLayout = dialog.findViewById(R.id.galleryLayout);
        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCameraIntent();
                dialog.dismiss();
            }
        });
        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchGalleryIntent();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showSettingsDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });

        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                File file = new File(uri.getPath());
                loadProfile(uri.toString());
                try {
                    imageZipperFile = new ImageZipper(ProfileActivity.this)
                            .setQuality(50)
                            .setMaxWidth(300)
                            .setMaxHeight(300)
                            .compressToFile(file);

                } catch (IOException e) {
                    e.printStackTrace();
                } }
        }
    }

    private void loadProfile(String url) {
        Log.d("TAG", "Image cache path: " + url);

        Glide.with(this).load(url).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(profile_image);
        profile_image.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent));
    }

    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if (granted)
        {
            checkGPS();
        }
        else
        {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(this, "You Need Accept This Location Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }
    private void checkGPS()
    {
        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(ProfileActivity.this, "Please Turn GPS on to continue..", Toast.LENGTH_LONG).show();
        }
        else
        {
            gpsTracker=new GPSTracker(this);
            user_lat=String.valueOf(gpsTracker.getLatitude());
            user_lng=String.valueOf(gpsTracker.getLongitude());

            if(getProfileModel.getEmail()==null)
            {
                hitUpdateProfileApi(user_name.getText().toString(),email_address.getText().toString(),imageZipperFile,user_lat,user_lng);
            }
            else
            {
                hitUpdateProfileNULLApi(user_name.getText().toString(),imageZipperFile,user_lat,user_lng);
            }
        }
    }
}
