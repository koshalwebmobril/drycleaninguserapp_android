package com.wm.muggamu.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookingConfirmActivity extends AppCompatActivity
{
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.order_details)
    TextView orderDetails;

    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;

    @BindView(R.id.text_shop_name)
    TextView textShopName;

    @BindView(R.id.txt_orderno)
    TextView txtOrderno;
    @BindView(R.id.txt_total_amount)
    TextView txtTotalAmount;
    @BindView(R.id.txt_pickupaddresstime)
    TextView txtPickupaddresstime;
    @BindView(R.id.txt_deliveryaddresstime)
    TextView txtDeliveryaddresstime;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.pickup_location)
    TextView pickup_location;
    @BindView(R.id.img_bookingconfirm)
    ImageView img_bookingconfirm;

    @BindView(R.id.txtmessage)
    TextView txtmessage;

    @BindView(R.id.linear_dry_cleaning_item)
    LinearLayout linear_dry_cleaning_item;

    @BindView(R.id.relative_booking_date_time)
    RelativeLayout relative_booking_date_time;

    @BindView(R.id.relative_pickup_address)
    RelativeLayout relative_pickup_address;

    @BindView(R.id. txt_booking_date_time)
    TextView  txt_booking_date_time;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_confirm);
        ButterKnife.bind(this);

        if(LoginPreferences.getActiveInstance(this).getCategoryId().equals(getString(R.string.categoryid)))
        {
            Glide.with(this).load(getIntent().getStringExtra("image")).apply(new RequestOptions()
                    .placeholder(R.drawable.image).error(R.drawable.image)).into(img_bookingconfirm);
            textShopName.setText(getIntent().getStringExtra("shop_name"));
            txtOrderno.setText(getIntent().getStringExtra("order_no"));
            txtTotalAmount.setText(getString(R.string.currency_icon)+" "+getIntent().getStringExtra("total_amount"));
            txtPickupaddresstime.setText(getIntent().getStringExtra("pickup_date_time"));
            txtDeliveryaddresstime.setText(getIntent().getStringExtra("delivery_date_time"));
            pickup_location.setText(getIntent().getStringExtra("pickup_address"));

            linear_dry_cleaning_item.setVisibility(View.VISIBLE);
            relative_pickup_address.setVisibility(View.VISIBLE);
            relative_booking_date_time.setVisibility(View.GONE);
        }
        else
        {
            linear_dry_cleaning_item.setVisibility(View.GONE);
            relative_pickup_address.setVisibility(View.GONE);
            relative_booking_date_time.setVisibility(View.VISIBLE);

            Glide.with(this).load(getIntent().getStringExtra("image")).apply(new RequestOptions()
                    .placeholder(R.drawable.image).error(R.drawable.image)).into(img_bookingconfirm);
            textShopName.setText(getIntent().getStringExtra("shop_name"));
            txtOrderno.setText(getIntent().getStringExtra("order_no"));
            txt_booking_date_time.setText(getIntent().getStringExtra("pickup_date_time"));
        }
    }
    @OnClick(R.id.back)
    public void onClick()
    {
       Intent i=new Intent(this,MainActivity.class);
       startActivity(i);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent i=new Intent(this,MainActivity.class);
        startActivity(i);
    }
}
