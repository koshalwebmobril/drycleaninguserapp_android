package com.wm.muggamu.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.developers.imagezipper.ImageZipper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wm.muggamu.Adapter.AddPostSaveItemsParentAdapter;
import com.wm.muggamu.Models.serviceonpostmodel.ServicesListsModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.createpostmodel.CreatePostResponse;
import com.wm.muggamu.Models.serviceonpostmodel.ServiceOnPostResponse;
import com.wm.muggamu.RoomAddPost.AddPostDatabase;
import com.wm.muggamu.RoomAddPost.AddPostEntity;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.Utils.UrlApi;
import com.wm.muggamu.network.ApiInterface;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.view.View.GONE;

public class AddPostActivity extends AppCompatActivity
{
    ArrayList<String> servicesnamelist = new ArrayList<>();
    ArrayList<String> servicesidlist = new ArrayList<>();
    File imagezi;
    private FragmentTransaction ft;
    private Fragment currentFragment;

    @BindView(R.id.service_address)
    TextView serviceAddress;

    @BindView(R.id.service_amount)
    EditText serviceAmount;
    @BindView(R.id.service_remark)
    EditText serviceRemark;
    @BindView(R.id.txt_servicedate)
    TextView txtServicedate;

    @BindView(R.id.service_time)
    TextView serviceTime;


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.add_post_title)
    TextView addPostTitle;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;


    @BindView(R.id.service_date)
    ImageView serviceDate;
    @BindView(R.id.open_time)
    ImageView openTime;

    @BindView(R.id.card_service_date)
    CardView cardserviceDate;

    @BindView(R.id.card_service_time)
    CardView cardserviceTime;
    @BindView(R.id.pin_location)
    ImageView pinLocation;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.img_service)
    ImageView imgService;

    @BindView(R.id.relative_address)
    RelativeLayout relativeaddress;

    @BindView(R.id.cart_addpost_item)
    CardView cart_addpost_item;

    @BindView(R.id.recyclerbid)
    RecyclerView recyclerbid;

    @BindView(R.id.img_when_update)
    ImageView imgWhenUpdate;
    @BindView(R.id.relative_add_image)
    RelativeLayout relativeAddImage;
    private int mHour, mMinute;
    private int mYear, mMonth, mDay;
    private List<ServicesListsModel> servicelist;
    Dialog dialog;
    private static final int REQUEST_IMAGE = 999;
    File imageZipperFile;
    ProgressD progressDialog;
    String service_id,service_name;
    AutoCompleteTextView autoCompleteState;
    private ArrayAdapter<String> timeSlotAdapter;
    AddPostSaveItemsParentAdapter cartParentAdapter;
    List<AddPostEntity> allcart_items;
    String childitemid ="";
    String childquantity ="";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        ButterKnife.bind(this);
        getServiceTitle();
        AddPostDatabase.getInstance(AddPostActivity.this).addPostDao().emptyList();
        autoCompleteState=findViewById(R.id.autoCompleteState);
        SharedPreferences preferences = getSharedPreferences("AddressPerference", 0);
        preferences.edit().remove("address").commit();
        autoCompleteState.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                initilizeStateSpinner();
                //  autoCompleteState.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(ParticularServiceProvider.this,R.drawable.ic_calendar), null);
            }
        });
        relativeaddress.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(AddPostActivity.this,GetAddressActivity.class);
                LoginPreferences.getActiveInstance(AddPostActivity.this).setPageId("2");
                startActivityForResult(i,101);
            }
        });

        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH);
        String currentdate = df.format(new Date());
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE");
        String  survey_day_String = formatter.format(cal.getTime());

        Date date = cal.getTime();
        int mHour1 = date.getHours();
        int mMinute1 = date.getMinutes();
        if(mMinute1<10)
        {
            serviceTime.setText(mHour1 + ":" +"0"+ mMinute1);
        }
        else
        {
            serviceTime.setText(mHour1 + ":" + mMinute1);
        }
        txtServicedate.setText(currentdate);
    }

    private void initilizeStateSpinner()
    {
        timeSlotAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, servicesnamelist);
        autoCompleteState.setAdapter(timeSlotAdapter);
        autoCompleteState.requestFocus();
        autoCompleteState.showDropDown();
        autoCompleteState.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                autoCompleteState.setText(servicesnamelist.get(position));
                service_name=servicesnamelist.get(position);
                service_id=servicesidlist.get(position);

                if(LoginPreferences.getActiveInstance(AddPostActivity.this).getServiceId().equals(service_id))
                {
                    Intent intent = new Intent(AddPostActivity.this,ChooseBidItemsActivity.class);
                    intent.putExtra("service_name",service_name);
                    intent.putExtra("service_id",service_id);
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(AddPostActivity.this,ChooseBidItemsActivity.class);
                    intent.putExtra("service_name",service_name);
                    intent.putExtra("service_id",service_id);
                    startActivity(intent);
                    AddPostDatabase.getInstance(AddPostActivity.this).addPostDao().emptyList();
                }
                Log.d("selected",service_name+service_id);
            }
        });
    }

    public void getServiceTitle()
    {
        final ProgressD progressDialog = ProgressD.show(AddPostActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ServiceOnPostResponse> call = service.servicePost(LoginPreferences.getActiveInstance(AddPostActivity.this).getToken());
        call.enqueue(new Callback<ServiceOnPostResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ServiceOnPostResponse> call, Response<ServiceOnPostResponse> response) {
                progressDialog.dismiss();
                try {
                    ServiceOnPostResponse resultFile = response.body();
                    if (resultFile.getCode() == 200)
                    {
                        servicelist = resultFile.getServiceList();
                        for(int i = 0; i < servicelist.size(); i++)
                        {
                            servicesnamelist.add(servicelist.get(i).getName());
                            servicesidlist.add(String.valueOf(servicelist.get(i).getId()));
                        }
                    }
                    else if (resultFile.getCode() == 401)
                    {
                        Toast.makeText(AddPostActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ServiceOnPostResponse> call, Throwable t) {
                Toast.makeText(AddPostActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @OnClick({R.id.back, R.id.btn_submit, R.id.relative_add_image, R.id.card_service_date, R.id.card_service_time})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.btn_submit:
                if (CommonMethod.isOnline(this))
                {
                    childitemid="";
                    childquantity="";

                    hideKeyboard((Button) view);
                    if(validation())
                    {
                        LoginPreferences.getActiveInstance(this).setServiceId("");
                        allcart_items= AddPostDatabase.getInstance(AddPostActivity.this).addPostDao().getAllData();
                        if(allcart_items.size()==0)
                        {
                            Toast.makeText(this, "Please select item for selected services.", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            for(int i=0;i<allcart_items.size();i++)
                            {
                                if(allcart_items.size()==0)
                                {
                                    childitemid = String.valueOf(allcart_items.get(0).getChild_id());
                                    childquantity = String.valueOf(allcart_items.get(0).getChild_quantity());
                                }
                                else
                                {
                                    if(childitemid.equals(""))
                                    {
                                        childitemid = String.valueOf(allcart_items.get(i).getChild_id());
                                        childquantity = String.valueOf(allcart_items.get(i).getChild_quantity());
                                    }
                                    else
                                    {
                                        childitemid = childitemid +","+ String.valueOf(allcart_items.get(i).getChild_id());
                                        childquantity = childquantity +","+ String.valueOf(allcart_items.get(i).getChild_quantity());
                                    }
                                }
                            }
                            hitAddPostApi(childquantity,childitemid,service_id,txtServicedate.getText().toString(),serviceTime.getText().toString(),serviceAddress.getText().toString(),serviceAmount.getText().toString(),serviceRemark.getText().toString(),imageZipperFile);
                        }
                    }
                }
                else
                {
                    childquantity="";
                    childitemid="";
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;

            case R.id.card_service_date:
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddPostActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                txtServicedate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
                break;


            case R.id.card_service_time:
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                if (minute < 10) {
                                    serviceTime.setText(hourOfDay + ":" + "0" + minute);
                                } else {
                                    serviceTime.setText(hourOfDay + ":" + minute);
                                }
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
                break;

            case R.id.relative_add_image:
                Dexter.withContext(this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    openDialogToUpdateProfilePIC();
                                }
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                                permissionToken.continuePermissionRequest();
                            }
                        }).check();
                break;
        }
    }



    public void openDialogToUpdateProfilePIC() {
        dialog = new Dialog(AddPostActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_curved_bg_inset);

        dialog.setContentView(R.layout.dialog_select);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        LinearLayout cameraLayout = dialog.findViewById(R.id.cameraLayout);
        LinearLayout galleryLayout = dialog.findViewById(R.id.galleryLayout);
        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCameraIntent();
                dialog.dismiss();
            }
        });
        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchGalleryIntent();
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });

        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }


    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                File file = new File(uri.getPath());
                loadProfile(uri.toString());
                try {
                    imageZipperFile = new ImageZipper(AddPostActivity.this)
                            .setQuality(50)
                            .setMaxWidth(300)
                            .setMaxHeight(300)
                            .compressToFile(file);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (requestCode == 101)
        {
            if(resultCode == Activity.RESULT_OK){
                String selectedLocation = data.getStringExtra("ADDRESS_RESULT");
                serviceAddress.setText(selectedLocation);
            }
            if(resultCode == Activity.RESULT_CANCELED)
            {
                //Write your code if there's no result
                serviceAddress.setText("");
            }
        }
    }

    private void loadProfile(String url) {
        imgService.setVisibility(View.GONE);
        imgWhenUpdate.setVisibility(View.VISIBLE);

        Glide.with(this).load(url).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(imgWhenUpdate);
        imgWhenUpdate.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent));
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(AddPostActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void hitAddPostApi(String child_quantity,String childitem_id,String service_id, String strServicedate,String strserviceTime,String strserviceAddress,String strserviceRemark,String serviceamount, File imageZipperFile) {
        progressDialog = ProgressD.show(this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        MultipartBody.Part documentBody = null;
        if(imageZipperFile != null)
        {
            RequestBody document1 = RequestBody.create(MediaType.parse("multipart/form-data"), imageZipperFile);
            documentBody = MultipartBody.Part.createFormData("post_attachment", imageZipperFile.getName(), document1);
        }

        RequestBody serviceid = RequestBody.create(MediaType.parse("multipart/form-data"), service_id);
        RequestBody childitemid = RequestBody.create(MediaType.parse("multipart/form-data"),childitem_id);
        RequestBody childquantity = RequestBody.create(MediaType.parse("multipart/form-data"), child_quantity);

        RequestBody servicedate = RequestBody.create(MediaType.parse("multipart/form-data"), strServicedate);
        RequestBody servicetime = RequestBody.create(MediaType.parse("multipart/form-data"), strserviceTime);
        RequestBody serviceaddress = RequestBody.create(MediaType.parse("multipart/form-data"), strserviceAddress);
        RequestBody amount = RequestBody.create(MediaType.parse("multipart/form-data"), serviceamount);
        RequestBody remarks = RequestBody.create(MediaType.parse("multipart/form-data"), strserviceRemark);

        Call<CreatePostResponse> call = service.createPost(
                LoginPreferences.getActiveInstance(this).getToken(),childitemid,childquantity, serviceid, servicedate, servicetime,serviceaddress,remarks,amount,documentBody);
        call.enqueue(new Callback<CreatePostResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<CreatePostResponse> call, Response<CreatePostResponse> response) {
                progressDialog.dismiss();
                try
                {
                    CreatePostResponse resultFile = response.body();
                    Toast.makeText(AddPostActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                       Intent i=new Intent(AddPostActivity.this,MainActivity.class);
                       i.putExtra("pagestatus","1");
                       startActivity(i);
                        SharedPreferences preferences = getSharedPreferences("AddressPerference", 0);
                        preferences.edit().remove("address").commit();
                    }
                    else if(resultFile.getCode() == 422)
                    {

                    }
                    else
                        {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<CreatePostResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }


    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private boolean validation()
    {
        if(TextUtils.isEmpty(txtServicedate.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Select Service Date", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (TextUtils.isEmpty(serviceTime.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Select Service Time", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(TextUtils.isEmpty(serviceAddress.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Select Service Location", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(serviceAmount.getText().toString().trim().equals("0"))
        {
            Toast.makeText(this, "Invalid Amount", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(serviceAmount.getText().toString().trim().equals("00"))
        {
            Toast.makeText(this, "Invalid Amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(serviceAmount.getText().toString().trim().equals("000"))
        {
            Toast.makeText(this, "Invalid Amount", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(serviceAmount.getText().toString().trim().equals("0000"))
        {
            Toast.makeText(this, "Invalid Amount", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(serviceAmount.getText().toString().trim().equals("00000"))
        {
            Toast.makeText(this, "Invalid Amount", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(serviceAmount.getText().toString().trim().equals("000000"))
        {
            Toast.makeText(this, "Invalid Amount", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences("AddressPerference", MODE_PRIVATE);
        serviceAddress.setText(prefs.getString("address", ""));

        allcart_items= AddPostDatabase.getInstance(AddPostActivity.this).addPostDao().getAllData();
        if(allcart_items.size()==0)
       {
           cart_addpost_item.setVisibility(GONE);
       }
       else
       {
           cart_addpost_item.setVisibility(View.VISIBLE);
           LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
           recyclerbid.setLayoutManager(mLayoutManager);
           cartParentAdapter = new AddPostSaveItemsParentAdapter(this,allcart_items);
           recyclerbid.setAdapter(cartParentAdapter);
       }
    }
}