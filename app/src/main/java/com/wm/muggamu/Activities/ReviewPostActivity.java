package com.wm.muggamu.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.developers.imagezipper.ImageZipper;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wm.muggamu.ApiClient.RetrofitConnection;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.reviewsubmitmodel.ReviewSubmitResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.CommonMethod;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.network.ApiInterface;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewPostActivity extends AppCompatActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.review)
    TextView review;
    @BindView(R.id.relative_toolbar)
    RelativeLayout relativeToolbar;
    @BindView(R.id.service_date)
    ImageView serviceDate;
    @BindView(R.id.open_time)
    ImageView openTime;
    @BindView(R.id.txt_servicedate)
    TextView txtServicedate;
    @BindView(R.id.card_service_date)
    CardView cardServiceDate;
    @BindView(R.id.service_time)
    TextView serviceTime;
    @BindView(R.id.card_service_time)
    CardView cardServiceTime;
    @BindView(R.id.img_service)
    ImageView imgService;
    @BindView(R.id.img_when_update)
    ImageView imgWhenUpdate;
    @BindView(R.id.relative_add_image)
    RelativeLayout relativeAddImage;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.edit_review)
    EditText editReview;
    String rating;
    File imageZipperFile;
    Dialog dialog;
    private static final int REQUEST_IMAGE = 999;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_post);
        ButterKnife.bind(this);
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(date);
        int mHour1 = date.getHours();
        int mMinute1 = date.getMinutes();
        if(mMinute1<10)
        {
            serviceTime.setText(mHour1 + ":" +"0"+ mMinute1);
        }
        else
        {
            serviceTime.setText(mHour1 + ":" + mMinute1);
        }
        txtServicedate.setText(formattedDate);
    }

    @OnClick({R.id.back, R.id.btn_submit,R.id.relative_add_image})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;
            case R.id.btn_submit:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((Button) view);
                    if (validation())
                    {
                        hitpostreview();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;


            case R.id.relative_add_image:
                Dexter.withContext(this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    openDialogToUpdateProfilePIC();
                                }
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                                permissionToken.continuePermissionRequest();
                            }
                        }).check();
        }
    }


    public void openDialogToUpdateProfilePIC()
    {
        dialog = new Dialog(ReviewPostActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_curved_bg_inset);

        dialog.setContentView(R.layout.dialog_select);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        LinearLayout cameraLayout = dialog.findViewById(R.id.cameraLayout);
        LinearLayout galleryLayout = dialog.findViewById(R.id.galleryLayout);
        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCameraIntent();
                dialog.dismiss();
            }
        });
        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchGalleryIntent();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });

        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                File file = new File(uri.getPath());
                loadProfile(uri.toString());
                try {
                    imageZipperFile = new ImageZipper(ReviewPostActivity.this)
                            .setQuality(50)
                            .setMaxWidth(300)
                            .setMaxHeight(300)
                            .compressToFile(file);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void loadProfile(String url) {
        imgService.setVisibility(View.GONE);
        imgWhenUpdate.setVisibility(View.VISIBLE);

        Glide.with(this).load(url).error(R.drawable.profile).placeholder(R.drawable.profile)
                .into(imgWhenUpdate);
        imgWhenUpdate.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent));
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(ReviewPostActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        startActivityForResult(intent, REQUEST_IMAGE);
    }



    private void hitpostreview()
    {
        ProgressD  progressDialog = ProgressD.show(this, getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        MultipartBody.Part documentBody = null;
        if (imageZipperFile != null)
        {
            RequestBody document1 = RequestBody.create(MediaType.parse("multipart/form-data"), imageZipperFile);
            documentBody = MultipartBody.Part.createFormData("image", imageZipperFile.getName(), document1);
        }
        RequestBody provider_id = RequestBody.create(MediaType.parse("multipart/form-data"), getIntent().getStringExtra("provider_id"));
        RequestBody booking_id = RequestBody.create(MediaType.parse("multipart/form-data"), getIntent().getStringExtra("booking_id"));
        RequestBody appointmentId = RequestBody.create(MediaType.parse("multipart/form-data"), getIntent().getStringExtra("appointment_id"));
        RequestBody rating = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(ratingBar.getRating()));
        RequestBody review = RequestBody.create(MediaType.parse("multipart/form-data"), editReview.getText().toString());

        Call<ReviewSubmitResponse> call = service.reviewsubmit(LoginPreferences.getActiveInstance(this).getToken(),
                provider_id,booking_id,rating,review,documentBody,appointmentId);
        call.enqueue(new Callback<ReviewSubmitResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ReviewSubmitResponse> call, Response<ReviewSubmitResponse> response) {
                progressDialog.dismiss();
                try
                {
                    ReviewSubmitResponse resultFile = response.body();
                    Toast.makeText(ReviewPostActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        finish();
                    }
                    else if(resultFile.getCode() == 422)
                    {

                    }
                    else {
                        // Toast.makeText(LoginActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ReviewSubmitResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private boolean validation()
    {
        /*if(ratingBar.getRating()==0)
        {
            Toast.makeText(this, "Please Give Rating", Toast.LENGTH_SHORT).show();
            return false;
        }*/
        if(TextUtils.isEmpty(editReview.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Review", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

}