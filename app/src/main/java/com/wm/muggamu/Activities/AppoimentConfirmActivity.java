package com.wm.muggamu.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamu.ApiClient.RetrofitConnection;
import com.wm.muggamu.Models.createappoimentmodel.AppoimentResponse;
import com.wm.muggamu.R;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;

public class AppoimentConfirmActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView service_image,back;
    String selecteddate,selectedtime,address;
    TextView txt_selecteddate,txt_selectedtime,search_address,service_name;
    Button btn_confirm;
    String servicename;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appoiment_confirm);
        init();
        address=LoginPreferences.getActiveInstance(this).getService_address();
        selecteddate=getIntent().getStringExtra("selected_date");
        selectedtime=getIntent().getStringExtra("selected_time");
        servicename=LoginPreferences.getActiveInstance(this).getService_name();
        Glide.with(this).load(LoginPreferences.getActiveInstance(this).getService_image()).apply(new RequestOptions().placeholder(R.drawable.profile).error(R.drawable.profile)).into(service_image);
        txt_selecteddate.setText(selecteddate);
        txt_selectedtime.setText(selectedtime);
        search_address.setText(address);
        service_name.setText(servicename);
    }
    public void init()
    {
        service_image=findViewById(R.id.service_image);
        txt_selecteddate=findViewById(R.id.txt_selecteddate);
        txt_selectedtime=findViewById(R.id.txt_selectedtime);
        search_address=findViewById(R.id.search_address);
        btn_confirm=findViewById(R.id.btn_confirm);
        back=findViewById(R.id.back);
        service_name=findViewById(R.id.service_name);
        btn_confirm.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_confirm:
                hitsubmitTimeSlotsApi(selecteddate,selectedtime);
                break;

            case R.id.back:
                finish();
                break;
        }
    }
    private void hitsubmitTimeSlotsApi(String selectedDate,String selectedTime)
    {
        final ProgressD progressDialog = ProgressD.show(this, getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<AppoimentResponse> call = service.createAppoiment(LoginPreferences.getActiveInstance(this).getToken(),
                LoginPreferences.getActiveInstance(this).getProviderId(), selectedDate,selectedTime);
        call.enqueue(new Callback<AppoimentResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<AppoimentResponse> call, retrofit2.Response<AppoimentResponse> response) {
                progressDialog.dismiss();
                try {
                    AppoimentResponse resultFile = response.body();
                    Toast.makeText(AppoimentConfirmActivity.this,resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if(resultFile.getCode() == 200)
                    {
                        Intent intent = new Intent(AppoimentConfirmActivity.this, BookingConfirmActivity.class);
                        intent.putExtra("image",LoginPreferences.getActiveInstance(AppoimentConfirmActivity.this).getService_image());
                        intent.putExtra("shop_name",resultFile.getAppointment().getProviderName());
                        intent.putExtra("order_no",resultFile.getAppointment().getAppointmentId());
                        intent.putExtra("pickup_date_time",resultFile.getAppointment().getBookingDate()
                       +" " +"at"+" "+resultFile.getAppointment().getBookingTime());
                        startActivity(intent);
                    }
                    else if (resultFile.getCode() == 404)
                    { }

                    else
                    { }
                } catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<AppoimentResponse> call, Throwable t) {
                Toast.makeText(AppoimentConfirmActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}