package com.wm.muggamu.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.wm.muggamu.R;
import com.wm.muggamu.Utils.CommonMethod;

public class ChangePasswordActivity extends AppCompatActivity {
    ImageView back;
    Button btn_changes;
    EditText old_password,new_password,re_setpassword;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        init();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (CommonMethod.isOnline(ChangePasswordActivity.this))
                {
                    hideKeyboard((Button)v);
                    if(validation())
                    {
                        //hitchangepasswordApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), ChangePasswordActivity.this);
                }
            }
        });
    }

    public void init()
    {
        back=findViewById(R.id.back);
        btn_changes=findViewById(R.id.btn_changes);
        old_password=findViewById(R.id.old_password);
        new_password=findViewById(R.id.new_password);
        re_setpassword=findViewById(R.id.re_setpassword);
    }

    /*public void hitchangepasswordApi()
    {
        final ProgressD progressDialog = ProgressD.show(ChangePasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ChangePasswordResponse> call = service.changepassword(LoginPreferences.getActiveInstance(ChangePasswordActivity.this).getToken(),old_password.getText().toString().trim(),new_password.getText().toString().trim());
        call.enqueue(new Callback<ChangePasswordResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, retrofit2.Response<ChangePasswordResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    ChangePasswordResponse resultFile = response.body();
                    Toast.makeText(ChangePasswordActivity.this,resultFile.getMessage(), Toast.LENGTH_SHORT).show();

                    if(resultFile.getCode() == 200)
                    {
                        Intent i=new Intent(ChangePasswordActivity.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(resultFile.getCode()== 401)
                    {
                        Toast.makeText(ChangePasswordActivity.this, "Unauthorized", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t)
            {
                Toast.makeText(ChangePasswordActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }*/
    private boolean validation()
    {
        if(TextUtils.isEmpty(old_password.getText().toString().trim()))
        {
            Toast.makeText(ChangePasswordActivity.this, "Please enter Old Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(new_password.getText().toString().trim()))
        {
            Toast.makeText(ChangePasswordActivity.this, "Please enter New password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(new_password.getText().toString().trim().length()<8 || new_password.getText().toString().trim().length()>16 )
        {
            Toast.makeText(ChangePasswordActivity.this, "New Password must be more than 7 upto 16 characters", Toast.LENGTH_LONG).show();
            return false;
        }

        else if(TextUtils.isEmpty(re_setpassword.getText().toString().trim()))
        {
            Toast.makeText(ChangePasswordActivity.this, "Please enter Re-Set Password", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if(re_setpassword.getText().toString().trim().length()<8 || re_setpassword.getText().toString().trim().length()>16 )
        {
            Toast.makeText(ChangePasswordActivity.this, "Re-Set Password must be more than 7 upto 16 characters", Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!re_setpassword.getText().toString().trim().equals(new_password.getText().toString().trim()))
        {
            Toast.makeText(ChangePasswordActivity.this, "Password and Confirm Password Not Match!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

}