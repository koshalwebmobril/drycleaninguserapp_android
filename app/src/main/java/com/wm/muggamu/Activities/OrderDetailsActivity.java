package com.wm.muggamu.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wm.muggamu.Adapter.OrderItemDetailsParentAdapter;
import com.wm.muggamu.ApiClient.RetrofitConnection;
import com.wm.muggamu.Chat.ChatActivity;
import com.wm.muggamu.Models.orderdetailsmodel.OrderDetailsItemsModel;
import com.wm.muggamu.Models.orderdetailsmodel.OrderDetailsModel;
import com.wm.muggamu.R;
import com.wm.muggamu.Models.orderdetailsmodel.OrderDetailsResponse;
import com.wm.muggamu.Sharedpreference.LoginPreferences;
import com.wm.muggamu.Utils.ProgressD;
import com.wm.muggamu.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class OrderDetailsActivity extends AppCompatActivity
{
    String booking_id;
    OrderItemDetailsParentAdapter orderItemDetailsParentAdapter;
    ArrayList<OrderDetailsItemsModel> orderdetailslist;
    ImageView back, provider_profile;
    TextView txtprovider_name,txt_order_no,txttotal_amount,deliver_date;
    RecyclerView recyclerview_yourbooking;
    TextView txt_delivery_amount,txtgrandtotal;

    LinearLayout linear_view_details;
    TextView review_status,txtpices;
    OrderDetailsResponse resultFile;
    RelativeLayout relative_track_order,relative_order_rejected;
    TextView txt_itemtotalamount;
    LinearLayout linear_total_deliveryfee;
    TextView booked_at,booking_status;
    Button btn_chat;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        init();

        getOrderDetailsApi();
        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        linear_view_details.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(OrderDetailsActivity.this,TrackOrderActivity.class);
                i.putExtra("booking_id",booking_id);
                i.putExtra("order_no",txt_order_no.getText().toString().trim());
                i.putExtra("booking_date",booked_at.getText().toString().trim());
                i.putExtra("total_amount",txtgrandtotal.getText().toString().trim());
                i.putExtra("item_count",txtpices.getText().toString().trim());
                startActivity(i);
            }
        });
        review_status.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(OrderDetailsActivity.this, ReviewPostActivity.class);
                i.putExtra("booking_id",String.valueOf(booking_id));
                i.putExtra("provider_id",String.valueOf(resultFile.getBookingDetails().getProviderId()));
                i.putExtra("appointment_id",String.valueOf(""));
                startActivity(i);
                finish();
            }
        });

        btn_chat.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(OrderDetailsActivity.this, ChatActivity.class);
                i.putExtra("provider_name",resultFile.getBookingDetails().getProviderName());
                i.putExtra("user_id",String.valueOf(resultFile.getBookingDetails().getUserId()));
                i.putExtra("provider_id",String.valueOf(resultFile.getBookingDetails().getProviderId()));
                i.putExtra("provider_img",resultFile.getBookingDetails().getProviderImage());
                i.putExtra("provider_device_token",resultFile.getBookingDetails().getProviderDeviceToken());
                startActivity(i);
            }
        });
    }

    private void init()
    {
        booking_id=getIntent().getStringExtra("booking_id");
        txtprovider_name=findViewById(R.id.provider_name);
        booking_status=findViewById(R.id.booking_status);

        recyclerview_yourbooking=findViewById(R.id.recycler_booking_details);
        back=findViewById(R.id.back);
        provider_profile =findViewById(R.id.provider_profile);
        txt_order_no=findViewById(R.id.order_no);
        txttotal_amount=findViewById(R.id.txttotal_amount);
        txt_delivery_amount=findViewById(R.id.txt_delivery_amount);
        linear_view_details=findViewById(R.id.linear_view_details);
        review_status=findViewById(R.id.review_status);
        btn_chat=findViewById(R.id.btn_chat);
        relative_track_order=findViewById(R.id.relative_track_order);
        relative_order_rejected=findViewById(R.id.relative_order_rejected);
        linear_total_deliveryfee=findViewById(R.id.linear_total_deliveryfee);
        txt_itemtotalamount=findViewById(R.id.txt_itemtotalamount);


        booked_at=findViewById(R.id.booked_at);
        txtgrandtotal=findViewById(R.id.txtgrandtotal);
        txtpices=findViewById(R.id.txtpices);

    }


    public void getOrderDetailsApi()
    {
        final ProgressD progressDialog = ProgressD.show(this, getResources().getString(R.string.logging_in), true, false, null);
        ApiInterface service = RetrofitConnection.getInstance().createService();
        Call<OrderDetailsResponse> call = service.orderDetailsResponse(LoginPreferences.getActiveInstance(this).getToken(),booking_id);
        call.enqueue(new Callback<OrderDetailsResponse>() {
            @SuppressLint({"SetTextI18n", "DefaultLocale"})
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                progressDialog.dismiss();
                try {
                    resultFile = response.body();
                    if(resultFile.getCode() == 200)
                    {
                        txtprovider_name.setText(getIntent().getStringExtra("provider_name"));
                        Glide.with(OrderDetailsActivity.this).load(resultFile.getBookingDetails().getProviderImage()).apply(new RequestOptions().placeholder(R.drawable.clothes_img).error(R.drawable.clothes_img)).into(provider_profile);
                        txt_order_no.setText(getString(R.string.order_no1) + resultFile.getBookingDetails().getOrderId());
                        booked_at.setText(getIntent().getStringExtra("booking_date"));
                        txttotal_amount.setText(getIntent().getStringExtra("total_amount"));
                        txtpices.setText(getIntent().getStringExtra("pices"));

                        double cartitemtotal=resultFile.getBookingDetails().getTotalAmount() - resultFile.getBookingDetails().getDeliveryFee();

                        txt_itemtotalamount.setText(getString(R.string.currency_icon) +" "+ String.format("%.2f",cartitemtotal));

                        txtgrandtotal.setText(getIntent().getStringExtra("total_amount"));
                        @SuppressLint("DefaultLocale") String deliver_charge = String.format("%.2f",resultFile.getBookingDetails().getDeliveryFee());
                        txt_delivery_amount.setText(getString(R.string.currency_icon)+" "+ deliver_charge);

                        if(resultFile.getBookingDetails().getBooking_status()==12 && resultFile.getBookingDetails().getisrated().equals("false"))
                        {
                            review_status.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            review_status.setVisibility(GONE);
                        }


                                                                                          //  for booking status
                        if(resultFile.getBookingDetails().getBooking_status()==3)
                        {
                            booking_status.setText(getString(R.string.booking_rejected));
                            booking_status.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.red));
                            relative_order_rejected.setVisibility(View.VISIBLE);
                            relative_track_order.setVisibility(GONE);
                        }
                        else if(resultFile.getBookingDetails().getBooking_status()==2)
                        {
                            booking_status.setText(getString(R.string.booking_confirm));
                            booking_status.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.accept_color));
                            relative_order_rejected.setVisibility(GONE);
                            relative_track_order.setVisibility(View.VISIBLE);
                        }

                        else if(resultFile.getBookingDetails().getBooking_status()==7)
                        {
                            booking_status.setText(getString(R.string.pickedup));
                            booking_status.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.accept_color));
                            relative_order_rejected.setVisibility(GONE);
                            relative_track_order.setVisibility(View.VISIBLE);
                        }

                        else if(resultFile.getBookingDetails().getBooking_status()==5)
                        {
                            booking_status.setText(getString(R.string.inprocess));
                            booking_status.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.accept_color));
                            relative_order_rejected.setVisibility(GONE);
                            relative_track_order.setVisibility(View.VISIBLE);
                        }

                        else if(resultFile.getBookingDetails().getBooking_status()==10)
                        {
                            booking_status.setText(getString(R.string.shipped));
                            booking_status.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.accept_color));
                            relative_order_rejected.setVisibility(GONE);
                            relative_track_order.setVisibility(View.VISIBLE);
                        }

                        else if(resultFile.getBookingDetails().getBooking_status()==12)
                        {
                            booking_status.setText(getString(R.string.txtdelivered));
                            booking_status.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.accept_color));
                            relative_order_rejected.setVisibility(GONE);
                            relative_track_order.setVisibility(View.VISIBLE);
                        }


                        List<OrderDetailsModel> orderparentmodel=resultFile.getItemDetails();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(OrderDetailsActivity.this, RecyclerView.VERTICAL, false);
                        recyclerview_yourbooking.setLayoutManager(mLayoutManager);
                        orderItemDetailsParentAdapter = new OrderItemDetailsParentAdapter(OrderDetailsActivity.this,orderparentmodel);
                        recyclerview_yourbooking.setAdapter(orderItemDetailsParentAdapter);
                    }
                    else if (resultFile.getCode() == 401)
                    {
                        Toast.makeText(OrderDetailsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (resultFile.getCode() == 404)
                    {
                        Toast.makeText(OrderDetailsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (resultFile.getCode() == 400)
                     {
                         Toast.makeText(OrderDetailsActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                     }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                Toast.makeText(OrderDetailsActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


}